.. _pmo:

*************************
Optimizing thermodynamics
*************************

Probabilistic Metabolic Optimization (PMO) allows to run optimization problems using
both flux and thermodynamic constraints. This is done using the :code:`PmoProblem`
class. 

.. code-block:: python
    :linenos:

    # Create a PMO problem.
    problem = pta.PmoProblem(model, thermodynamic_space)

    # Solve the PMO problem. The object now contains the solution.
    status = problem.solve()

Internally, optimization problems are constructed using `CVXPY
<https://www.cvxpy.org/>`_ as interface for different solvers. The CVXPY optimization
variables are :code:`problem.m` (the basis of the thermodynamic space),
:code:`problem.d` (the direction of the reactions constrained by thermodynamics) and
:code:`problem.vs` (the scaled fluxes). For convenience, we provide properties to access
predicted values of the biologically relevant variables :code:`v` (fluxes),
:code:`log_c` (log-concentrations), :code:`drg` (reaction energies), :code:`drg0`
(standard reaction energies).

Setting the objective
======================

By default, PMO maximizes the probability of the variables in the thermodynamic space.
It is possible to set custom objective (e.g. on fluxes or concentrations) by passing a
function creating the objective to the constructor.

.. code-block:: python
    :linenos:
    
    import cvxpy as cp
    objective = lambda p: cp.Minimize(p.vs[26])
    problem = pta.PmoProblem(model, thermodynamic_space, objective=objective)

Numerics
========

PMO uses the Big-M formulation to encode the directionality constraints in a MILP
problem. While this is an efficient representation, it is also known to have numerical
limitations. In particular, PMO may fail or give inaccurate solutions if fluxes and
reaction energies span more than nine orders of magnitude.

For this reason, we internally transform the problem to use scaled fluxes and a
full-dimensional basis for the thermodynamic space. While we can automatically compute
optimal values for the Big-M coefficients of fluxes, PMO guidance for the reaction
energies. In practice, we assume that all reaction energies have magnitude between 0.1
and 1000 kJ/mol, which we believe is biologically reasonable. It is possible to change
these values in the constructor of :code:`PmoProblem` in case larger or smaller reaction
energies are expected. However, this should be done with care, keeping in mind that it
may lead to numerical issues.

Currently we optimized numerics-related solver parameters for Gurobi only. If you use
other solvers, you may need to use the :code:`solver_options` argument to set stricter
feasibility tolerances for your solver.
