.. _thermodynamic_space:

***********************
The thermodynamic space
***********************

The quick way
=============

The thermodynamic space of a network models metabolite and free energies for a selected
set of reactions. In absence of additional information, a thermodynamic space can be
constructed from a COBRApy model:

.. code-block:: python
    :linenos:

    thermodynamic_space = pta.ThermodynamicSpace.from_cobrapy_model(model)

This is a wrapper around the constructor of the :class:.:`ThermodynamicSpace` class,
which can be used when interfacing with other modeling frameworks.

Advanced initialization
=======================

When additional information is available, one can provide a series of additional
optional parameters. The parameters are explained in the following sections.

.. code-block:: python
    :linenos:

    thermodynamic_space = pta.ThermodynamicSpace.from_cobrapy_model(
        model,
        metabolite_interpreter,
        constrained_rxns,
        estimator,
        parameters,
        concentrations
    )

Metabolite interpreter
----------------------

An instance of the :code:`MetaboliteInterpreter` class specifying how PTA should parse
metabolite identifiers. This is necessary to find the compound identifier and the
compartment of the metabolite. Note that at the moment only BiGG names are supported.

Constrained reactions
----------------------

The :code:`constrained_rxns` parameter specifies which reactions in the network should
be modeled in the thermodynamic space. Thermodynamic constraints should be applied only
to balanced reaction, as pseudo-reactions are not thermodynamically realistic. Thus, by
default, boundary reactions and biomass are excluded. Reactions can be specified as
lists of indices, lists of identifiers or lists of reactions. As a starting point, we
recommend building the list of constrained reactions using:

.. code-block:: python
    :linenos:

    pta.utils.get_candidate_thermodynamic_constraints(
        FluxSpace.from_cobrapy_model(model),
        metabolite_interpreter
    )

Estimator
---------

An object used for estimating standard reaction energies. At the moment PTA only uses
eQuilibrator (:code:`EquilibratorGibbsEstimator`), but you are free to implement an
interface for alternative estimation tools.

Compartment parameters
----------------------

An object specifying the parameters (pH, pMg, ionic strength, electrostatic potential)
of each compartment. Temperature must be constant for the entire system. You can either
fill this object yourself or load one of the default ones (currently `e_coli` and
`human`):

.. code-block:: python
    :linenos:

    parameters = pta.CompartmentParameters.load('e_coli')

Concentrations prior
--------------------

:code:`ConcentrationsPrior` objects specify measured or assumed distributions for the
concentration of each metabolite. Since measurements are usually not available for all
metabolites, PTA will use the following information, in order of preference, to
determine the distribution of metabolite M in compartment C:

1. The distribution of the concentration of M itself.
2. A default distribution for any metabolite in C.
3. A default distribution for any metabolite.

You can load priors from files or create your own. PTA includes priors for the
extracellular concentrations in M9 (:code:`M9_aerobic`, :code:`M9_anaerobic`) and for
the intracellular concentrations with different carbon sources (:code:`ecoli_M9_<s>`,
where :code:`<s>` can be :code:`ac`, :code:`fru`, :code:`gal`, :code:`glc`,
:code:`glcn`, :code:`glyc`, :code:`pyr`, :code:`succ`) based on
:footcite:`gerosa2015pseudo`. Different can also be combined. In case of conflict, the added
prior overwrites distributions of the original prior.

.. code-block:: python
    :linenos:

    # Load the prior for metabolite concentrations in M9 media.
    concentrations = pta.ConcentrationsPrior.load('M9_aerobic')

    # Add the prior for intracellular concentrations when growing on succinate.
    concentrations.add(pta.ConcentrationsPrior.load('ecoli_M9_succ'))

    # Manually add a distribution. Note that we must use log-normal distributions.
    import numpy as np
    concentrations.metabolite_distributions[('bigg:metabolite:g3p', 'c')] = \
        pta.LogNormalDistribution(log_mean=np.log(1e-4), log_std=0.2)

    # Updating identifiers after a modification is recommended to make sure that we can
    properly recognize identifiers from different namespaces.
    concentrations.update_identifiers()

    # Save your newly created prior for later use.
    concentrations.save('my_prior.csv')

Thermodynamic space basis
=========================

Because of correlation existing within and between metabolite concentrations, standard
reaction energies and reaction energies, the dimensionality of the thermodynamic space
is usually lower than the number of variables. One can easily obtain a full-dimensional
representation that can be used for PMO and TFS:

.. code-block:: python
    :linenos:
    
    basis = pta.ThermodynamicSpaceBasis(thermodynamic_space)

This class contains a mapping between a full dimensional basis and the variables of the
thermodynamic space. Variables of the basis are referred to as **m** in
:footcite:`gollub2020probabilistic`.

Performance considerations
==========================

Many of the functions above rely on eQuilibrator's :code:`CompoundCache` to identify
compounds from different namespaces. Initializing this object takes several seconds,
thus we recommend creating it once (either using
:code:`pta.EquilibratorGibbsEstimator.eq_api.ccache` or creating it with
:code:`equilibrator_api.create_compound_cache_from_zenodo()`) and passing it to the
methods or classes using it (e.g. :code:`get_candidate_thermodynamic_constraints`,
:code:`ThermodynamicSpace`, :code:`ConcentrationsPrior`).

References
==========================

.. footbibliography::