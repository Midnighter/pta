PTA Overview
============

This is the documentation for the PTA python package. PTA is available on `git
<https://gitlab.com/csb.ethz/pta/>`_ and via `pip <https://pypi.org/project/pta/>`_. See
:ref:`getting started <getting_started>` for the installation instructions and a simple
example. 

************************************
Probabilistic Thermodynamic Analysis
************************************
 
Probabilistic Thermodynamic Analysis (PTA) :footcite:`gollub2020probabilistic` is a
framework for the exploration of the thermodynamic properties of a metabolic network. In
PTA, we consider the  *steady-state thermodynamic space* of a network, that is, the
space of standard reaction energies and metabolite concentrations that are compatible
with steady state flux constraints. The uncertainty of the variables in the
thermodynamic space is modeled with a probability distribution, allowing analysis with
optimization and sampling approaches.

Probabilistic Metabolic Optimization (PMO)
##########################################

PMO aims at finding the most probable values of reaction energies and metabolite
concentrations that are compatible with the steady state constrain. This method is
particularly useful to indentify features of the network that are thermodynamically
unrealistic. For example, PMO can identify substrate channeling, incorrect cofactors or
inaccurate directionalities. 

Thermodynamic and Flux Sampling (TFS)
#####################################
TFS allows to jointly sample the thermodynamic and flux spaces of a network. The method
provides estimates of metabolite concentrations, reactions directions, and flux
distributions.

.. toctree::
   :hidden:
   :numbered:
   :maxdepth: 2
 
   self
   getting_started
   preprocessing
   thermodynamic_space
   pmo
   assessment
   tfs
   autoapi/index

**********
References
**********

.. footbibliography::