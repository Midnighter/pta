{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creation of metabolite concentration priors for *E. coli*\n",
    "Based on data from (Gerosa et al., 2015, https://doi.org/10.1016/j.cels.2015.09.008)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import required packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from equilibrator_cache import create_compound_cache_from_quilt\n",
    "import numpy as np\n",
    "import numpy.random as rnd\n",
    "import pandas as pd\n",
    "import pta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load measured metabolite concentrations from the supplementary data and store them in PTA format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "file_name = 'data/gerosa_2015_S2.xlsx'\n",
    "sheet_name = 'Metabolite concentrations'\n",
    "uM_per_gCDW_to_M = 1e-6 / 2.3 * 1e3\n",
    "min_log_std = 0.05\n",
    "\n",
    "df = pd.read_excel(file_name, sheet_name=sheet_name, header=1, usecols='B,D:K,M:T', nrows=43)\n",
    "df.columns = ['met', 'ac', 'fru', 'gal', 'glc', 'glyc', 'glcn', 'pyr', 'succ',\n",
    "    'ac_std', 'fru_std', 'gal_std', 'glc_std', 'glyc_std', 'glcn_std', 'pyr_std', 'succ_std']\n",
    "\n",
    "# Drop 3pg+2pg as we don't know how to handle sums of concentrations.\n",
    "df = df[df['met'] != '2pg+3pg']\n",
    "\n",
    "df['met'] = df['met'].replace({\n",
    "        'glu' : 'glu__L',\n",
    "        'gln' : 'gln__L',\n",
    "        'asn' : 'asn__L',\n",
    "        'asp' : 'asp__L',\n",
    "        'tyr' : 'tyr__L',\n",
    "        'arg' : 'arg__L',\n",
    "        'phe' : 'phe__L',\n",
    "        'cAMP' : 'camp',\n",
    "}).map(lambda m : 'bigg.metabolite:' + m.replace('-', '__'))\n",
    "\n",
    "# Convert to molar.\n",
    "df[df.columns.difference(['met'])] = df[df.columns.difference(['met'])] * uM_per_gCDW_to_M\n",
    "\n",
    "# The reported uncertainty is normally distributed. Find a log-normal approximation.\n",
    "def normal_to_log_normal(series):\n",
    "    samples = np.maximum(rnd.normal(series[0], series[1], 10000), 1e-10)\n",
    "    log_samples = np.log(samples)\n",
    "    series[0] = np.log(series[0])\n",
    "    series[1] = np.std(log_samples)\n",
    "    return series\n",
    "\n",
    "# Convert all values to log scale.\n",
    "for i in range(8):\n",
    "    df.iloc[:,[1+i, 9+i]] = df.iloc[:,[1+i, 9+i]].apply(\n",
    "        lambda x : normal_to_log_normal(x), axis=1)\n",
    "\n",
    "# Assume a minimum technical error on for each measurement.\n",
    "df.iloc[:,9:].applymap(lambda std : max(std, min_log_std));"
   ]
  },
  {
   "source": [
    "### Create an 'unspecific' prior, where the distribution is the same for any metabolite."
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "output_type": "stream",
     "name": "stdout",
     "text": "Downloading package metadata...\nFragments already downloaded\n"
    }
   ],
   "source": [
    "ccahe = create_compound_cache_from_quilt()\n",
    "\n",
    "all_values = df.iloc[:,1:9].values.ravel()\n",
    "all_values = all_values[np.isfinite(all_values)]\n",
    "default_distribution = pta.LogNormalDistribution(\n",
    "    np.mean(all_values), np.std(all_values))\n",
    "\n",
    "compartment_distributions = {\n",
    "    'c' : default_distribution,\n",
    "    'p' : pta.LogNormalDistribution(\n",
    "        default_distribution.log_mean, \n",
    "        default_distribution.log_std * 5),\n",
    "    'e' : pta.LogNormalDistribution(\n",
    "        default_distribution.log_mean, \n",
    "        default_distribution.log_std * 5)\n",
    "}\n",
    "\n",
    "unspecific_prior = pta.ConcentrationsPrior(\n",
    "    ccahe,\n",
    "    compartment_distributions=compartment_distributions,\n",
    "    default_distribution=default_distribution\n",
    ")\n",
    "unspecific_prior.save('../pta/data/concentration_priors/ecoli_M9_unspecific.csv')"
   ]
  },
  {
   "source": [
    "### Create priors that are specific for metabolites and conditions."
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "conditions = ['ac', 'fru', 'gal', 'glc', 'glyc', 'glcn', 'pyr', 'succ']\n",
    "for c in range(8):\n",
    "    metabolite_distributions = {}\n",
    "    for _, r in df.iloc[:,[0, 1+c, 9+c]].iterrows():\n",
    "        if np.isfinite(r[1]) and np.isfinite(r[2]):\n",
    "            metabolite_distributions[(r[0], 'c')] = pta.LogNormalDistribution(\n",
    "                r[1], r[2])\n",
    "\n",
    "    condition_prior = pta.ConcentrationsPrior(\n",
    "        ccahe,\n",
    "        metabolite_distributions=metabolite_distributions,\n",
    "        compartment_distributions=compartment_distributions,\n",
    "        default_distribution=default_distribution\n",
    "    )\n",
    "    condition_prior.save(\n",
    "        f'../pta/data/concentration_priors/ecoli_M9_{conditions[c]}.csv')\n"
   ]
  },
  {
   "source": [
    "### Create a prior that is specific for metabolites but not for conditions."
   ],
   "cell_type": "markdown",
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "metabolite_distributions = {}\n",
    "for _, r in df.iterrows():\n",
    "    metabolite_distributions[(r[0], 'c')] = pta.LogNormalDistribution(\n",
    "        np.mean(r[1:9].dropna()), np.std(r[1:9].dropna()))\n",
    "\n",
    "    prior = pta.ConcentrationsPrior(\n",
    "        ccahe,\n",
    "        metabolite_distributions=metabolite_distributions,\n",
    "        compartment_distributions=compartment_distributions,\n",
    "        default_distribution=default_distribution\n",
    "    )\n",
    "    prior.save(\n",
    "        f'../pta/data/concentration_priors/ecoli_M9_specific.csv')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}