import time

import numpy as np
import pytest

import cobra
import cobra.test
import pta
from pta.sampling.tfs import (
    TFSModel,
    get_initial_points,
    sample_drg,
    sample_drg0_from_drg,
    sample_fluxes_from_drg,
    sample_log_conc_from_drg,
)

cobra_config = cobra.Configuration()
cobra_config.processes = 1


def test_tfs():
    model = cobra.test.create_test_model("textbook")
    model.reactions.Biomass_Ecoli_core.lower_bound = 0.5
    pta.prepare_for_pta(model)
    thermodynamic_space = pta.ThermodynamicSpace.from_cobrapy_model(
        model, parameters=pta.CompartmentParameters.load("e_coli")
    )

    num_chains = 4
    tfs_model = TFSModel(model, thermodynamic_space, solver="GUROBI")

    points = get_initial_points(tfs_model, num_chains)

    assert points.shape[1] == num_chains

    result = sample_drg(tfs_model, initial_points=points, num_chains=num_chains)

    assert result is not None
    assert result.check_convergence()

    r = sample_drg0_from_drg(thermodynamic_space, result.samples)
    c = sample_log_conc_from_drg(thermodynamic_space, result.samples)
    f = sample_fluxes_from_drg(model, result.samples, result.orthants)

    assert True
