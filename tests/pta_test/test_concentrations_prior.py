from pta.concentrations_prior import ConcentrationsPrior
from pta.distributions import LogNormalDistribution


def test_different_namespaces():
    distribution = LogNormalDistribution(-5, 1)
    prior = ConcentrationsPrior(
        metabolite_distributions={('g6p', 'c'): distribution}
    )

    kegg_distribution = prior.get('C00092', 'c')
    assert kegg_distribution.log_mean == distribution.log_mean
    assert kegg_distribution.log_std == distribution.log_std
