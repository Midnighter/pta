import cobra.test
import numpy as np
import pytest

from pta.utils import get_internal_cycles

cobra_config = cobra.Configuration()
cobra_config.processes = 1

def test_get_internal_cycles():
    model = cobra.test.create_test_model("textbook")
    cycles = get_internal_cycles(model)
    assert cycles.shape[1] == 1
