import math

import numpy as np
import pytest

import cobra
import cobra.test
import pta
from pta.compartment_parameters import CompartmentParameters
from pta.concentrations_prior import ConcentrationsPrior
from pta.constants import Q
from pta.distributions import LogNormalDistribution
from pta.flux_space import FluxSpace
from pta.gibbs_estimators.equilibrator_gibbs_estimator import EquilibratorGibbsEstimator
from pta.metabolite import Metabolite
from pta.metabolite_interpreter import (
    CompartmentConvention,
    MetaboliteInterpreter,
    Namespace,
)
from pta.model_assessment import prepare_for_pta
from pta.pmo import PmoProblem
from pta.thermodynamic_space import ThermodynamicSpace, ThermodynamicSpaceBasis

cobra_config = cobra.Configuration()
cobra_config.processes = 1


@pytest.fixture(scope="module")
def equilibrator() -> EquilibratorGibbsEstimator:
    return EquilibratorGibbsEstimator()


def test_pmo(equilibrator):
    S = np.array(
        [
            [1, -1, 0],
            [0, 1, -1],
        ]
    )

    metabolites = [
        Metabolite("g6p", "c", 11, -2),
        Metabolite("f6p", "c", 11, -2),
    ]

    reaction_ids = ["R1", "R2", "R3"]

    dfg0_estimate = (
        Q(np.array([1, 0]).T, "kJ/mol"),
        Q(np.array([[0.1, 0], [0, 0.1]]), "kJ/mol"),
    )

    thermodynamic_space = ThermodynamicSpace(
        S[:, [1]],
        reaction_ids[1:2],
        metabolites,
        parameters=CompartmentParameters(),
        concentrations=ConcentrationsPrior(equilibrator.eq_api.ccache),
        dfg0_estimate=dfg0_estimate,
    )

    flux_space = FluxSpace(
        S,
        np.array([[0, 0, 10]]).T,
        np.array([[1000, 1000, 10]]).T,
        reaction_ids,
        [m.key for m in metabolites],
    )

    problem = PmoProblem(flux_space, thermodynamic_space)
    status = problem.solve(verbose=True)

    assert status == "optimal"
    log_c = problem.log_c
    assert math.exp(log_c[0]) > math.exp(log_c[1])
    assert np.isclose(problem.v[1], 10.0)


def test_pmo_large(equilibrator):
    model = cobra.test.create_test_model("textbook")
    prepare_for_pta(model)
    thermodynamic_space = pta.ThermodynamicSpace.from_cobrapy_model(
        model, parameters=pta.CompartmentParameters.load("e_coli")
    )
    problem = PmoProblem(model, thermodynamic_space, solver="GUROBI")

    status = problem.solve(verbose=True)
    assert status == "optimal"

    problem2 = problem.rebuild_for_directions(problem.d.value)
    status = problem2.solve(verbose=True)
    assert status == "optimal"
