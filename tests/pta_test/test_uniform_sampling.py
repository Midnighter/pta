import numpy as np
import pytest

import cobra.test
from pta.sampling.uniform import sample_flux_space_uniform


def test_uniform_sampling():
    model = cobra.test.create_test_model("textbook")
    model.reactions.Biomass_Ecoli_core.lower_bound = 0.5

    result = sample_flux_space_uniform(model, 1000)
    assert result.check_convergence()
