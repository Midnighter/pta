import numpy as np
import pytest

import cobra
import cobra.test
from cobra.util.array import create_stoichiometric_matrix
from pta.compartment_parameters import CompartmentParameters
from pta.concentrations_prior import ConcentrationsPrior
from pta.constants import Q
from pta.flux_space import FluxSpace
from pta.gibbs_estimators.equilibrator_gibbs_estimator import EquilibratorGibbsEstimator
from pta.metabolite import Metabolite
from pta.metabolite_interpreter import (
    CompartmentConvention,
    MetaboliteInterpreter,
    Namespace,
)
from pta.thermodynamic_space import ThermodynamicSpace, ThermodynamicSpaceBasis
from pta.utils import get_candidate_thermodynamic_constraints, get_internal_reactions


@pytest.fixture(scope="module")
def equilibrator() -> EquilibratorGibbsEstimator:
    return EquilibratorGibbsEstimator()


def test_creation_cobra(equilibrator):
    model = cobra.test.create_test_model("textbook")
    constraint_rxns = get_internal_reactions(model)

    thermodynamic_space = ThermodynamicSpace.from_cobrapy_model(
        model,
        MetaboliteInterpreter(CompartmentConvention.UNDERSCORE, Namespace.BIGG),
        constraint_rxns,
        equilibrator,
        CompartmentParameters(),
        None,
    )

    assert thermodynamic_space.drg0_prime_mean.size == len(constraint_rxns)


def test_creation_with_estimates(equilibrator):

    S = np.array(
        [
            [1, -1, 0],
            [0, 1, -1],
            [-1, 0, 1],
        ]
    )

    metabolites = [
        Metabolite("M_A", "c", 11, -2),
        Metabolite("M_B", "c", 11, -2),
        Metabolite("M_C", "c", 11, -2),
    ]

    constraint_rxn_ids = ["R1", "R2", "R3"]

    dfg0_estimate = (
        Q(np.array([0, 1, 2]).T, "kJ/mol"),
        Q(np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), "kJ**2/mol**2"),
    )

    thermodynamic_space = ThermodynamicSpace(
        S,
        constraint_rxn_ids,
        metabolites,
        CompartmentParameters(),
        ConcentrationsPrior(equilibrator.eq_api.ccache),
        dfg0_estimate=dfg0_estimate,
    )

    assert thermodynamic_space.drg0_prime_mean.size == len(constraint_rxn_ids)


@pytest.mark.parametrize(
    "S, metabolites, constraint_rxn_ids, explicit_conc, dimensions",
    [
        (
            np.array(
                [
                    [1, -1, 0],
                    [0, 1, -1],
                    [-1, 0, 1],
                ]
            ),
            [
                Metabolite("bigg.metabolite:g1p", "c", 11, -2),
                Metabolite("bigg.metabolite:g6p", "c", 11, -2),
                Metabolite("bigg.metabolite:f6p", "c", 11, -2),
            ],
            ["A", "B", "C"],
            False,
            2,
        ),
        (
            np.array(
                [
                    [1, -1, 0],
                    [0, 1, -1],
                    [-1, 0, 1],
                ]
            ),
            [
                Metabolite("bigg.metabolite:g1p", "c", 11, -2),
                Metabolite("bigg.metabolite:g6p", "c", 11, -2),
                Metabolite("bigg.metabolite:f6p", "c", 11, -2),
            ],
            ["A", "B", "C"],
            True,
            5,
        ),
    ],
)
def test_minimal_basis(
    equilibrator, S, metabolites, constraint_rxn_ids, explicit_conc, dimensions
):
    thermodynamic_space = ThermodynamicSpace(
        S, constraint_rxn_ids, metabolites, estimator=equilibrator
    )

    minimal_basis = ThermodynamicSpaceBasis(
        thermodynamic_space, explicit_conc, False, True
    )

    assert np.size(minimal_basis.to_drg_transform[0], 1) == dimensions
    assert explicit_conc != (minimal_basis.to_log_conc_transform is None)


def test_large_minimal_basis(equilibrator):
    model = cobra.test.create_test_model("textbook")
    constraint_rxns = get_internal_reactions(model)

    thermodynamic_space = ThermodynamicSpace.from_cobrapy_model(
        model,
        MetaboliteInterpreter(CompartmentConvention.UNDERSCORE, Namespace.BIGG),
        constraint_rxns,
        equilibrator,
        CompartmentParameters(),
        None,
    )

    minimal_basis = ThermodynamicSpaceBasis(thermodynamic_space, True, True, True)


def test_get_candidate_thermodynamic_constraints(equilibrator):
    model = cobra.test.create_test_model("textbook")
    candidates = get_candidate_thermodynamic_constraints(
        FluxSpace.from_cobrapy_model(model),
        MetaboliteInterpreter(CompartmentConvention.UNDERSCORE, Namespace.BIGG),
        ccache=equilibrator.eq_api.ccache,
    )

    assert "H2Ot" not in candidates
    assert "Biomass_Ecoli_core" not in candidates
    assert all(["EX_" not in c for c in candidates])
    assert len(candidates) == len(model.reactions) - len(model.exchanges) - 2
