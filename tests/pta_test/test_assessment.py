import cobra
import cobra.test
import numpy as np

import pta

cobra_config = cobra.Configuration()
cobra_config.processes = 1

def test_structural_assessment():
    model = cobra.test.create_test_model("textbook")
    model.reactions.Biomass_Ecoli_core.lower_bound = 0.5
    assessment = pta.StructuralAssessment(model)
    assert len(assessment.forced_internal_cycles) == 1
