% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% Settings
modelDefinition = 'e_coli_core.yaml';
outputFolder = fullfile(getPtaRoot(), 'data/results/e_coli_core');

thermodynamicSpaceSettings.sampleConcentrations = false;
thermodynamicSpaceSettings.freeEnergyVarsConfidence = 0.95;
thermodynamicSpaceSettings.minDimensionEigenvalue = 1e-4;

optimizationSettings.fluxEpsilon = 1e-4;
optimizationSettings.drgEpsilon = 1e-1;
optimizationSettings.drgMax = 1000;

optimizationSettings.gurobiSettings.IntFeasTol = 1e-9;
optimizationSettings.gurobiSettings.FeasibilityTol = 1e-9;
optimizationSettings.gurobiSettings.TimeLimit = 1800;

[drgSamplingSettings, fluxSamplingSettings] = ...
    getDefaultTFSSettings(outputFolder);

%% Generate model.
changeCobraSolver('gurobi');
model = cobragen.generateCobraModelFromYaml(which(modelDefinition));
model = removeRxns(model, findBlockedReaction(model));
model.lb(model.c ~= 0) = 0.5;
model.lb(strcmp(model.rxns, 'EX_ac_e')) = 5;
model = tightenModelBounds(model);
model = cobragen.addThermodynamicDataToModel(model);

%% Run PMO. Predicted concentrations and DrG' are stored in outputFolder.
runThermodynamicModelAssessment(model, outputFolder, ...
    optimizationSettings, thermodynamicSpaceSettings);

%% Sample DrG' and directions.
freeEnergySpace = GibbsFreeEnergiesSpace(model, thermodynamicSpaceSettings);
drgSamplingResult = sampleFreeEnergies(model, freeEnergySpace, drgSamplingSettings);
drg = drgSamplingResult.drg.samples;
directions = drgSamplingResult.directions;

%% Sample concentrations and DrG°'.
[logConc, drg0] = sampleLogConcAndDrg0FromDrg( ...
    drg, model, thermodynamicSpaceSettings);

%% Sample fluxes.
reversibleRxns = model.rxns( ...
    model.lb < 0 & model.ub > 0 & model.isConstraintRxn);
fluxes = sampleFluxSpaceWithDirectionsPrior(model, directions, ...
    reversibleRxns, fluxSamplingSettings);

%% Plot results.
nPlotsX = 7;
nPlotsY = 5;
nPlots = nPlotsX * nPlotsY;
skipPlots = 15;

figure('Name', 'DrG', 'NumberTitle', 'off');
constrainedRxnNames = model.rxns(model.isConstraintRxn);
for i=skipPlots+1:skipPlots+nPlots
   subplot(nPlotsY, nPlotsX, i-skipPlots);
   histogram(drg(i, :));
   title(constrainedRxnNames{i}, 'Interpreter', 'none');
   xlabel('DrG (kJ/mol)');
   ylabel('# samples');
end

figure('Name', 'DrG°', 'NumberTitle', 'off');
constrainedRxnNames = model.rxns(model.isConstraintRxn);
for i=skipPlots+1:skipPlots+nPlots
   subplot(nPlotsY, nPlotsX, i-skipPlots);
   histogram(drg0(i, :));
   title(constrainedRxnNames{i}, 'Interpreter', 'none');
   xlabel('DrG° (kJ/mol)');
   ylabel('# samples');
end

figure('Name', 'Concentrations', 'NumberTitle', 'off');
metNames = model.mets;
for i=skipPlots+1:skipPlots+nPlots
    subplot(nPlotsY, nPlotsX, i-skipPlots);
    [~, edges] = histcounts(logConc(i, :));
    histogram(exp(logConc), exp(edges));
    set(gca, 'xscale','log');
    title(metNames{i}, 'Interpreter', 'none');
    xlabel('Concentration (M)');
   ylabel('# samples');
end

figure('Name', 'Fluxes', 'NumberTitle', 'off');
constrainedRxnNames = model.rxns(model.isConstraintRxn);
constrainedFluxes = fluxes(model.isConstraintRxn, :);
for i=skipPlots+1:skipPlots+nPlots
   subplot(nPlotsY, nPlotsX, i-skipPlots);
   histogram(constrainedFluxes(i, :));
   title(constrainedRxnNames{i}, 'Interpreter', 'none');
   xlabel('Flux (mmol/gCDW/h)');
   ylabel('# samples');
end