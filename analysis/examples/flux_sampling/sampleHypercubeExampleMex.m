% Demonstrate how to uniformly sample from an n-dimensional hypercube.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% Test settings.
nDimensions     = 100;
nSteps          = nDimensions * nDimensions * 8;
dataPath        = fullfile(getPtaRoot(), 'data', 'examples', 'hypercube');
[~, ~] = mkdir(dataPath);

settings = struct();
settings.workerId = 0;
settings.stepsThinning = max(1, floor(nSteps / 1000));
settings.nSteps = nSteps;
settings.nSamples = nan;
settings.nRecordedSteps = 1000;
settings.nChains = 4;
settings.nWarmupSteps = floor(nSteps / 2);
settings.logsDir = string(fullfile(dataPath, 'logs'));
settings.maxWorkers = inf;
    
%% Setup test polytope P = { x | Ax = b, Gx <= h }.
lb        = -ones(nDimensions, 1);
ub        = ones(nDimensions, 1);
ub(2) = 60;

A = zeros(0, nDimensions);
b = [];
G = [eye(nDimensions); -eye(nDimensions)];
h = [ub; -lb];
    
%% Sample the test polytope.
polytope = samply.Polytope(A, b, G, h);
polytope.transformToMinimalParametrization();

sampler = @(initialPoints, settings) sample_flux_space_uniform_mex( ...
    polytope.G, polytope.h, polytope.fromMaxIsotropyFrame.T, ...
    initialPoints, settings);

initialPoints = polytope.getInitialSamples(settings.nChains);

tic;
samplerResult = runSampler(sampler, initialPoints, settings);
toc;

samples = polytope.toOriginal.transform(samplerResult.samples);
chainSteps = polytope.toOriginal.transformChains(samplerResult.chains);

%% Plot marginal distributions for each dimension.
figure(1);
samply.plotMarginalsHistograms(samples);

%% Diagnose convergence.
chains = { struct( ...
    'name', 'Value ', ...
    'parameterNames', {cellstr(num2str((1:nDimensions)'))}, ...
    'chains', chainSteps) };

cs.enabled = true;
cs.outputFolder = fullfile(dataPath, 'convergence');
cs.tests = {'splitR', 'ESS', 'traces'};
samply.analyzeChainsConvergence(chains, cs);
