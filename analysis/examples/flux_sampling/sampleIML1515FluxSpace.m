% Uniformly sample the flux space of the e_coli_core model.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% Model and general settings.
model = readCbModel(fullfile(getPtaRoot(), ...
    'data', 'models', 'iML1515.mat'));
changeCobraSolver('gurobi');
dataPath = fullfile(getPtaRoot(), 'data', 'examples', 'core');
[~, ~] = mkdir(dataPath);

%% Sampler settings.

% Total number of samples. If NaA, it is automatically set to
% nChains * nRecordedSteps. 
settings.samplerSettings.nSamples = NaN;

% Number of parallel chains to run. At least 4 chains should be used for the
% convergence diagnostic methods to work.
settings.samplerSettings.nChains = 24;

% Number of MCMC steps per chain. If NaN, it is automatically set based on the
% dimensionality of the space.
settings.samplerSettings.nSteps = NaN;

% Number of steps that must be ignored before starting recording. If NaN, this
% is set to nSteps / 2 (highly recommended).
settings.samplerSettings.nWarmupSteps = NaN;

% Number of steps to record from each chain (after warmup). At least 100 steps
% are recommended. If you increase this number make sure that all the chains can
% fit in the computer memory.
settings.samplerSettings.nRecordedSteps = 1000;

% Maximum number of workers to use. If infinite, all the workers of the current
% parallel pool will be used. If 0, no parallelization is applied and the
% sampler is executed on the host.
settings.samplerSettings.maxWorkers = Inf;

% Directory to which the progress of the sampler is logged. On Linux, a simple 
% way of monitoring the state of the sampler if to run the following ocmmand in
% a terminal:
%     watch -n 1 tail -q -n 1 <path_to_log_files>/*.log
settings.samplerSettings.logsDir = string(fullfile(dataPath, 'logs'));

%% Convergence diagnostics settings.

% Specifies whether convergence checks should be run.
settings.convergenceDiagnostic.enabled = true;

% Directory to which the plots resulting from the convergence tests should be
% stored.
settings.convergenceDiagnostic.outputFolder = ...
    string(fullfile(dataPath, 'convergence'));

% List of convergence tests to apply. Possible tests are:
% > 'Split-R':  Compute the R-hat measure as described in (Gelman, 2013). If any
%               parameter has R-hat > 1.1, the chains did *not* mix well and
%               should be simulated for a higher number of steps.
% > 'ESS':      Compute the Effective Sample Size measure as described in 
%               (Gelman, 2013). If any parameter has ESS < 10 * nChains, the
%               chains exhibit high autocorrelation and should be simulated for
%               a higher number of steps.
% > 'traces':   Simply plots the traces of a few chains for each parameters.
%               Only useful to visually investigate the bahavior of the chains.
settings.convergenceDiagnostic.tests = {'ESS', 'splitR' };

%% Sample from the flux space polytope.
[~, ~] = mkdir(settings.samplerSettings.logsDir);
[~, ~] = mkdir(settings.convergenceDiagnostic.outputFolder);
% The result object contains samples, initial points, and all recorded steps.

model = removeRxns(model, findBlockedReaction(model));
result = sampleFluxSpaceUniform(model, settings);

%% Plot the resulting distributions.
figure(1);
samply.plotMarginalsHistograms(result.fluxes.samples, ...
    'paramNames', model.rxns, 'nColumns', 10);