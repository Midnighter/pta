% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% Settings
modelDefinition = 'Recon3D.yaml';
outputFolder = fullfile(getPtaRoot(), 'data/results/Recon3D');

thermodynamicSpaceSettings.freeEnergyVarsConfidence = 0.95;
thermodynamicSpaceSettings.minDimensionEigenvalue = 1e-4;

optimizationSettings.fluxEpsilon = 1e-4;
optimizationSettings.drgEpsilon = 1e-1;
optimizationSettings.drgMax = 1000;

optimizationSettings.gurobiSettings.IntFeasTol = 1e-9;
optimizationSettings.gurobiSettings.FeasibilityTol = 1e-9;
optimizationSettings.gurobiSettings.TimeLimit = 1800;

%% Generate model.
changeCobraSolver('gurobi');
model = cobragen.generateCobraModelFromYaml(which(modelDefinition));
% Skip the expensive step of finding blocked reaction, since the original
% Recon3D doesn't seem to have any.
% model = removeRxns(model, findBlockedReaction(model));
model = cobragen.addThermodynamicDataToModel(model);

%% Run PMO. Predicted concentrations and DrG' are stored in outputFolder.
runThermodynamicModelDiagnostics(model, outputFolder, ...
    optimizationSettings, thermodynamicSpaceSettings);