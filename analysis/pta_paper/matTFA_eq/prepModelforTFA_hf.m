function modeloutput = prepModelforTFA_hf(model, confidence)
% prepares a COBRA toolbox model for TFBA analysis by doing the following:
% - checks if a reaction is a transport reaction
% - checks the ReactionDB for Gibbs energies of formation of metabolites
% - computes the Gibbs energies of reactions
%
% INPUTS:
% i) model - COBRA toolbox metabolic model structure
% ii) ReactionDB - ReactionDB database with all the compounds and reactions
% data
% iii) CompartmentData - structure storing the compartmental pH,
% ionic strength, membrane potential, etc.
%
% OUTPUT:
% i) modeloutput - processed COBRA toolbox model ready to be converted to TFBA
% model
% 
% things to add: Summary report of the number of compounds and reactions
% with thermodynamic data

%%

if issparse(model.S)
    model.S=full(model.S);
end

[num_mets, num_rxns] = size(model.S);
modeloutput = model;

%%-----------------------------------------------------------------------%%
%                              METABOLITES
%%-----------------------------------------------------------------------%%

disp('Checking for transport reactions');
% modeloutput = checkTransport(modeloutput);

disp('Fetching compounds thermodynamic data');

%% Use eQuilibrator's dfG estimates that are already in the model.
modeloutput.metDeltaGFerr   = sqrt(diag(model.dfg0Cov));
modeloutput.metDeltaGFtr    = model.dfg0Mean;
modeloutput.metCharge       = model.metCharges;

%%-----------------------------------------------------------------------%%
%                              REACTIONS
%%-----------------------------------------------------------------------%%

modeloutput.rxnThermo = model.isConstraintRxn;
modeloutput.rxnDeltaGR = ones(num_rxns,1) * 1e7;
modeloutput.rxnDeltaGRerr = ones(num_rxns,1) * 1e7;

toDrg0 = model.S(:, model.isConstraintRxn)';
confidenceMultiplier = sqrt(chi2inv(confidence, 1));
modeloutput.rxnDeltaGR(model.isConstraintRxn) = ...
    toDrg0 * model.dfg0Mean + model.drgCorr(model.isConstraintRxn);
modeloutput.rxnDeltaGRerr(model.isConstraintRxn) = ...
     confidenceMultiplier * sqrt(diag(toDrg0 * model.dfg0Cov * toDrg0'));

 
% Check if we have an information field on the lumped reactions
% model.info_LMPD = 
% [LumpName BBBname LumpFormula LumpSubNetowrkRxnNames LumpSubNetworkRxnFormula]
FIELDS=fieldnames(model);
if ismember({'info_LMPD'},FIELDS)
    modeloutput.info_LMDP=model.info_LMDP;
end

disp('Done');

end
