library(cowplot)
library(philentropy)
library(ggplot2)
library(PEIP)
library(rbin)
library(scales)
library(stringr)
library(truncnorm)

source('hyperflux_data.R')

root.dir <- '../../../data/results/iML1515_CAN_without_metabolomics'
confidence.interval <- 0.95
confidence.mult <- sqrt(chi2inv(confidence.interval, 1))

# Load data ####################################################################

# Load expeimantal data and estimates from TFS and TMFA.
gerosa.2015.conc.df <- load.gerosa.2015.concentrations(root.dir)
tfs.conc.samples.df <- load.concentration.samples(root.dir)
tmfa.conc.bounds.df <- load.tmfa.bounds(root.dir)

met.levels <- union(union(union(
  levels(gerosa.2015.conc.df$Metabolite),
  levels(tfs.conc.samples.df$Metabolite)),
  levels(tmfa.conc.bounds.df$Metabolite)),
  'prior')
cond.levels <- union(union(
  levels(gerosa.2015.conc.df$Condition),
  levels(tfs.conc.samples.df$Condition)),
  levels(tmfa.conc.bounds.df$Condition))

gerosa.2015.conc.df <- gerosa.2015.conc.df %>%
  mutate(Metabolite = factor(Metabolite, levels = met.levels))
tfs.conc.samples.df <- tfs.conc.samples.df %>%
  mutate(Metabolite = factor(Metabolite, levels = met.levels))
tmfa.conc.bounds.df <- tmfa.conc.bounds.df %>%
  mutate(Metabolite = factor(Metabolite, levels = met.levels))

gerosa.2015.conc.df <- gerosa.2015.conc.df %>%
  mutate(Condition = factor(Condition, levels = cond.levels))
tfs.conc.samples.df <- tfs.conc.samples.df %>%
  mutate(Condition = factor(Condition, levels = cond.levels))
tmfa.conc.bounds.df <- tmfa.conc.bounds.df %>%
  mutate(Condition = factor(Condition, levels = cond.levels))

# Generate samples from the TFS prior for visualization.
tfs.conc.prior.df <- get.concentration.pior(gerosa.2015.conc.df)
prior.samples.df <- get.prior.samples(tfs.conc.prior.df)

# Compute confidence intervals for the experimental data.
gerosa.2015.conc.df <- gerosa.2015.conc.df %>%
  filter(is.finite(Mean)) %>%
  mutate(Min = pmax(Mean - Std * confidence.mult, 1e-7),
         Max = Mean + Std * confidence.mult)

# Build metabolite/condition lists.
common.mets.df <- inner_join(
  tfs.conc.samples.df %>% 
    select(Metabolite, Condition) %>%
    distinct(), 
  gerosa.2015.conc.df %>%
    select(Metabolite, Condition))

example.mets.df <- data.frame(
  Metabolite = factor(
    c('prior', 'gtp_c', 'nad_c', 'nadh_c', 'glu__L_c', 'pi_c'),
    levels = met.levels),
  Condition = factor(
    c('prior', 'succ', 'succ', 'succ', 'succ', 'succ'),
    levels = levels(gerosa.2015.conc.df$Condition)))

# Samples and bounds for the example metabolites.
example.tfs.samples.df <- rbind(inner_join(
  tfs.conc.samples.df, example.mets.df), prior.samples.df) %>%
  mutate(Metabolite = factor(Metabolite, levels = rev(example.mets.df$Metabolite)))
example.tfs.bounds.df <- get.median.and.quantiles(
  example.tfs.samples.df, 'Metabolite', confidence.interval)
example.tmfa.bounds.df <- inner_join(tmfa.conc.bounds.df, example.mets.df)
example.gerosa.2015.df <- inner_join(gerosa.2015.conc.df, example.mets.df,
                                     by = c('Metabolite', 'Condition'))
pi.conc.df <- data.frame(Metabolite = as.factor('pi_c'),
                         Min = 1e-3, Max = 55e-3, 
                         Mean = exp((log(1e-3) + log(55e-3)) / 2))

get.frequencies <- function(samples, breaks) {
  samples.table = table(cut(samples, breaks, right = FALSE))
  return(unname(samples.table) / sum(unname(samples.table)))
}

# Samples and bounds for the common metabolites.
common.tfs.samples.df <- inner_join(tfs.conc.samples.df, common.mets.df)
common.tfs.bounds.df <- get.median.and.quantiles(
  common.tfs.samples.df, 'Metabolite', confidence.interval)
common.tmfa.bounds.df <- inner_join(tmfa.conc.bounds.df, common.mets.df)

log.common.tfs.samples.df <- common.tfs.samples.df %>% 
  mutate(Value = log(Value))
log.prior.samples.df <- prior.samples.df %>%
  mutate(Value = log(Value))

###################################

# Compute the prior used for TMFA.
tmfa.prior <- data.frame(
  Condition = 'prior', Metabolite = 'prior', 
  Min = exp(tfs.conc.prior.df$LogMean - 
              tfs.conc.prior.df$LogStd * confidence.mult),
  Max = exp(tfs.conc.prior.df$LogMean + 
              tfs.conc.prior.df$LogStd * confidence.mult))
tmfa.conc.bounds.df <- rbind(tmfa.conc.bounds.df, tmfa.prior)

# Compare TFS distributions, TMFA ranges and measured concentrations ###########
plots.width <- 0.7
errorbar.width <- 0.5

examples.plot <- ggplot() + 
  geom_violin(aes(x = Metabolite, y = Value), example.tfs.samples.df,
              scale = 'width', fill = '#E69F00', color = '#E69F00', 
              width = plots.width) + 
  geom_hline(yintercept = exp(tfs.conc.prior.df$LogMean), 
             color='black', size=0.5, linetype='dashed') +
  geom_errorbar(aes(x = Metabolite, ymin = Min, ymax = Max),
                example.tmfa.bounds.df, width = errorbar.width, size=0.8,
                color='#0072B2') +
  geom_errorbar(aes(x = Metabolite, ymin = Min, ymax = Max),
                example.tfs.bounds.df, width = errorbar.width, size=0.8,
                color='orangered4') +
  geom_crossbar(aes(x = Metabolite, y = Mean, ymin = Min, ymax = Max), 
                example.gerosa.2015.df, width = plots.width, size=0.5) +
  geom_crossbar(aes(x = Metabolite, y = Mean, ymin = Min, ymax = Max), 
                pi.conc.df, width = plots.width, size=0.5, color = 'gray33') +
  scale_y_continuous(trans = log_trans(),
                     limits = c(1e-7, 1e0), 
                     breaks = c(1e-5, 1e-3, 1e-1),
                     labels = trans_format("log10", math_format(10^.x))) + 
  scale_x_discrete(labels = c('Pi', 'Glu', 'NADH', 'NAD', 'GTP', 'Prior')) +
  coord_flip() + 
  labs(x = 'Metabolite', y = 'Concentration (M)') +
  paper.theme()
plot(examples.plot)

# Compare width of TFS and TMFA ranges #########################################
tfs.vs.tmfa.ranges.df <- inner_join(
  common.tfs.bounds.df  %>% rename(TFS.Min = Min, TFS.Max = Max), 
  common.tmfa.bounds.df %>% rename(TMFA.Min = Min, TMFA.Max = Max)) %>%
  mutate(
    TFS.width = log10(TFS.Max) - log10(TFS.Min),
    TMFA.width = log10(TMFA.Max) - log10(TMFA.Min)) %>%
  mutate(Width.Diff = TMFA.width - TFS.width)

tfs.vs.tmfa.ranges.diff.plot <- ggplot(tfs.vs.tmfa.ranges.df, aes(
  x = Width.Diff, colour = Condition)) + 
  geom_hline(yintercept = nlevels(droplevels(tfs.vs.tmfa.ranges.df$Metabolite)) / 2, 
             color='black', size=0.5, linetype='dashed') +
  stat_bin(data=subset(tfs.vs.tmfa.ranges.df, Condition == "fru"), aes(y=cumsum(..count..)), geom="step", bins = 200) +
  stat_bin(data=subset(tfs.vs.tmfa.ranges.df, Condition == "glyc"), aes(y=cumsum(..count..)), geom="step", bins = 200) +
  stat_bin(data=subset(tfs.vs.tmfa.ranges.df, Condition == "pyr"), aes(y=cumsum(..count..)), geom="step", bins = 200) +
  stat_bin(data=subset(tfs.vs.tmfa.ranges.df, Condition == "succ"), aes(y=cumsum(..count..)), geom="step", bins = 200) +
  scale_fill_brewer(palette = 'Set1') +
  scale_x_continuous(breaks = c(0, 0.5, 1, 1.5)) + 
  labs(x = 'Range reduction (log10(M))', y = 'Estimates count') +
  paper.theme()
plot(tfs.vs.tmfa.ranges.diff.plot)

# Compare TFS medians and measured concentrations ##############################
KL.threshold <- 0.2

binning.range <- range(c(
  log.prior.samples.df$Value, log.common.tfs.samples.df$Value))
bin.breaks = seq(
  from = binning.range[1], to = binning.range[2], length.out = 100)

KL.divergences <- log.common.tfs.samples.df %>%
  group_by(Metabolite, Condition) %>%
  summarise(KL.div = suppressMessages(distance(rbind(
    get.frequencies(Value, bin.breaks),
    get.frequencies(log.prior.samples.df$Value, bin.breaks)),
    method = "kullback-leibler"))) %>%
  ungroup()

# Merge concentration data and divergences in a single dataframe.
tfs.vs.gerosa.conc.df <- inner_join(
  common.tfs.bounds.df  %>% rename(T.Median = Median, T.Min = Min, T.Max = Max), 
  gerosa.2015.conc.df %>% rename(G.Mean = Mean, G.Min = Min, G.Max = Max))
tfs.vs.gerosa.conc.df <- inner_join(tfs.vs.gerosa.conc.df, KL.divergences) %>%
  mutate(Divergence = factor(ifelse(KL.div >= KL.threshold, 'high', 'low')))

cor.result.all <- cor.test(log(tfs.vs.gerosa.conc.df$T.Median), 
                           log(tfs.vs.gerosa.conc.df$G.Mean), method = "pearson")
tfs.vs.gerosa.conc.high.df <- tfs.vs.gerosa.conc.df %>% filter(Divergence == 'high')
cor.result.high <- cor.test(log(tfs.vs.gerosa.conc.high.df$T.Median), 
                            log(tfs.vs.gerosa.conc.high.df$G.Mean), method = "pearson")

fancy_scientific <- function(l) {
  l <- signif(l, digits = 2)
  # turn in to character string in scientific notation
  l <- format(l, scientific = TRUE)
  # quote the part before the exponent to keep all the digits
  l <- gsub("^(.*)e", "'\\1'e", l)
  # turn the 'e+' into plotmath format
  l <- gsub("e", "%*%10^", l)
  # return this as an expression
  parse(text=l)
}

# Credits: https://stackoverflow.com/questions/37583715/round-up-values-to-a-specific-significant-figure-in-r
signif_up <- function(x) {
  if (isTRUE(all.equal(x,0))) return (x)
  decs <- 10^floor(log10(abs(x)))
  ceiling(x/decs)*decs
}

limits = c(2e-6, 1e-1)
breaks = c(1e-5, 1e-3, 1e-1)
tfs.vs.gerosa.conc.plot <- ggplot(tfs.vs.gerosa.conc.df, aes(
  x = T.Median, xmin = T.Min, xmax = T.Max, ymin = G.Min, ymax = G.Max, 
  y = G.Mean, label = Metabolite, color = Condition)) +
  geom_vline(xintercept = exp(tfs.conc.prior.df$LogMean), 
             color='black', size=0.7, linetype='dashed') +
  geom_abline(intercept = 0, size=0.7, color = 'gray50') +
  geom_point(data = tfs.vs.gerosa.conc.df %>% filter(Divergence == 'low'),
             size = 1.8, shape = 3) +
  geom_point(data = tfs.vs.gerosa.conc.df %>% filter(Divergence == 'high'), 
             size = 1.8) +
  scale_x_continuous(trans = log_trans(),
                     limits = limits, breaks = breaks,
                     labels = trans_format("log10", math_format(10^.x))) +
  scale_y_continuous(trans = log_trans(),
                     limits = limits, breaks = breaks,
                     labels = trans_format("log10", math_format(10^.x))) +
  scale_color_brewer(palette = 'Set1') +
  labs(x = 'Predicted (M)', y = 'Measured (M)') +
  annotate('text', x = 1e-2, y = 8e-5, size = 2.5, label = sprintf(
    'italic(r) == %.2f', cor.result.all$estimate), parse = TRUE) + 
  annotate('text', x = 1e-2, y = 2e-5, size = 2.5, label = sprintf(
    'italic(r) == %.2f', cor.result.high$estimate), parse = TRUE) + 
  annotate('text', x = 1e-2, y = 4e-6, size = 2.5, label = sprintf(
    'italic(p) <= %s', fancy_scientific(signif_up(
      max(cor.result.all$p.value, cor.result.high$p.value)))), 
    parse = TRUE) + 
  paper.theme()
plot(tfs.vs.gerosa.conc.plot)

# Compute overlapping between measure distributions and TFS/TMFA ###############
log.gerosa.2015.samples.df <- gerosa.2015.conc.df %>%
  group_by(Metabolite, Condition) %>%
  do(cbind(., Value = log(rnorm(10000, unique(.$Mean), unique(.$Std))))) %>%
  filter(is.finite(Value)) %>%
  select(Metabolite, Condition, Value)

log.common.tmfa.samples.df <- common.tmfa.bounds.df %>%
  group_by(Metabolite, Condition) %>%
  do(cbind(., Value = runif(10000, log(unique(.$Min)), log(unique(.$Max))))) %>%
  select(Metabolite, Condition, Value)

conc.range <- range(c(log.gerosa.2015.samples.df$Value, 
                      log.common.tfs.samples.df$Value,
                      log.common.tmfa.samples.df$Value), na.rm = TRUE)
bin.breaks = seq(from = conc.range[1], to = conc.range[2], length.out = 100)

hellinger.tfs.df <- log.common.tfs.samples.df %>%
  group_by(Metabolite, Condition) %>%
  summarise(Hellinger = (suppressMessages(distance(rbind(
    get.frequencies(Value, bin.breaks),
    get.frequencies((log.gerosa.2015.samples.df %>% filter(
      Condition == unique(.$Condition) && 
      Metabolite == unique(.$Metabolite)))$Value, bin.breaks)),
    method = "hellinger")) / 2) ^ 2) %>% # philentropy has an unconventional way of computing the hellinger distance
  ungroup()

hellinger.tmfa.df <- log.common.tmfa.samples.df %>%
  group_by(Metabolite, Condition) %>%
  summarise(Hellinger = (suppressMessages(distance(rbind(
    get.frequencies(Value, bin.breaks),
    get.frequencies((log.gerosa.2015.samples.df %>% filter(
      Condition == unique(.$Condition) && 
      Metabolite == unique(.$Metabolite)))$Value, bin.breaks)),
    method = "hellinger")) / 2) ^ 2) %>%
  ungroup()

hellinger.df <- inner_join(
  hellinger.tfs.df %>% rename(Hellinger.TFS = Hellinger),
  hellinger.tmfa.df %>% rename(Hellinger.TMFA = Hellinger)) %>%
  select(-Condition) %>%
  pivot_longer(-Metabolite, names_to = 'Method', values_to = 'Distance')

tfs.vs.tmfa.plot <- ggplot(hellinger.df, aes(x = Distance, fill = Method)) +
  geom_histogram(binwidth = 0.05, position = position_identity()) +
  scale_fill_brewer(palette = 'Set1') +
  geom_abline(intercept = 0, size=0.7, color = 'gray50') +
  paper.theme()
plot(tfs.vs.tmfa.plot)

# Plot the expected normalized distance between predictions and estimates ######
expected.normalized.diff.single <- function(
  log.min, log.mean, log.max, meas.mean, meas.std) {
  log.m.samples = log(rtruncnorm(10000, a = 1e-8, mean = meas.mean, 
                                 sd = meas.std))
  
  difference <- mean(log(meas.mean) - log.mean)

  if (difference < 0) {
    return(-difference / (log.mean - log.min))
  } else {
    return(difference / (log.max - log.mean))
  }
}

expected.normalized.diff <- function(
  log.min, log.mean, log.max, meas.mean, meas.std) {
  return(mapply(expected.normalized.diff.single, 
                log.min, log.mean, log.max, meas.mean, meas.std))
}

ranges.comparison.df <- Reduce(
  function(x, y) merge(x, y, by = c('Metabolite', 'Condition')), 
  list(
    gerosa.2015.conc.df %>%
      rename(G.Mean = Mean, G.Std = Std) %>%
      filter(!is.nan(G.Mean)),
    tfs.conc.samples.df  %>% 
      group_by(Condition, Metabolite) %>%
      summarise(TFS.LogMin = log(quantile(Value, (1 - confidence.interval) / 2)),
                TFS.LogMean = log(mean(Value)),
                TFS.LogMax = log(quantile(Value, (1 + confidence.interval) / 2))) %>%
      ungroup(),
    tmfa.conc.bounds.df  %>% 
      mutate(TMFA.LogMin = log(Min),
             TMFA.LogMean = (log(Max) + log(Min)) / 2,
             TMFA.LogMax = log(Max)) %>%
      ungroup() %>%
      select(-Min, -Max)))

ranges.comparison.df <- ranges.comparison.df %>%
  mutate(
    TFS.diff = expected.normalized.diff(
      .$TFS.LogMin, .$TFS.LogMean, .$TFS.LogMax, .$G.Mean, .$G.Std),
    TMFA.diff = expected.normalized.diff(
      .$TMFA.LogMin, .$TMFA.LogMean, .$TMFA.LogMax, .$G.Mean, .$G.Std))

ranges.quality.plot <- ggplot(ranges.comparison.df) + 
  geom_hline(yintercept=nrow(ranges.comparison.df) / 2, 
             color='black', size=0.5, linetype='dashed') +
  geom_vline(xintercept=1, color='black', size=0.5, linetype='dashed') +
  stat_bin(aes(x = TFS.diff, y=cumsum(..count..)), geom="step", bins = 200) +
  stat_bin(aes(x = TMFA.diff, y=cumsum(..count..)), geom="step", bins = 200, linetype='dashed') +
  scale_fill_brewer(palette = 'Set1') +
  scale_x_continuous(breaks = c(0, 0.5, 1)) + 
  labs(x = 'Expected normalized distance', y = 'Estimates count') +
  paper.theme()
plot(ranges.quality.plot)

plot.file.name <- file.path(root.dir, 'tfs.vs.tmfa.pdf')
ggsave(plot.file.name, device = "pdf", width = 86, 
       height = 78, units = 'mm', 
       dpi = 300, limitsize = FALSE)

# Assemble panels in a single figure ###########################################
plots.scale <- 1.4
figure.4 <- plot_grid(
  conc.plot,
  tfs.vs.tmfa.width.diff.plot,
  tfs.vs.gerosa.conc.plot,
  tfs.vs.gerosa.conc.comp.plot,
  ncol = 2,
  nrow = 2,
  labels = 'AUTO')
plot(figure.4)
plot.file.name <- file.path(root.dir, 'figure4.pdf')
ggsave(plot.file.name, device = "pdf", width = 86 * plots.scale, 
       height = 78 * plots.scale, units = 'mm', 
       dpi = 300, limitsize = FALSE)



d <-philentropy::distance(rbind(
  get.frequencies((log.gerosa.2015.samples.df %>% filter(
    Condition == 'succ' & Metabolite == 'atp_c'))$Value, bin.breaks),
  get.frequencies((log.gerosa.2015.samples.df %>% filter(
    Condition == 'glyc' & Metabolite == 'atp_c'))$Value, bin.breaks)),
  method = "hellinger")