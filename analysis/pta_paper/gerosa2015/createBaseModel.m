startcna(1);
changeCobraSolver('gurobi');

%% Load model
model = readCbModel(which('iML1515.mat'));
model = removeRxns(model, 'BIOMASS_Ec_iML1515_WT_75p37M');
model.subSystems = [model.subSystems{:}]';

% Set a 'mixed' media, containing all the substrates used in the in-vivo
% experiments and remove reactions that cannot be active in any condition.
substrateRxns = { 
    'EX_ac_e', 'EX_fru_e', 'EX_gal_e', 'EX_glc__D_e', ...
    'EX_glyc_e', 'EX_glcn_e', 'EX_pyr_e', 'EX_succ_e' };
[~, substrateRxnIds] = intersect(model.rxns, substrateRxns);
model.lb(substrateRxnIds) = -50;
model = removeRxns(model, findBlockedReaction(model));

nReactions = size(model.S, 2);
biomassRxnId = find(strcmp(model.rxns, 'BIOMASS_Ec_iML1515_core_75p37M'));
[~, substrateRxnIds] = intersect(model.rxns, substrateRxns);

%% Find removable reactions.
isRemovableRxn = ...
    strcmp(model.subSystems, 'Cell Envelope Biosynthesis') | ...
    strcmp(model.subSystems, 'Glycerophospholipid Metabolism') | ...
    strcmp(model.subSystems, 'Lipopolysaccharide Biosynthesis / Recycling') | ...
    strcmp(model.subSystems, 'Membrane Lipid Metabolism') | ...
    strcmp(model.subSystems, 'Cofactor and Prosthetic Group Biosynthesis') | ...
    strcmp(model.subSystems, 'Folate Metabolism') |...
    strcmp(model.subSystems, 'Murein Biosynthesis') | ...
    strcmp(model.subSystems, 'Murein Recycling');

isTransportExchOrBiomassRxn = ...
    strcmp(model.subSystems, 'Biomass and maintenance functions') | ...
    strcmp(model.subSystems, 'Extracellular exchange') | ...
    strcmp(model.subSystems, 'Inorganic Ion Transport and Metabolism') | ...
    strcmp(model.subSystems, 'Transport, Inner Membrane') | ...
    strcmp(model.subSystems, 'Transport, Outer Membrane') | ...
    strcmp(model.subSystems, 'Transport, Outer Membrane Porin');

isProtectedRxn = ~isRemovableRxn & ~isTransportExchOrBiomassRxn;
removableMets = findMetsFromRxns(model, model.rxns(isRemovableRxn));
removableMets = unique(cellfun(@(m) m(1:end-2), removableMets, ...
    'UniformOutput', false));

isProtectedMet = true(size(model.mets));
for removableMet = removableMets'   
    % In no reaction outside lipds or transport involves the current metabolite,
    % we can freely leave it unprotected.
    isCurrentMet = startsWith(model.mets, removableMet);
    if ~any(any(model.S(isCurrentMet, isProtectedRxn)))
        isProtectedMet(isCurrentMet) = false;
    end
end

%% Set up reactions and functionalities that must be preserved.
[~, additionalProtectedRxnIds] = intersect(model.rxns, { ...
    'OHPBAT' ... # Needed to prevent removal of physiological pathway.
});

% Protect all reactions that have no connection to unprotected mets.
[~, removableRxnIds] = intersect(model.rxns, findRxnsFromMets( ...
    model, model.mets(~isProtectedMet)));
protectedRxnIds = setdiff(1:nReactions, removableRxnIds);
protectedRxnIds = union(protectedRxnIds, additionalProtectedRxnIds);
protectedMetIds = [];

protectedFunctionalities = repmat( ...
    struct('D', zeros(21, nReactions), 'd', zeros(21, 1)), 1, 8);

gerosa2015Data = getGerosa2015data();
confidenceMultiplier = sqrt(chi2inv(0.95, 1));

for iCondition = 1:8
    data = gerosa2015Data(iCondition);
    conditionModel = model;
    
    % Requirement 1: fluxes for exchange reacitons must lie within the interval
    % measured by Gerosa et al.    
    exchangeFluxNames = { ...
        'Ace_Ex', 'Fru_Ex', 'Gal_Ex', 'Glc_Ex', 'Gly_Ex', 'Gln_Ex', ...
        'Pyr_Ex', 'Suc_Ex'};
    exchangeRxnNames = { ...
        'EX_ac_e', 'EX_fru_e', 'EX_gal_e', 'EX_glc__D_e', 'EX_glyc_e', ...
        'EX_glcn_e', 'EX_pyr_e', 'EX_succ_e' };
    nExchConstraints = numel(exchangeFluxNames);
    for iRxn = 1:nExchConstraints
        dataRxnIdx = find(strcmp(data.fluxShortNames, exchangeFluxNames{iRxn}));
        modelRxnIdx = find(strcmp(conditionModel.rxns, exchangeRxnNames{iRxn}));
        
        if data.fluxMean(dataRxnIdx) == 0 && data.fluxStd(dataRxnIdx) == 0
            conditionModel.lb(modelRxnIdx) = 0;
            conditionModel.ub(modelRxnIdx) = 0.05;
        else
            conditionModel.lb(modelRxnIdx) = data.fluxMean(dataRxnIdx) - ...
                confidenceMultiplier * data.fluxStd(dataRxnIdx);
            conditionModel.ub(modelRxnIdx) = data.fluxMean(dataRxnIdx) + ...
                confidenceMultiplier * data.fluxStd(dataRxnIdx);
        end
        
        protectedFunctionalities(iCondition) ...
            .D(iRxn, modelRxnIdx) = -1;
        protectedFunctionalities(iCondition) ...
            .D(nExchConstraints + iRxn, modelRxnIdx) = 1;
        protectedFunctionalities(iCondition) ...
            .d(iRxn) = -conditionModel.lb(modelRxnIdx);
        protectedFunctionalities(iCondition) ...
            .d(nExchConstraints + iRxn) = conditionModel.ub(modelRxnIdx);
    end
  
    % Requirement 2: the model must reach the same optimal growth under the
    % constraints above.
    result = optimizeCbModel(conditionModel);
    protectedFunctionalities(iCondition).D(21, biomassRxnId) = -1;
    protectedFunctionalities(iCondition).d(21) = -0.99 * result.f;
    fprintf('Optimium: %f\n', result.f);
end

%% Use NetworkReducer to 

% Parameters
minDegreesOfFreedom = 1;
preserveRxnsFeasibility = true;
minRxns = 1;
solverType = 3;
compressNetwork = false;
useRational = 1;

% Run network reducer
model.c = zeros(size(model.c));
cnaModel = CNAgenerateMFNetwork(CNAcobra2cna(model));
    
[reducedModel, deletedRxns, deletedMetabolites] = CNAreduceMFNetwork( ...
    cnaModel, minDegreesOfFreedom, protectedFunctionalities, ...
    protectedMetIds, protectedRxnIds, preserveRxnsFeasibility, minRxns, ...
    solverType, compressNetwork, useRational);
removedRxnNames = model.rxns(setdiff(deletedRxns, protectedRxnIds));

%% Remove reactions and store new model.
model = readCbModel(which('iML1515.mat'));
model = removeRxns(model, 'BIOMASS_Ec_iML1515_WT_75p37M');
model = removeRxns(model, removedRxnNames);

writeCbModel(model, 'fileName', fullfile(getPtaRoot(), ...
    'data/models/generated/iML1515_compressed_lipids_and_cofactors.mat'));