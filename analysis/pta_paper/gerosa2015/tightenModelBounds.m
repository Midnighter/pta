function model = tightenModelBounds(model, relativeMargin)
%TIGHTENMODELBOUNDS Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 2
        relativeMargin = 0.1;
    end

    %% Perform FVA to find tighter bounds for the reaction fluxes.
    [lb, ub] = fluxVariability(model, 0);
    
    % Add tolerance for numerical errors.
    lb = lb - abs(lb) * relativeMargin;
    ub = ub + abs(ub) * relativeMargin;
    
    % Small positive lower bounds and negative upper bounds can be pushed to
    % zero, this will improve numerics.
    lb(0 < lb & lb < 1e-2) = 0;
    ub(-1e-2 < ub & ub < 0) = 0;
    
    model.lb = max(model.lb, lb);
    model.ub = min(model.ub, ub);
end

