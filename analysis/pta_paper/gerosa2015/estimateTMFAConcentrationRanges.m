function estimateTMFAConcentrationRanges(settings)

    %% Run estimation on selected models.
    for iCondition = settings.conditions
        %% Load experimental data.
        gerosa2015data = getGerosa2015data();
        data = gerosa2015data(iCondition);
        fprintf('Estimating TMFA ranges for substrate: %s.\n', ...
            data.conditionName);
        
        conditionSettings = replaceRecursive( ...
            settings, '${condition}', data.conditionShortName);
        [~, ~] = mkdir(conditionSettings.tmfa.resultsDir);
        modelName = ['e_coli_gerosa2015_' data.conditionShortName];

        %% Run TMFA estimation on the current model.
        try
            runTMFA(modelName, conditionSettings);
        catch exception
            warning('Estimation failed for condition %i.', iCondition);
            disp(getReport(exception));
        end
    end
end

function runTMFA(modelName, settings)
    
    %% Load the model
    modelFileName = fullfile(settings.modelsDir, [modelName '_td.mat']);
    model = load(modelFileName, 'model');
    model = model.model;
    confidence = settings.tmfa.confidence;
    
    %% Create TMFA model.
    model = prepModelforTFA_hf(model, confidence);
    model = convToTFA(model,  confidence, [], 'DGo', [], 0);
    model = addNetFluxVariables(model);
    
    %% Compute concentration ranges.
    concVarsIds = getAllVar(model, {'LC'});
    concentrationRanges = runTMinMax(model, model.varNames(concVarsIds));
    
    % Store results for later analysis.
    resultsFileName = fullfile( ...
        settings.tmfa.resultsDir, ['tmfa_' modelName '.mat']);
    mets = model.mets;
    save(resultsFileName, 'concentrationRanges', 'mets');
end