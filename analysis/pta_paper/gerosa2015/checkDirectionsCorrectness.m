function isCorrect = checkDirectionsCorrectness(model, data)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    value = model.lb(strcmp(model.rxns, 'CO2tpp'));
    if ~isempty(value) && value >= 0
        isCorrect = false;
        return;
    end
    
    value = model.ub(strcmp(model.rxns, 'GHMT2r'));
    if ~isempty(value) && value <= 0
        isCorrect = false;
        return;
    end

    LHS = data.getRxnToFluxMapping(model.rxns);
    nConstraints = size(LHS, 1);
    RHS = ones(nConstraints, 1) * -1e-4;
    sense = repmat('L', nConstraints, 1);
    sense(data.fluxMean > 0) = 'G';
    RHS(data.fluxMean > 0) = -RHS(data.fluxMean > 0);
    toRemove = data.fluxMean == 0;
    
    LP = buildLPproblemFromModel(model);
    LP.A = [LP.A; LHS(~toRemove, :)];
    LP.b = [LP.b; RHS(~toRemove)];
    LP.csense = [LP.csense; sense(~toRemove)];
    LP.c = zeros(size(LP.c));
    
    result = solveCobraLP(LP);
    isCorrect = result.stat == 1;
end