function  structure = applyRecursive(structure, fieldFunc)
% Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    fields = fieldnames(structure);
    for iField = 1:numel(fields)
        if isstruct(structure.(fields{iField}))
            structure.(fields{iField}) = applyRecursive( ...
                structure.(fields{iField}), fieldFunc);
        else
            structure.(fields{iField}) = fieldFunc(structure.(fields{iField}));
        end
    end
end