function [metNames, coefficients] = getRateDependentBiomassComposition(growthRate)

%  Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
%  Computational Systems Biology group, ETH Zurich
%
%  This software is freely available under the GNU General Public License v3.
%  See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    metNames = { ...
        '3pg_c', 'accoa_c', 'atp_c', 'e4p_c', 'f6p_c', 'g3p_c', 'g6p_c', ...
        'h2o_c', 'nad_c', 'nadph_c', 'oaa_c', 'pep_c', 'pyr_c', 'r5p_c', ... 	
        'adp_c', 'akg_c', 'coa_c', 'h_c', 'nadh_c', 'nadp_c', 'pi_c'}';
    getMetIdx = @(name) find(strcmp(metNames, name));
    
    compositions = [
        % ATP	G6P     PEP     PYR     F6P     GA3P	PGA     NADH	NADPH	CO2     R5P     E4P     OAA     AcCoA	OGA
        7.087	0.000	0.722	2.550	0.000	-0.054	0.953	-1.071	9.864	-1.940	0.144	0.361	1.218	0.428	1.220;
        6.380	0.000	0.000	0.000	0.000	0.000	0.598	-1.333	1.135	0.359	0.615	0.000	0.256	0.000	0.000;
        1.026	0.000	0.000	0.000	0.000	0.000	0.050	-0.200	0.300	0.050	0.100	0.000	0.050	0.000	0.000;
        1.692	0.000	0.000	0.332	0.000	0.159	0.095	-0.198	3.497	-0.562	0.000	0.000	0.102	1.557	0.000;
        0.490	0.054	0.025	0.000	0.017	0.017	0.025	-0.025	0.499	0.000	0.025	0.000	0.000	0.249	0.000;
        0.246	0.000	0.027	0.082	0.055	0.000	0.000	0.000	0.191	0.000	0.000	0.000	0.000	0.082	0.027;
        0.062	0.062	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.000;
        0.083	0.000	0.000	0.000	0.000	0.000	0.000	0.000	0.124	0.000	0.000	0.000	0.000	0.000	0.041;
    ];

    weigths = [
        ((-0.26384 * growthRate + 0.72754) / 0.55); % Proteins
        ((0.2671 * growthRate + 0.04716) / 0.20);   % RNA
        1                                           % DNA
        1                                           % Lipids
        1                                           % Lipopolysaccharides
        1                                           % Peptidoglycanes
        1                                           % Glycogen
        1                                           % Polyamine
    ]';

    composition = (weigths * compositions)';
    compositionToMets = zeros(numel(metNames), size(compositions, 2));
    
    compositionToMets(getMetIdx('atp_c'), 1) = -1;
    compositionToMets(getMetIdx('h2o_c'), 1) = -1;
    compositionToMets(getMetIdx('adp_c'), 1) = 1;
    compositionToMets(getMetIdx('h_c'), 1) = 1;
    compositionToMets(getMetIdx('pi_c'), 1) = 1;
    
    compositionToMets(getMetIdx('g6p_c'), 2) = -1;
    compositionToMets(getMetIdx('pep_c'), 3) = -1;
    compositionToMets(getMetIdx('pyr_c'), 4) = -1;
    compositionToMets(getMetIdx('f6p_c'), 5) = -1;
    compositionToMets(getMetIdx('g3p_c'), 6) = -1;
    compositionToMets(getMetIdx('3pg_c'), 7) = -1;
    
    compositionToMets(getMetIdx('nadh_c'), 8) = -1;
    compositionToMets(getMetIdx('nad_c'), 8) = 1;
    
    compositionToMets(getMetIdx('nadph_c'), 9) = -1;
    compositionToMets(getMetIdx('nadp_c'), 9) = 1;
    
    compositionToMets(getMetIdx('co2_c'), 10) = -1;
    compositionToMets(getMetIdx('r5p_c'), 11) = -1;
    compositionToMets(getMetIdx('e4p_c'), 12) = -1;
    compositionToMets(getMetIdx('oaa_c'), 13) = -1;
    
    compositionToMets(getMetIdx('accoa_c'), 14) = -1;
    compositionToMets(getMetIdx('coa_c'), 14) = 1;
    
    compositionToMets(getMetIdx('akg_c'), 15) = -1;
    
    coefficients = compositionToMets * composition;
end