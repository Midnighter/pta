changeCobraSolver('gurobi');

wof = printModelStatistics('hyperflux/data/results/iML1515_CAN_without_metabolomics', '_flux')
wif = printModelStatistics('hyperflux/data/results/iML1515_CAN_with_metabolomics', '_flux')

wou = printModelStatistics('hyperflux/data/results/iML1515_CAN_without_metabolomics', '')
wiu = printModelStatistics('hyperflux/data/results/iML1515_CAN_with_metabolomics', '')

wot = printModelStatistics('hyperflux/data/results/iML1515_CAN_without_metabolomics', '_td')
wit = printModelStatistics('hyperflux/data/results/iML1515_CAN_with_metabolomics', '_td')

function stats = printModelStatistics(root, suffix)
    gerosa2015data = getGerosa2015data();
    CI = 0.95;
    ciMultiplier = sqrt(chi2inv(CI, 1));
    
    conditions = dir(root);
    conditions = conditions([conditions.isdir]);
    conditions = {conditions.name};
    conditions = conditions(~strcmp(conditions, '.') & ~strcmp(conditions, '..'));
    
    % Conditions, rxns, revRxns, rev13Cfluxes, incorrectDirs, mets, gamma, dim(m)
    stats = cell(numel(conditions), 8);
    
    for iCondition = 1:numel(conditions)
        data = gerosa2015data(strcmp({gerosa2015data.conditionShortName}, ...
            conditions{iCondition}));   
        
        stats{iCondition, 1} = conditions{iCondition};
        modelsPath = fullfile(root, conditions{iCondition}, 'models');
        modelFile = fullfile(modelsPath, ['e_coli_gerosa2015_' conditions{iCondition} suffix '.mat']);
        model = readCbModel(modelFile);
        stats{iCondition, 2} = numel(model.rxns);
        
        model = tightenModelBounds(model);
        stats{iCondition, 3} = sum(model.lb < 0 & model.ub > 0);
        
        nRevFluxes = 0;
        nIncorrectDirs = 0;
        c13lb = data.fluxMean - ciMultiplier * data.fluxStd;
        c13ub = data.fluxMean + ciMultiplier * data.fluxStd;
        fluxMapping = data.getRxnToFluxMapping(model.rxns); 
        
        for iFlux = 1:numel(data.fluxShortNames)
            tmpModel = model;
            tmpModel.c = fluxMapping(iFlux, :)';
            maxResult = optimizeCbModel(tmpModel, 'max');
            minResult = optimizeCbModel(tmpModel, 'min');
            
            if maxResult.obj > 0 && minResult.obj < 0
                nRevFluxes = nRevFluxes + 1;
            end
            
            if (maxResult.obj <= 0 && c13lb(iFlux) > 0) || ...
                    (minResult.obj >= 0 && c13ub(iFlux) < 0)
                nIncorrectDirs = nIncorrectDirs + 1;
            end
        end
        
        stats{iCondition, 4} = nRevFluxes;
        stats{iCondition, 5} = nIncorrectDirs;
        
        stats{iCondition, 6} = numel(model.mets);
        if isfield(model, 'isConstraintRxn')
            stats{iCondition, 7} = sum(model.isConstraintRxn);
        else
            stats{iCondition, 7} = 0;
        end
        
        if strcmp(suffix, '_td')
            settings.sampleConcentrations = false;
            settings.minDimensionEigenvalue = 1e-3;
            settings.freeEnergyVarsConfidence = 0.95;
            g = GibbsFreeEnergiesSpace(model, settings);
            
            stats{iCondition, 8} = size(g.standardMvnToDrgMvnTransform.T, 2);
        else
            stats{iCondition, 8} = 0;
        end
    end
end