function createGerosa2015EColiModels(settings)
% Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    data = getGerosa2015data();
    
    %% Create models for each condition .
    for iCondition = settings.conditions
        %% Set up paths.
        conditionSettings = replaceRecursive( ...
            settings, '${condition}', data(iCondition).conditionShortName);
        [~, ~] = mkdir(conditionSettings.modelsDir);
        
        try
            createModelsForCondition(data(iCondition), conditionSettings);
        catch exception
            warning('Failed to create model for condition %i.', iCondition);
            disp(getReport(exception));
        end
    end
end

function model = compressModelCallback(model, settings)
    findThermodynamicallyConflictingIrreversibilities( ...
        model, settings.modelDiagnostics);
end

function createModelsForCondition(data, settings)

    %% Create callbacks for custom model building actions.
    callbacks = {};
    if settings.addExchangeBounds
        callbacks = [callbacks, { 
            'applyBoundsCallback', ...
            @(model) setExchangeAndGrowthBounds( ...
                model, data, settings)
        }];
    end
    if settings.modelDiagnostics.findStructuralInconsistencies
        callbacks = [callbacks, { 
            'compressModelCallback', ...
            @(model) compressModelCallback(model, settings)
        }];
    end
    callbacks = [callbacks, {
    	'addMetaboliteConcentrationsCallback', ...
         @(model) setConcentrationPriors(model, data, settings)
    }];
        
    %% Load and build the models.        
    if settings.thermodynamicSampling.enabled
        thermodynamicsBaseModel = cobragen.generateCobraModelFromYaml( ...
            which(settings.thermodynamicSampling.modelDefinition), ...
            callbacks{:}, 'concentrationsPriorSource', ...
            settings.concentrationsPriorSource);
        createConditionModel(thermodynamicsBaseModel, ...
            data, settings.modelsDir, settings, ...
            settings.modelDiagnostics.enabled, false, '_td');
    end

    if settings.uniformFluxSampling.enabled
        uniformBaseModel = cobragen.generateCobraModelFromYaml( ...
            which(settings.uniformFluxSampling.modelDefinition), ...
            callbacks{:});
        createConditionModel(uniformBaseModel, data, ...
            settings.modelsDir, settings, false, true, '');
    end
end

function model = setExchangeAndGrowthBounds(model, data, settings)

    % Set rate dependent biomass composition according to Gerosa 2015.
    if settings.useCustomBiomassRxn
        isBiomassRxn = strcmp(model.rxns, 'BIOMASS');
        [biomassMetNames, biomassComposition] = ...
            getRateDependentBiomassComposition(data.mu);
        model.S(:, isBiomassRxn) = 0;
        for metId = 1:numel(biomassMetNames)
            model.S(strcmp(model.mets, biomassMetNames(metId)), isBiomassRxn) = ...
                biomassComposition(metId);
        end
    end
    
    confidenceMultiplier = sqrt(chi2inv(settings.measurementsConfidence, 1));    
    exchangeFluxNames = { ...
        'Ace_Ex', 'Fru_Ex', 'Gal_Ex', 'Glc_Ex', 'Gly_Ex', 'Gln_Ex', ...
        'Pyr_Ex', 'Suc_Ex', 'Fum_Ex', 'Lac_Ex', 'Growth_rate' };
    exchangeRxnNames = { ...
        'EX_ac_e', 'EX_fru_e', 'EX_gal_e', 'EX_glc__D_e', 'EX_glyc_e', ...
        'EX_glcn_e', 'EX_pyr_e', 'EX_succ_e', 'EX_fum_e', 'EX_lac__D_e', ...
        'BIOMASS' };
    nReactions = numel(model.rxns);
    model.vMean = nan(nReactions, 1);
    model.vCov = nan(nReactions, nReactions);
    dataRxnIds = nan(numel(exchangeFluxNames) + 1, 1);
    modelRxnIds = nan(numel(exchangeFluxNames) + 1, 1);
    
    for iRxn = 1:numel(exchangeFluxNames)
        dataRxnIdx = find(strcmp(data.fluxShortNames, exchangeFluxNames{iRxn}));
        modelRxnIdx = find(strcmp(model.rxns, exchangeRxnNames{iRxn}));
        
        if data.fluxMean(dataRxnIdx) == 0 && data.fluxStd(dataRxnIdx) == 0
            model.lb(modelRxnIdx) = 0;
            model.ub(modelRxnIdx) = settings.detectionThreshold;
        else
            model.lb(modelRxnIdx) = data.fluxMean(dataRxnIdx) - ...
                confidenceMultiplier * data.fluxStd(dataRxnIdx);
            model.ub(modelRxnIdx) = data.fluxMean(dataRxnIdx) + ...
                confidenceMultiplier * data.fluxStd(dataRxnIdx);
            dataRxnIds(iRxn) = dataRxnIdx;
            modelRxnIds(iRxn) = modelRxnIdx;
        end
    end
    
    dataRxnIds = dataRxnIds(isfinite(dataRxnIds));
    modelRxnIds = modelRxnIds(isfinite(modelRxnIds));
    model.vMean(modelRxnIds) = data.fluxMean(dataRxnIds);
    model.vCov(modelRxnIds, modelRxnIds) = diag(data.fluxStd(dataRxnIds).^2);
    
    model = removeRxns(model, findBlockedReaction(model));
    conflicts = []; % findThermodynamicallyConflictingIrreversibilities(model);
    if numel(conflicts) ~= 0
        warning('Model may contain thermodynamically conflicting directions.');
    end
end

function model = setConcentrationPriors(model, data, settings)
    % Add concentration of the carbon source.
    isSource = strcmp(model.mets, data.supplementedCarbonSource);
    model.logConcMean(isSource) = ...
        log(5 / data.supplementedCarbonSourceMolarWeight);
    model.logConcCov(isSource, isSource) = 0;
    
    % If required, add measured concentrations.
    if settings.addMeasuredConcentrations
        model = setMeasuredConcentrations(model, data);
    end
    % If required, add the assumed concentrations of a few ions.
    if settings.addCommonConcentrations
        model = setCommonConcentrations(model);
    end
end

function model = setMeasuredConcentrations(model, data)
    for iMetData = 1:numel(data.concShortNames)
        modelMetIdx = find(strcmp(model.mets, data.concShortNames{iMetData}));
        if ~isfinite(data.concMean(iMetData)) || ...
           ~isfinite(data.concStd(iMetData)) || numel(modelMetIdx) ~= 1
            continue;
        end
        
        [model.logConcMean(modelMetIdx), metLogStd] = ...
            normalToLogNormal(data.concMean(iMetData), data.concStd(iMetData));
        model.logConcCov(modelMetIdx, modelMetIdx) = metLogStd^2;
    end
end

function model = setCommonConcentrations(model)
    data = getCommonConcentrations();
    for iMetData = 1:numel(data.mets)
        modelMetIdx = find(strcmp(model.mets, data.mets{iMetData}));
        if ~isfinite(data.means(iMetData)) || ...
           ~isfinite(data.logStds(iMetData)) || numel(modelMetIdx) ~= 1
            continue;
        end
        
        model.logConcMean(modelMetIdx) = log(data.means(iMetData));
        model.logConcCov(modelMetIdx, modelMetIdx) = data.logStds(iMetData)^2;
    end
end

function createConditionModel( ...
    baseModel, data, outputPath, settings, ...
    runModelDiagnostics, applyTMFA, suffix)
    
    model = baseModel;
    modelName = ['e_coli_gerosa2015_' data.conditionShortName suffix];
    outputFileName = fullfile(outputPath, [modelName '.mat']);
    
    model = tightenModelBounds(model);
         
    %% Add standard free energies estimates to the model.
    if ~strcmp(suffix, '_flux')
        model = cobragen.addThermodynamicDataToModel(model);
    end
    
    if runModelDiagnostics
        outputPath = settings.modelDiagnostics.outputFolder;
        samplerSettings = settings ...
            .thermodynamicSampling.freeEnergySampling.samplerSettings;
        samplerSettings.gurobiSettings.outputflag = 1;
        samplerSettings.gurobiSettings.Threads = 0;
        runThermodynamicModelAssessment(model, outputPath, ...
            samplerSettings, ...
            settings.thermodynamicSampling.freeEnergySpace);
    end
    
    if applyTMFA
       model = applyTMFAFluxConstraints(model, settings.tmfa);
    end
    
    save(outputFileName, 'model');
end