function structure = replaceRecursive(structure, substring, newString)
% Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    structure = applyRecursive(structure, ...
        @(x) replaceIfString(x, substring, newString)); 
end

function object = replaceIfString(object, subString, newString)
    if ischar(object) || isstring(object)
        object = string(strrep(object, subString, newString));
    end
end