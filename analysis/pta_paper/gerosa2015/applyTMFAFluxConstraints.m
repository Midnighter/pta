function model = applyTMFAFluxConstraints(model, tmfaSettings)

    confidence = tmfaSettings.confidence;
    [~, ~] = mkdir(tmfaSettings.resultsDir);
    outputFileName = fullfile(tmfaSettings.resultsDir, 'irreversibilities.txt');

    %% Create TMFA model.
    modelTFA = prepModelforTFA_hf(model, confidence);
    modelTFA = convToTFA(modelTFA,  confidence, [], 'DGo', [], 0);
    modelTFA = addNetFluxVariables(modelTFA);
    
    %% Compute concentration ranges.
    fluxVarsIds = getAllVar(modelTFA, {'NF'});
    fluxRanges = runTMinMax(modelTFA, modelTFA.varNames(fluxVarsIds));
    
    isBlocked = fluxRanges(:, 1) == 0 & fluxRanges(:, 2) == 0;
    isNewIrreversibility = ((model.lb < 0 & fluxRanges(:, 1) >= 0) | ...
        (model.ub > 0 & fluxRanges(:, 2) <= 0)) & ~isBlocked;
    
    outputFile = fopen(outputFileName, 'w');
    
    fprintf(outputFile, 'Irreversible reactions:\n');
    if any(isNewIrreversibility)
        fprintf('New thermodynamic irreversibilities found by TMFA:');
        fprintf(' %s', model.rxns{isNewIrreversibility});
        fprintf(outputFile, ' %s', model.rxns{isNewIrreversibility});
        fprintf('\n');
        model.lb(isNewIrreversibility) = max( ...
            model.lb(isNewIrreversibility), fluxRanges(isNewIrreversibility, 1));
        model.ub(isNewIrreversibility) = min( ...
            model.ub(isNewIrreversibility), fluxRanges(isNewIrreversibility, 2));
    else
        fprintf('No additional thermodynamic irreversibility found.\n');
    end
    
    fprintf(outputFile, '\nBlocked reactions:\n');
    if any(isBlocked)
        fprintf('New blocked reactions found by TMFA:');
        fprintf(' %s', model.rxns{isBlocked});
        fprintf(outputFile, ' %s', model.rxns{isBlocked});
        fprintf('\n');
        model.lb(isBlocked) = max( ...
            model.lb(isBlocked), fluxRanges(isBlocked, 1));
        model.ub(isBlocked) = min( ...
            model.ub(isBlocked), fluxRanges(isBlocked, 2));
    else
        fprintf('No additional blocked reactions found.\n');
    end
    
    fclose(outputFile);
end