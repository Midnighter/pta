function data = getCommonConcentrations()
% Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    data.mets = { 'pi_c' };
    data.means = [25e-3];
    data.logStds = [0.2];
end