################################################################################
# Experimental settings.
conditions: [1]
measurementsConfidence: 0.95
detectionThreshold: 0.5          # Assumed. Not reported in the paper.

addExchangeBounds: true
addMeasuredConcentrations: true
addCommonConcentrations: true
useCustomBiomassRxn: false
concentrationsPriorSource: "gerosa2015AcGlc"

################################################################################
# Paths.
rootDataDir: "${pta}/data/results/iML1515_diagnostics_GNG_channeling"
modelsDir: "${root}/${condition}/models"
samplesDir: "${root}/${condition}/samples"

################################################################################
# System settings.

# Only affects calls to COBRA functions. PMO and TFS currently support only 
# Gurobi.
cobraSolver: "gurobi"

################################################################################
# Model diagnostics
modelDiagnostics : {
    enabled: true,
    findStructuralInconsistencies: false,
    outputFolder: "${root}/${condition}/model_diagnostics",
}

################################################################################
# Uniform flux sampling.
uniformFluxSampling: {
    enabled: false,
}

################################################################################
# Thermodynamics-based flux sampling.
thermodynamicSampling: {
    enabled: true,
    modelDefinition: iML1515_gerosa_2015_quantitative_corrections_no_GNG_channeling.yaml,
    nSamplesPerDirectionSample: 5,

    freeEnergySpace: {
        sampleConcentrations: false,
        freeEnergyVarsConfidence: 0.95,
        minDimensionEigenvalue: 1e-4,
    },

    freeEnergySampling: {
        saveBenchmarkData: false,
        cache: {
            file: "${root}/${condition}/free_energy_sampling_cache.mat",
            useInitialPoints: true,
            useDirectionsDistribution: false,
        },

        adaptiveDirectionsDistribution: {
            enabled: true,
            nIterations: 9,
            nStepsPerIteration: 500000,
            stepsMultiplier: 1.4,
            outputFolder: "${root}/${condition}/adaptive_directions_distribution",
            guessScaleMultiplier: 8,
            updateScaleMultiplier: 0.7,
        },

        samplerSettings: {
            nSamples: 1000,          # Automatically set to nChains * nRecordedSteps.
            nChains: 200,
            nSteps: 100000000,
            nWarmupSteps: .nan,     # Automatically set to half the chain length.
            nRecordedSteps: 200,
            maxWorkers: .inf,
            logsDir: "${root}/${condition}/logs/tfs",
            consoleLoggingIntervalMs: 15000,
            feasibilityCacheSize: 100000,

            maxTestedRxnsForInitialPoints: 200,
            fluxEpsilon: 1e-4,
            drgEpsilon: 1e-1,
            drgMax: 1000,
            minRelRegionCoverage: 1e-8,
            diagnoseInfeasibilities: false,

            gurobiSettings: {
                outputflag: 0,
                IntFeasTol: 1e-9,
                FeasibilityTol: 1e-9,
                TimeLimit: 1800,
                Threads: 1,
                # NumericFocus: 3,
            },
        },

        convergenceDiagnostic: {
            enabled: true,
            outputFolder: "${root}/${condition}/convergence/tfs",
            tests: [splitR, ESS],
        },
    },

    fluxSamplingFromDirections: {
        enabled: false,
    }
}