function settings = loadGerosa2015Settings(yamlFileName)
% Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    assert(exist(yamlFileName, 'file') == 2, 'Settings file not found.');
    settings = yaml.ReadYaml(yamlFileName);
    settings = replaceRecursive(settings, '${pta}', getPtaRoot());
    settings = replaceRecursive(settings, '${root}', settings.rootDataDir);
    
    settings.thermodynamicSampling.fluxSamplingFromDirections ...
        .samplerSettings.nSamples = settings.thermodynamicSampling ...
        .freeEnergySampling.samplerSettings.nSamples * settings ...
        .thermodynamicSampling.nSamplesPerDirectionSample;
    
    settings.conditions = cell2mat(settings.conditions);
end