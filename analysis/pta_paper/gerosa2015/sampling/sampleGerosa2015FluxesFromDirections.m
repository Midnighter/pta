function sampleGerosa2015FluxesFromDirections(settings, binning)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    if nargin <= 1
        binning = 1;
    end

    if settings.thermodynamicSampling.enabled && ...
       settings.thermodynamicSampling.fluxSamplingFromTDPrior.enabled
        settings.binning = binning;
        runForEachCondition( ...
            @sampleGerosa2015FluxesFromDirectionsForCondition, settings);
    end
end

function sampleGerosa2015FluxesFromDirectionsForCondition( ...
    ~, data, h5File, matFileName, settings)

    binning = settings.binning;
    load(matFileName, 'tdModel', 'directions', 'revRxns');
    bins = linspace(0, double(max(directions.counts)), binning + 1);
    
    resultFcnHandle = @(model, workerResult, workerSamplesIds, ...
        isSamplingSuccessful, nFluxSamples) resultFunction(model, ...
        workerResult, workerSamplesIds, isSamplingSuccessful, ...
        nFluxSamples, data);
    postProcessFcnHandle = @(samples, sampleIds, model) postprocessOrthant( ...
        samples, sampleIds, model, data);
    
    distances = [];
    orthantWeights = [];
    orthants = [];
    binIds = [];
        
    for bin = 1:binning     
        
        binSettings = settings.thermodynamicSampling.fluxSamplingFromTDPrior;
        binSettings.nOrthants = floor(binSettings.nOrthants / binning);
        binSettings.nApproxSamples = floor(binSettings.nApproxSamples / binning);
        isInBin = directions.counts > bins(bin) & directions.counts <= bins(bin+1);
        binDirections.counts = directions.counts(isInBin);
        binDirections.signs = directions.signs(:, isInBin);
        
        if ~any(isInBin)
            continue;
        end
        
        [binFluxesResult, binOrthants, binOrthantWeights] = ...
            sampleFluxSpaceWithDirectionsPrior(tdModel, binDirections, ...
            revRxns, binSettings, 'minSamples', 1000, 'resultFunction', ...
            resultFcnHandle, 'postprocessFunction', postProcessFcnHandle);
        
        if bin == 1
            fluxesResult = binFluxesResult;
        end
        distances = [distances, binFluxesResult.distances]; %#ok<AGROW>
        orthantWeights = [orthantWeights, binOrthantWeights]; %#ok<AGROW>
        orthants = [orthants, binOrthants]; %#ok<AGROW>
        binIds = [binIds, ones(1, numel(orthantWeights)) * bin]; %#ok<AGROW>
    end
    
    if binning == 1
        h5WriteMatrixDataset(h5File, 'fluxes', fluxesResult.fluxSamples);
        h5WriteMatrixDataset(h5File, 'flux_distances', distances);
        h5WriteMatrixDataset(h5File, 'flux_weights', orthantWeights);
        h5WriteInt8MatrixDataset(h5File, 'flux_orthants', orthants);
    else
        h5WriteMatrixDataset(h5File, 'flux_distances_binned', distances);
        h5WriteMatrixDataset(h5File, 'flux_weights_binned', orthantWeights);
        h5WriteInt8MatrixDataset(h5File, 'flux_orthants_binned', orthants);
        h5WriteMatrixDataset(h5File, 'flux_bin_ids', binIds);
    end
    
    save(matFileName, 'fluxesResult', '-append');
end

function result = postprocessOrthant(samples, workerSampleIds, model, data)
    
    nSamples = numel(workerSampleIds);  
    mapping = data.getRxnToFluxMapping(model.rxns);
 
    sampleIds = datasample(1:size(samples, 2), nSamples, 'Replace', false);
    result.fluxSamples = samples(:, sampleIds);
            
    samplesMean = mapping * mean(samples, 2);
    result.distance = abs(samplesMean - data.fluxMean);
end

function result = resultFunction(model, workerResult, workerSamplesIds, isSamplingSuccessful, nFluxSamples, data)
    nDimensions = numel(model.lb);
    nOrthants = numel(workerResult);
    nSteps = size(workerResult{1}, 3);
    fluxSamples = nan(nDimensions, nFluxSamples, nSteps);
    
    mapping = data.getRxnToFluxMapping(model.rxns);
    distances = nan(size(mapping, 1), nOrthants);
    
    for iUniqueDirection = 1:nOrthants
        if isSamplingSuccessful(iUniqueDirection)
            workerSamples = workerResult{iUniqueDirection};
            fluxSamples(:, workerSamplesIds{iUniqueDirection}, :) = ...
                workerSamples.fluxSamples;
            distances(:, iUniqueDirection) = workerSamples.distance;
        end
    end
    
    result.fluxSamples = fluxSamples;
    result.distances = distances;
end