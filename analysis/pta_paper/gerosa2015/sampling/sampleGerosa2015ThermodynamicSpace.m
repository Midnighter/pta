function sampleGerosa2015ThermodynamicSpace(settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    if settings.thermodynamicSampling.enabled
        runForEachCondition( ...
            @sampleGerosa2015ThermodynamicSpaceForCondition, settings);
    end
end

function sampleGerosa2015ThermodynamicSpaceForCondition( ...
    ~, ~, h5File, matFileName, settings)

    % Setup the space of steady-state Gibbs energies.
    load(matFileName, 'tdModel');
    freeEnergiesSpace = GibbsFreeEnergiesSpace( ...
        tdModel, settings.thermodynamicSampling.freeEnergySpace);
    revRxns = tdModel.rxns( ...
        tdModel.lb < 0 & tdModel.ub > 0 & tdModel.isConstraintRxn);
    
    % Sample the space of steady-state free energies.
    drgSamplingResult = sampleFreeEnergies(tdModel, freeEnergiesSpace, ...
        settings.thermodynamicSampling.freeEnergySampling);
    drg = drgSamplingResult.drg.samples;
    directions = drgSamplingResult.directions;
    directionSamples = -sign(drg);
    
    % Sanity check, verify that all samples are at steady state.
    isSteadyState = isSteadyStateFluxDirection( ...
        directionSamples, tdModel.S, tdModel.lb, tdModel.ub, ...
        tdModel.isConstraintRxn);
    if ~all(isSteadyState)
        warning(['%i out of %i samples do not allow a steady state ', ...
                 'flux distibution.'], sum(~isSteadyState), ...
                 numel(isSteadyState));
    end
    
    h5WriteMatrixDataset(h5File, 'samples_td_drg', drg);
    h5WriteInt8MatrixDataset(h5File, 'direction_samples', ...
        drgSamplingResult.directions.signs);
    h5WriteUInt32MatrixDataset(h5File, 'direction_counts', ...
        drgSamplingResult.directions.counts);
    h5WriteStringArrayDataset(h5File, 'reversible_rxns', revRxns);
        
    save(matFileName, 'freeEnergiesSpace', 'drgSamplingResult', ...
        'drg', 'directions', 'revRxns', '-append');
end