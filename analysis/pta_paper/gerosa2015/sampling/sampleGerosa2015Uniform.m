function sampleGerosa2015Uniform(settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    fprintf('Sampling models uniformly... \n');
    runForEachCondition(@sampleGerosa2015UniformForCondition, settings);
end

function sampleGerosa2015UniformForCondition( ...
    ~, ~, h5File, matFileName, settings)

    if settings.uniformFluxSampling.enabled
        % Sample the flux space uniformly.
        load(matFileName, 'uniformModel');
        uniformFluxSamplingResult = sampleFluxSpaceUniform( ...
            uniformModel, settings.uniformFluxSampling);
        uniformFluxSamples = uniformFluxSamplingResult.fluxes.samples;
        
        % Store resulting samples.
        h5WriteMatrixDataset(h5File, ...
            'samples_uniform_fluxes', uniformFluxSamples);
        save(matFileName, 'uniformFluxSamplingResult', '-append');
    end
end