function sampleGerosa2015ConcentrationsAndDrg0(settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    if settings.thermodynamicSampling.enabled
        runForEachCondition( ...
            @sampleGerosa2015ConcentrationsAndDrg0ForCondition, settings);
    end
end

function sampleGerosa2015ConcentrationsAndDrg0ForCondition( ...
    ~, ~, h5File, matFileName, settings)

    load(matFileName, 'tdModel', 'drg');
    
    % Sample metabolite concentrations and standard reaction energies
    % conditioned on the sampled reaction energies.
    [logConc, drg0] = sampleLogConcAndDrg0FromDrg( ...
        drg, tdModel, settings.thermodynamicSampling.freeEnergySpace);
    activityCorrections = ...
        tdModel.S(:, tdModel.isConstraintRxn)' * logConc;
    
    h5WriteMatrixDataset(h5File, ...
        'samples_td_activity_corrections', activityCorrections);
    h5WriteMatrixDataset(h5File, ...
        'samples_td_drg0', drg0);
    h5WriteMatrixDataset(h5File, ...
        'samples_td_log_concentrations', logConc);
        
    save(matFileName, 'activityCorrections', 'drg0', 'logConc', '-append');
end