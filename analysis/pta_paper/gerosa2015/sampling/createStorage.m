function createStorage(settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    fprintf('Creating H5 datasets... \n');
    saveGerosaData(settings.rootDataDir);
    
    %% Setup HDF5 files for storing results.
    for iCondition = settings.conditions
        gerosa2015data = getGerosa2015data();
        data = gerosa2015data(iCondition);
        modelName = ['e_coli_gerosa2015_' data.conditionShortName];
        conditionSettings = replaceRecursive( ...
            settings, '${condition}', data.conditionShortName);
        [~, ~] = mkdir(conditionSettings.samplesDir);

        %% Setup MAT file for storing intermediate data.
        matFileName = fullfile( ...
            conditionSettings.samplesDir, ['state_', modelName, '.mat']);
        save(matFileName, 'data', '-v7.3');
        
        %% Setup HDF5 for storing results.
        resultsFileName = fullfile( ...
            conditionSettings.samplesDir, ['samples_', modelName, '.h5']);
        h5File = H5F.create(resultsFileName, 'H5F_ACC_TRUNC', ...
            H5P.create('H5P_FILE_CREATE'), H5P.create('H5P_FILE_ACCESS'));
        H5F.close(h5File);
    end
    
    runForEachCondition(@createStorageForCondition, settings);
end

function createStorageForCondition( ...
    modelName, data, h5File, matFileName, settings)
    
    % Store original 13C data.
    h5WriteStringArrayDataset(h5File, ...
        'condition_name', { data.conditionShortName });
    h5WriteMatrixDataset(h5File, 'grouped_fluxes_13C_mean', data.fluxMean);
    h5WriteMatrixDataset(h5File, 'grouped_fluxes_13C_std', data.fluxStd);
    h5WriteStringArrayDataset(h5File, ...
        'grouped_flux_names', data.fluxNames');
    
    % Load the model for uniform sampling.
    if settings.uniformFluxSampling.enabled
        uniformModelPath = fullfile(settings.modelsDir, [modelName '.mat']);
        uniformModelMat = load(uniformModelPath);
        uniformModel = uniformModelMat.model;
   
        h5WriteMatrixDataset(h5File, ...
            'uniform_fluxes_to_grouped_fluxes', ...
            data.getRxnToFluxMapping(uniformModel.rxns));
        h5WriteStringArrayDataset( ...
            h5File, 'uniform_rxn_names', uniformModel.rxns);
        
        save(matFileName, 'uniformModel', '-append');
    end
    
    % Load the model for thermodynamic sampling.
    if settings.thermodynamicSampling.enabled
        tdModelPath = fullfile(settings.modelsDir, [modelName '_td.mat']);
        tdModelMat = load(tdModelPath);
        tdModel = tdModelMat.model;
        tdModel.isConstraintRxn = tdModel.isConstraintRxn == 1; 
        
        h5WriteMatrixDataset(h5File, ...
            'td_fluxes_to_grouped_fluxes', ...
            data.getRxnToFluxMapping(tdModel.rxns));
            
        h5WriteStringArrayDataset(h5File, 'td_rxn_names', tdModel.rxns);
        h5WriteStringArrayDataset(h5File, 'met_names', tdModel.mets);
        h5WriteStringArrayDataset(h5File, ...
            'constrained_rxn_names', tdModel.rxns(tdModel.isConstraintRxn));
        
        save(matFileName, 'tdModel', '-append');
    end
end

function saveGerosaData(folder)
    gerosa2015data = getGerosa2015data();
    gerosa2015data = rmfield(gerosa2015data, 'getRxnToFluxMapping');
    ac = gerosa2015data(1);
    fru = gerosa2015data(2);
    gal = gerosa2015data(3);
    glc = gerosa2015data(4);
    glyc = gerosa2015data(5);
    glcn = gerosa2015data(6);
    pyr = gerosa2015data(7);
    succ = gerosa2015data(8);
    outputFile = fullfile(folder, 'gerosa2015.mat');
    save(outputFile, 'ac', 'fru', 'gal', 'glc', 'glyc', 'glcn', 'pyr', 'succ');
end