function runForEachCondition(action, settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    for iCondition = settings.conditions
        
        %% Load experimental data.
        gerosa2015data = getGerosa2015data();
        data = gerosa2015data(iCondition);
        fprintf('Running action for condition: %s.\n', data.conditionName);
        modelName = ['e_coli_gerosa2015_' data.conditionShortName];
        conditionSettings = replaceRecursive( ...
            settings, '${condition}', data.conditionShortName);

        %% Open HDF5 for storing results.
        resultsFileName = fullfile( ...
            conditionSettings.samplesDir, ['samples_', modelName, '.h5']);
        matFileName = fullfile( ...
            conditionSettings.samplesDir, ['state_', modelName, '.mat']);
        h5File = H5F.open(resultsFileName, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
        
        %% Sample condition model.
        try
            action(modelName, data, h5File, matFileName, conditionSettings);
        catch exception
            warning('Action failed for condition %i.', iCondition);
            disp(getReport(exception));
        end

        H5F.close(h5File);
    end
    
    fprintf('Done.\n\n');
end