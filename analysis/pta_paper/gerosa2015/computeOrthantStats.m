function computeOrthantStats(settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    if settings.thermodynamicSampling.enabled
        runForEachCondition( ...
            @computeOrthantStatsForCondition, settings);
    end
end

function computeOrthantStatsForCondition( ...
    ~, data, h5File, matFileName, ~)

    load(matFileName, 'tdModel', 'directions', 'revRxns');
    [~, isConstrainedRxn] = intersect(tdModel.rxns, revRxns, 'stable');
    parforModel = tdModel;
    
    nOrthants = numel(directions.counts);
    nMaxOrthants = min(nOrthants, 1e6);
    [~, order] = sort(directions.counts, 'descend');
    order = order(1:nMaxOrthants);
    
    pFBAobjectives = nan(nMaxOrthants, 1);
    couldBeCorrect = false(nMaxOrthants, 1);
    maxATPM = nan(nMaxOrthants, 1);
    
    signs = directions.signs(:, order);
    
    parfor i = 1:nMaxOrthants
        orthantModel = parforModel;
        orthantSigns = signs(:, i);
        
        isFW = false(size(parforModel.lb));
        isBW = false(size(parforModel.lb));
        isFW(isConstrainedRxn) = orthantSigns > 0;
        isBW(isConstrainedRxn) = orthantSigns < 0;
        
        orthantModel.lb(isFW) = max(orthantModel.lb(isFW), 0);
        orthantModel.ub(isBW) = min(orthantModel.ub(isBW), 0);
        
        pFBAresult = optimizeCbModel(orthantModel, 'max', 'one');
        pFBAobjectives(i) = pFBAresult.obj;
        couldBeCorrect(i) = checkDirectionsCorrectness(orthantModel, data);
        
        orthantModel.c = zeros(size(orthantModel.c));
        orthantModel.c(strcmp(orthantModel.rxns, 'ATPM')) = 1;
        ATPMresult = optimizeCbModel(orthantModel);
        maxATPM(i) = ATPMresult.obj;
    end
    
    pFBAobjectives2 = nan(nOrthants, 1);
    couldBeCorrect2 = false(nOrthants, 1);
    maxATPM2 = nan(nOrthants, 1);
    pFBAobjectives2(order) = pFBAobjectives;
    couldBeCorrect2(order) = couldBeCorrect;
    maxATPM2(order) = maxATPM;
    
    h5WriteMatrixDataset(h5File, 'pfba_objectives', pFBAobjectives2);
    h5WriteMatrixDataset(h5File, 'max_atpm', maxATPM2);
    h5WriteInt8MatrixDataset(h5File, 'is_plausible', int8(couldBeCorrect2));
        
    save(matFileName, 'pFBAobjectives', '-append');
end