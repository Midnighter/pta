%% Example network in which ll-ACHRB fails to generate loopless samples.
% Requires ll-ACHRB. Place this script in the "examples" directory of ll-ACHRB.
%
% Based on the example network by Pedro Saa and a counter-example by Elad Noor. 

clc, addpath('fxns/','examples/');

%% Step 0. Load model and define sampling and optmization parameters
filename       = 'toyModel';     % For example: 'toyModel','ecoli_core', 'iRBC283' (examples are found in the examples/ folder)
load([filename,'.mat']);           % model structure has the following fields: S, c, b, lb, ub, rxns, rxnNames, mets, metNames, description (see toy model for an example)

% Modification to the original toy model. Make all the exchanges reversible.
model.lb(1:4) = -10;
model.ub(1:4) = 10;

numSamples     = 1e3;              % Number of samples to be generated
numDiscarted   = 1e3;              % Samples discarded
stepsPerPoint  = 1e2;              % Steps per point (thinning)
method         = 'll_ACHRB';       % 'll_ACHRB' or 'ACHR'
warmUpMethod   = 'loopless';       % 'loopless' or 'conventional'
parallelFlag   = 1;                % Choose 1 for parallel sampling  (only available for MATLAB)
if exist('gurobi','file')
    solverName            = 'gurobi';
    params.outputflag     = 0;         % Optimization parameters (Gurobi)
    params.OptimalityTol  = 1e-9;
    params.IntFeasTol     = 1e-9;
    params.FeasibilityTol = 1e-9;
    params.MIPGapAbs      = 1e-6;
    params.MIPGap         = 1e-6;
    disp('GUROBI solver found')
else
    solverName     = 'glpk';
    params.tolint  = 1e-6;           % Optimization parameters (GLPK)
    params.tolobj  = 1e-6;
    params.tolbnd  = 1e-6;
    params.toldj   = 1e-6;
    disp('Warning: GUROBI solver not found. GLPK used instead.')
end

%% Step 1. Build loopless structure and pre-process model using ll-FVA
% Note: Consider checking numerical tractability of the model before progressing to Step 2. For example,
% the fxns compactCbModel() and processBiomassRxnCbModel() can help resolving numerical issues.
disp('Reducing model...');
if strcmp(solverName,'gurobi');
    
    % Remove singletons first using linear FVA
    modelLP   = linearStructure(model);
    modelLP   = looplessFVA(modelLP,'linear',params);
    initModel = looplessStructureMILP(modelLP);
else
    modelLP   = linearStructure_GLPK(model);
    modelLP   = looplessFVA_GLPK(modelLP,'linear',params);
    initModel = looplessStructureMILP_GLPK(modelLP);
end

disp(['Loopless structure succesfully loaded: rxns = ',num2str(length(initModel.rxns)),', mets = ',num2str(length(initModel.mets))]);
if strcmp(solverName,'gurobi');
    modelSampling = looplessFVA(initModel,'loopless',params);
else
    modelSampling = looplessFVA_GLPK(initModel,'loopless',params);
end
disp('ll-FVA succesfully run');

% Check whether the model is numerically tractable
if strcmp(solverName,'gurobi');
    sol = gurobi(modelSampling,params);
else
    [xopt,fobj,status] = glpk(modelSampling.obj,full(modelSampling.A),modelSampling.rhs,modelSampling.lb,modelSampling.ub,modelSampling.ctype,modelSampling.vartype,modelSampling.sense,params);
end
modelTractable = false;
if strcmp(solverName,'gurobi')
    if strcmp(sol.status,'OPTIMAL')
        modelTractable = true;
    end
else
    if (status==5)
        modelTractable = true;
    end
end
if modelTractable; disp(['Model to be sampled: rxns = ',num2str(length(modelSampling.rxns)),', mets = ',num2str(length(modelSampling.mets))]);
else disp('Warning: Model is ill-conditioned for MILP optimization. Consider revising tractability.'); end;

%% Step 2. Generate warmup-points
disp('Generating warmup points...');
if strcmp(solverName,'gurobi')
    modelSampling = warmUpLoopless(modelSampling,2*modelSampling.numRxns,warmUpMethod,params);
else
    modelSampling = warmUpLoopless_GLPK(modelSampling,2*modelSampling.numRxns,warmUpMethod,params);
end

% Set-up parameters for each sampler: ACHR and ll-ACHR/ACHRB
centerPoint     = modelSampling.centroid;

% Check whether the there are potentially active loop laws
if ~isempty(modelSampling.Nint)
    nonZeroElements = sum((modelSampling.Nint~=0),2);
    loopMatrix      = sign(modelSampling.Nint);
    
    % Check feasibility of the initial point
    checkPattern = diag(sign(centerPoint(modelSampling.internal)));
    
    % If centroid unfeasible, find closest feasible point
    if any(abs(sum(loopMatrix*checkPattern,2))==nonZeroElements)
        if strcmp(solverName,'gurobi')
            modelSampling.centroid = findClosestFeasiblePoint(modelSampling,centerPoint);
        else
            modelSampling.centroid = getInteriorPoint_GLPK(modelSampling,.95,params);
        end
    end
end
disp('Feasible centroid found... (initial point)');

%% Step 3. Loopless sampling
% Stand-alone PC sampling
if ~parallelFlag
    looplessACHRBsampler(modelSampling,numSamples,filename,method,numDiscarted,stepsPerPoint);
else
    % Parallel sampling (requires Parallel Computing Toolbox)
    numWorkers  = 2; % Number of available cores (typically 4 in a desktop machine)
    parallelSampler(modelSampling,numSamples,numWorkers,numDiscarted,stepsPerPoint,filename,method)
end

%% Verify correctness of the samples.
load('toyModel_warmup_loopless_sampler_ll_ACHRB_parallel.mat');
[~, idx] = ismember(modelSampling.rxns, model.rxns);
model.b = zeros(size(model.mets));

changeCobraSolver('gurobi')
nLoopy = 0;
for i=1:size(samplePoints, 2)
    m = model;
    m.lb(idx) = samplePoints(:, i);
    m.ub(idx) = samplePoints(:, i);
    try
        [lb, ub] = fluxVariability(m, 0, 'max', m.rxns, 0, false);
    catch
        nLoopy = nLoopy + 1;
    end
end
fprintf('Found %i loopy samples.\n', nLoopy);

clear all