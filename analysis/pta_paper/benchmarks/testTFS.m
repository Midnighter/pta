outputFolder = fullfile(getPtaRoot(), "data/results/benchmarks/TFS");
[~, ~] = mkdir(outputFolder);
changeCobraSolver('gurobi');

%% Setup model.
model.mets = {'g6p_c', 'g1p_c', 'f1p_c', 'f6p_c'}';
model.metCharges = [-2, -2, -2, -2]';
model.metFormulas = {'C6H11O9P', 'C6H11O9P', 'C6H11O9P', 'C6H11O9P'}';
model.rxns = {'A', 'B', 'C', 'D', 'E', 'EX_g6p_c', 'EX_f1p_c'}';
model.lb = -1000 * ones(numel(model.rxns), 1);
model.ub = 1000 * ones(numel(model.rxns), 1);
model.S = [
    -1,  0,  0,  1,  0, -1,  0;
     1, -1,  0,  0, -1,  0,  0;
     0,  1, -1,  0,  0,  0, -1;
     0,  0,  1, -1,  1,  0,  0;
];
model.c = zeros(size(model.rxns));
model.b = zeros(size(model.mets));

model.metsI = 0.1 * ones(numel(model.mets), 1);
model.metsT = 310.15 * ones(numel(model.mets), 1);
model.metsPhi = -0.086 * ones(numel(model.mets), 1);
model.metsPh = 7.8 * ones(numel(model.mets), 1);
model.isConstraintRxn = false(numel(model.rxns), 1);
model.isConstraintRxn(1:5) = true;

[logConcMean, logConcStd] = getMetaboliteConcentrationPriorGerosa2015();
model.logConcMean = ones(numel(model.mets), 1) * logConcMean;
model.logConcCov = diag(ones(numel(model.mets), 1) * logConcStd^2);

model = cobragen.addThermodynamicDataToModel(model);

%% Sample free energies using TFS.
thermodynamicSpaceSettings.sampleConcentrations = false;
thermodynamicSpaceSettings.freeEnergyVarsConfidence = 0.99999;
thermodynamicSpaceSettings.minDimensionEigenvalue = 1e-4;

optimizationSettings.fluxEpsilon = 1e-4;
optimizationSettings.drgEpsilon = 1e-1;
optimizationSettings.drgMax = 1000;

optimizationSettings.gurobiSettings.IntFeasTol = 1e-9;
optimizationSettings.gurobiSettings.FeasibilityTol = 1e-9;
optimizationSettings.gurobiSettings.TimeLimit = 1800;

[drgSamplingSettings, fluxSamplingSettings] = ...
    getDefaultTFSSettings(outputFolder);
[~, ~] = mkdir(drgSamplingSettings.samplerSettings.logsDir);
drgSamplingSettings.convergenceDiagnostic.enabled = false;

freeEnergySpace = GibbsFreeEnergiesSpace(model, thermodynamicSpaceSettings);
drgSamplingResult = sampleFreeEnergies(model, freeEnergySpace, drgSamplingSettings);
drg = drgSamplingResult.drg.samples;
directions = drgSamplingResult.directions;

%% Sample free energies using rejection rampling.
nSamples = 2000;
drgCov = freeEnergySpace.standardMvnToDrgMvnTransform.T * freeEnergySpace.standardMvnToDrgMvnTransform.T';
drgMean = freeEnergySpace.standardMvnToDrgMvnTransform.shift;
iSample = 1;
nTests = 0;
rejSamples = zeros(size(drgMean, 1), nSamples);

while iSample <= nSamples
    sample = mvnrnd(drgMean, drgCov, 1);
    nTests = nTests + 1;
    orthantModel = model;
    orthantModel.lb(model.isConstraintRxn) = (sample > 0) * -1000 + 0.001;
    orthantModel.ub(model.isConstraintRxn) = (sample < 0) * 1000 - 0.001;
    result = optimizeCbModel(orthantModel);
    if result.stat == 1
        rejSamples(:,iSample) = sample;
        iSample = iSample + 1;
    end
end
fprintf('Rejection rate: %f%%\n', 100 - nSamples / nTests * 100);

%% Plot results
figure(1)
clf;

for i = 1:5
    for j = 1:i
        subplot(5, 5, (i-1)*5+j);
        hold on;
        if i ~= j
            scatter(drg(i,:), drg(j,:), 0.6, '.');
            xlim([-25, 25]);
            ylim([-25, 25]);
            xlabel("DrG' " + model.rxns{j});
            ylabel("DrG' " + model.rxns{i});
        else
            h1 = histogram(drg(i,:));
            h2 = histogram(rejSamples(i,:));
            h1.Normalization = 'probability';
            h1.BinWidth = 2;
            h2.Normalization = 'probability';
            h2.BinWidth = 2;
            xlim([-25, 25]);
            xlabel("DrG' " + model.rxns{i});
            ylabel('Probability');
            if i==1
                legend('PTA', 'RS');
            end
        end
    end
    
    for j = i:5
        subplot(5, 5, (i-1)*5+j);
        hold on;
        if i ~= j
            scatter([], [], 0.6, '.');
            scatter(rejSamples(j,:), rejSamples(i,:), 0.6, '.');
            xlim([-25, 25]);
            ylim([-25, 25]);
            xlabel("DrG' " + model.rxns{j});
            ylabel("DrG' " + model.rxns{i});
        end
    end
end