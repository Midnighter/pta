changeCobraSolver('gurobi')
ptaRoot = getPtaRoot();
outputDir = fullfile(ptaRoot, 'data', 'results', 'benchmarks', 'CHRR');
[~, ~] = mkdir(outputDir);

% Load test models.
iML1515_CAN = load(fullfile(ptaRoot, 'data', 'models', 'generated', ...
    'iML1515-CAN', 'with_metabolomics', 'e_coli_gerosa2015_glc.mat'), 'model');
models = {
    readCbModel(fullfile(ptaRoot, 'data', 'models', 'e_coli_core.mat')), ...
    iML1515_CAN.model
};
nModels = numel(models);

preprocessingTimes = zeros(2, nModels);
samplingTimes = zeros(2, nModels);
PSRFs = cell(2, nModels);
means = cell(2, nModels);
stds = cell(2, nModels);
KSs = cell(1, nModels);
nRxns = zeros(1, nModels);

%% Sample models with both methods

for m = 1:1
    model = models{m};
    nRxns(m) = numel(model.rxns);
    
    % Sampling settings.
    nDims = size(null(full(model.S)), 2);
    nReactions = size(model.S, 2);
    nTotalSteps = 32 * nDims^2;
    nSamples = 10000;
    nChains = 8;
    nWarmup = 1e4; % Fix this, as it is hardcoded in CHRR.
    nSamplesPerChain = nSamples / nChains;
    nWorkers = 4;

    % Adjust number of steps so that the effective number of steps for PTA and
    % CHRR is the same.
    nChrrStepsPerSample = floor((nTotalSteps - nWarmup) / nSamplesPerChain);
    nTotalSteps = nChrrStepsPerSample * nSamplesPerChain + nWarmup;

    % Sample model using CHRR (PTA).
    ptaOptions.samplerSettings.nSamples = nSamples;
    ptaOptions.samplerSettings.nChains = nChains;
    ptaOptions.samplerSettings.nSteps = nTotalSteps;
    ptaOptions.samplerSettings.nWarmupSteps = nWarmup;
    ptaOptions.samplerSettings.nRecordedSteps = ceil(nSamplesPerChain);
    ptaOptions.samplerSettings.maxWorkers = nWorkers;
    ptaOptions.samplerSettings.logsDir = outputDir + "/logs";
    ptaOptions.convergenceDiagnostic.enabled = false;

    tic;
    result = sampleFluxSpaceUniform(model, ptaOptions);
    ptaTime = toc;
    preprocessingTimes(1, m) = result.preprocessingTime;
    samplingTimes(1, m) = ptaTime - result.preprocessingTime;
    
    ptaChains = result.fluxes.chains(:,:,1:nSamplesPerChain);
    PSRFs{1, m} = getSplitR(ptaChains);
    
    ptaSamples = reshape(permute(ptaChains,[1, 3, 2]), size(ptaChains, 1), [], 1);
    [means{1, m}, stds{1, m}] = getDistribution(ptaSamples);
    
    % Sample model using CHRR (cobratoolbox)
    options.nStepsPerPoint = nChrrStepsPerSample;
    options.nPointsReturned = 0;
    options.toRound = 1;
    options.optPercentage = 0;

    chrrChains = zeros(nReactions, nChains, nSamplesPerChain);
    tic;
    [roundedModel, ~] =  sampleCbModel(model, [], [], options);
    preprocessingTimes(2, m) = toc;
    options.nPointsReturned = nSamplesPerChain;
    options.toRound = 0;

    tic;
    parfor (i = 1:nChains, nWorkers)
        maxNumCompThreads(1);
        [~, chrrChains(:, i, :)] =  sampleCbModel(roundedModel, [], [], options, roundedModel);
    end
    samplingTimes(2, m) = toc;
    
    PSRFs{2, m} = getSplitR(chrrChains);
  
    chrrSamples = reshape(permute(chrrChains,[1, 3, 2]), size(chrrChains, 1), [], 1);
    [means{2, m}, stds{2, m}] = getDistribution(chrrSamples);
    
    KSs{m} = getKSs(ptaSamples, chrrSamples);
end

save(fullfile(outputDir, 'data.mat'));

%% Plot
figure(1);
clf;

psrfBinWidth = [0.005, 0.005, 0.005];

for m = 1:nModels    
    subplot(nModels, 4, (m-1)*4+1);
    hold on;
    h1 = histogram(PSRFs{1, m});
    h2 = histogram(PSRFs{2, m});
    h1.Normalization = 'probability';
    h1.BinWidth = psrfBinWidth(m);
    h2.Normalization = 'probability';
    h2.BinWidth = psrfBinWidth(m);
    xlabel('PSRF');
    ylabel('Probability');
    legend('PTA', 'CT');
    
    subplot(nModels, 4, (m-1)*4+2);
    scatter(means{1, m}, means{2, m}, '+');
    set(gca,'XScale','log');
    set(gca,'YScale','log');
    xlabel('Flux mean (PTA)');
    ylabel('Flux mean (CT)');
    xlim([1e-5, 1e5]);
    ylim([1e-5, 1e5]);
    xticks([1e-5, 1e0, 1e5]);
    yticks([1e-5, 1e0, 1e5]);
    
    subplot(nModels, 4, (m-1)*4+3);
    scatter(stds{1, m}, stds{2, m}, '+');
    set(gca,'XScale','log');
    set(gca,'YScale','log');
    xlabel('Flux standard deviation (PTA)');
    ylabel('Flux standard deviation (CT)');
    xlim([1e-5, 1e5]);
    ylim([1e-5, 1e5]);
    xticks([1e-5, 1e0, 1e5]);
    yticks([1e-5, 1e0, 1e5]);
    
    subplot(nModels, 4, (m-1)*4+4);
    hold on;
    h1 = histogram(KSs{m},'facecolor','k');
    h1.BinWidth = 0.005;
    h1.Normalization = 'probability';
    xlabel('KS distance');
    ylabel('Probability');
end

%% Auxiliary functions
function splitR = getSplitR(chains)
    import samply.*
    chains = splitChains(chains);
        
    %% Compute split-R for each parameter.
    [nParameters, ~, ~] = size(chains);
    splitR = nan(1, nParameters);
    for iParameter = 1:nParameters
        parameterChains = squeeze(chains(iParameter, :, :));
        splitR(iParameter) = computeRhat(parameterChains);
    end
end

function KSs = getKSs(ptaSamples, chrrSamples)
    [nParameters, ~] = size(ptaSamples);
    KSs = nan(1, nParameters);
    for iParameter = 1:nParameters
        [~, ~, KSs(iParameter)] = kstest2( ...
            ptaSamples(iParameter,:), ...
            chrrSamples(iParameter,:));
    end
end

function [means, stds] = getDistribution(samples)
    means = mean(samples, 2);
    stds = std(samples, 0, 2);
end