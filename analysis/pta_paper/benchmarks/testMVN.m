%% Settings
outputFolder = fullfile(getPtaRoot(), 'data/results/benchmarks/TFS');
[~, ~] = mkdir(outputFolder);

nDims = [2, 4, 8, 16, 32, 64, 128, 256, 512];
nTests = numel(nDims);
nSamples = 1000;
minThinning = 100;
replicates = 20;

relErrorMeanPta = zeros(2, nTests);
relErrorMeanMvnrnd = zeros(2, nTests);
relErrorCovPta = zeros(2, nTests);
relErrorCovMvnrnd = zeros(2, nTests);

for i=1:nTests
    iRelErrorMeanPta = zeros(1, replicates);
    iRelErrorMeanMvnrnd = zeros(1, replicates);
    iRelErrorCovPta = zeros(1, replicates);
    iRelErrorCovMvnrnd = zeros(1, replicates);
    
    parfor r = 1:replicates
        % Construct a random multivariate normal distribution.
        [sqrtSigma, shift] = randomMvn(nDims(i), 1e-1, 1e5, 1e3);
        covTrue = sqrtSigma * sqrtSigma';
        meanTrue = shift;
        
        samplerSettings = struct();
        samplerSettings.nDirectionSamples = 1;
        samplerSettings.nChains = 1;
        samplerSettings.nSteps = max(nSamples*minThinning, nDims(i)^2*256);
        samplerSettings.nWarmupSteps = samplerSettings.nSteps / 2;
        samplerSettings.stepsThinning = floor(samplerSettings.nSteps / 2 / nSamples);
        samplerSettings.nRecordedSteps = 2*nSamples;
        samplerSettings.maxWorkers = inf;
        samplerSettings.logsDir = fullfile(outputFolder, "logs");
        samplerSettings.consoleLoggingIntervalMs = 1000;
        samplerSettings.feasibilityCacheSize = 10000;

        samplerSettings.maxTestedRxnsForInitialPoints = inf;
        samplerSettings.fluxEpsilon = 1e-4;
        samplerSettings.drgEpsilon = 1e-1;
        samplerSettings.drgMax = 500;
        samplerSettings.minRelRegionCoverage = 1e-8;
        samplerSettings.diagnoseInfeasibilities = false;

        samplerSettings.gurobiSettings.outputflag = 0;
        samplerSettings.gurobiSettings.IntFeasTol = 1e-9;
        samplerSettings.gurobiSettings.FeasibilityTol = 1e-9;
        samplerSettings.gurobiSettings.TimeLimit = 240;
        samplerSettings.gurobiSettings.Threads = 1;
        samplerSettings.workerId = r;
        samplerSettings.truncationMultiplier = ...
            sqrt(chi2inv(0.9999, nDims(i)));

        % Use a very simple flux space with unconstrained directions so that the
        % steady state thermodynamic space is a MVN.
        obsToDrg = zeros(1, nDims(i));
        obsToDrg(1) = 1;

        result = sample_td_space_mex(sqrtSigma, shift, ...
                [1, -1, 0; 0, 1, -1], [-1, -1, -1]', [1, 1, 1]', int64(1), ...
                shift, samplerSettings, ...
                obsToDrg, [0], ...
                eye(nDims(i)));
        ptaSamples = squeeze(result.chains)';
        covPta = cov(ptaSamples);
        meanPta = mean(ptaSamples)';

        % Sample using mvnrnd as comparison.
        mvnrndSamples = mvnrnd(shift, sqrtSigma * sqrtSigma', size(ptaSamples, 1));
        covMvnrnd = cov(mvnrndSamples);
        meanMvnrnd = mean(mvnrndSamples)';
        
        % Compute relative errors
        iRelErrorMeanPta(r) = sqrt((meanPta - meanTrue)' * inv(sqrtSigma * sqrtSigma') * (meanPta - meanTrue)); %#ok<MINV>
        iRelErrorMeanMvnrnd(r) = sqrt((meanMvnrnd - meanTrue)' * inv(sqrtSigma * sqrtSigma') * (meanMvnrnd - meanTrue));  %#ok<MINV>
        
        iRelErrorCovPta(r) = norm(covPta - covTrue, 'fro') / norm(covTrue, 'fro');
        iRelErrorCovMvnrnd(r) = norm(covMvnrnd - covTrue, 'fro') / norm(covTrue, 'fro');
    end
    
    relErrorMeanPta(1, i) = mean(iRelErrorMeanPta);
    relErrorMeanPta(2, i) = std(iRelErrorMeanPta);
    relErrorMeanMvnrnd(1, i) = mean(iRelErrorMeanMvnrnd);
    relErrorMeanMvnrnd(2, i) = std(iRelErrorMeanMvnrnd);
    
    relErrorCovPta(1, i) = mean(iRelErrorCovPta);
    relErrorCovPta(2, i) = std(iRelErrorCovPta);
    relErrorCovMvnrnd(1, i) = mean(iRelErrorCovMvnrnd);
    relErrorCovMvnrnd(2, i) = std(iRelErrorCovMvnrnd);
end

%% 
figure(1);
clf;
subplot(1, 2, 1);
hold on;
errorbar(nDims(1:nTests), relErrorMeanPta(1,:), relErrorMeanPta(2,:));
errorbar(nDims(1:nTests), relErrorMeanMvnrnd(1,:), relErrorMeanMvnrnd(2,:));
legend('PTA', 'mvnrnd','Location','northwest');
xlabel('N dimensions');
ylabel('Mahalanobis distance of the estimated mean');
set(gca,'XScale','log');

subplot(1, 2, 2);
hold on;
errorbar(nDims(1:nTests), relErrorCovPta(1,:), relErrorCovPta(2,:));
errorbar(nDims(1:nTests), relErrorCovMvnrnd(1,:), relErrorCovMvnrnd(2,:));
legend('PTA', 'mvnrnd','Location','northwest');
xlabel('N dimensions');
ylabel('Normalized error of the estimated covariance');
set(gca,'XScale','log');

    %% 
function [sqrtSigma, shift] = randomMvn(nDims, minEW, maxEW, maxShift)
    % Based on https://stats.stackexchange.com/questions/215497/how-to-create-an-arbitrary-covariance-matrix
    logMinEW = log(minEW);
    logMaxEW = log(maxEW);
    EWs = exp(rand(nDims, 1) * (logMaxEW - logMinEW) + logMinEW);
    
    [Q, ~] = qr(randn(nDims, nDims));
    
    sqrtSigma = Q * diag(EWs);
    shift = rand(nDims, 1) * maxShift;
end

function d = covDistance(C1, C2)
    C = (C1 + C2) / 2;
    d = 0.5*log(det(C) / sqrt(det(C1)*det(C2)));
end