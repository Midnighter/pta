% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% Search for structural thermodynamic inconsistencies.
settings = loadGerosa2015Settings('gerosa_2015_core_settings.yaml');
changeCobraSolver(settings.cobraSolver);

%% Generate models.
createGerosa2015EColiModels(settings);

%% Initialize H5 datasets for the samples.
createStorage(settings);

%% Sample the thermodynamic space of the models.
sampleGerosa2015ThermodynamicSpace(settings);

%% Sample concentrations and standard reaction energies of the models.
sampleGerosa2015ConcentrationsAndDrg0(settings);

%% Sample fluxes from the sampled directions.
sampleGerosa2015FluxesFromDirections(settings);