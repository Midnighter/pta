% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% 1) Search for structural thermodynamic inconsistencies.
settings = loadGerosa2015Settings('model_diagnostics_structural.yaml');
changeCobraSolver(settings.cobraSolver);

createGerosa2015EColiModels(settings);

%% 2) Search for areas of the network that appear thermodynamically unlikely.
settings = loadGerosa2015Settings('model_diagnostics_quantitative.yaml');
changeCobraSolver(settings.cobraSolver);

createGerosa2015EColiModels(settings);

%% 3) Investigate why gluconeogenesis doesn't seem to be feasible.
settings = loadGerosa2015Settings('model_diagnostics_GNG_channeling.yaml');
changeCobraSolver(settings.cobraSolver);

createGerosa2015EColiModels(settings);

%% 4) Finally analyze the curated model.
settings = loadGerosa2015Settings('model_diagnostics_curated.yaml');
changeCobraSolver(settings.cobraSolver);

createGerosa2015EColiModels(settings);

%%  5) Analyse result when gluconeogenesis is forced
settings = loadGerosa2015Settings('model_diagnostics_forced_GNG.yaml');
changeCobraSolver(settings.cobraSolver);

createGerosa2015EColiModels(settings);
