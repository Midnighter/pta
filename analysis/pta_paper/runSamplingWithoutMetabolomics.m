% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

%% 1) Define analysis settings.
settings = loadGerosa2015Settings('iML1515_CAN_without_metabolomics.yaml');
changeCobraSolver(settings.cobraSolver);

%% 2) Generate models.
createGerosa2015EColiModels(settings);

%% 3) Initialize H5 datasets for the samples.
createStorage(settings);

%% 4) Sample the models uniformly.
sampleGerosa2015Uniform(settings);

%% 5) Sample the thermodynamic space of the models.
sampleGerosa2015ThermodynamicSpace(settings);

%% 6) Sample concentrations and standard reaction energies of the models.
sampleGerosa2015ConcentrationsAndDrg0(settings);

%% 7) Sample fluxes from the sampled directions.
sampleGerosa2015FluxesFromDirections(settings);

%% 8) Compute different statistics for each orthant.
computeOrthantStats(settings);

%% 9) Run TMFA for comparison
estimateTMFAConcentrationRanges(settings);