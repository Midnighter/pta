% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

ptaMatlabRoot = fileparts(which('initPta.m'));

[~, ~] = mkdir(fullfile(ptaMatlabRoot, '..', 'bin'));
addpath(genpath(fullfile(ptaMatlabRoot)));
addpath(genpath(fullfile(ptaMatlabRoot, '..', 'analysis')));
addpath(genpath(fullfile(ptaMatlabRoot, '..', 'bin')));
addpath(genpath(fullfile(ptaMatlabRoot, '..', 'data', 'models')));
addpath(genpath(fullfile(ptaMatlabRoot, '..', 'externals', 'samply', 'MATLAB')));

configFileName = fullfile(getPtaRoot(), 'config', 'config.yaml');

if ~isfile(configFileName)
    fileID = fopen(configFileName, 'w');
    fprintf(fileID, ['pythonCommand: "python"  # The python executable you ' ...
        'want to use.\n']);
    fprintf(fileID, ['pythonPrefix: ""         # E.g. use "conda activate ' ...
        'pta &&" to use the conda environment "pta".\n']);
    fclose(fileID);
end