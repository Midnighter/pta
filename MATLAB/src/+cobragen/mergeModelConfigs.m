function config = mergeModelConfigs(baseConfig, config2)
% MERGEMODELCONFIGS Adds a model configuration on top a base configuration.
%
%       config = mergeModelConfigs(config1, config2);
%
%   Required:
%       baseConfig:     Struct representing the base configuration.
%       config2:        Struct representing a second configuration that must be
%                       added on top of baseConfig.
%
%   Returns:
%       config:         Configuration struct resultign from the merge.
%
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    config = baseConfig;
    allFieldNames = unique([fieldnames(config); fieldnames(config2)]);
    for iField = 1:length(allFieldNames)
        fieldName = allFieldNames{iField};
        if (isfield(config, fieldName) && isstruct(config.(fieldName))) ...
            || (isfield(config2, fieldName) && isstruct(config2.(fieldName)))
            config = mergeStructs(config, config2, fieldName);
        elseif (isfield(config, fieldName) && isvector(config.(fieldName))) ...
            || (isfield(config2, fieldName) && isvector(config2.(fieldName)))
            config = mergeLists(config, config2, fieldName);
        else
            % Ignore remaining fields.
        end
    end
    
end

function config = mergeLists(baseConfig, config2, listName)
% Adds list listName from config2 to baseConfig, merging with the existing list
% if needed.
    config = baseConfig;
    if isfield(baseConfig, listName) && isfield(config2, listName)
        config.(listName) = [baseConfig.(listName), config2.(listName)];
    elseif isfield(baseConfig, listName) && ~isfield(config2, listName)
        % Nothing to do. List already present.
    elseif ~isfield(baseConfig, listName) && isfield(config2, listName)
        config.(listName) = config2.(listName);
    else
        % Nothing to do. List doesn't exist.
        warning('Trying to merge non-existing field.');
    end
end

function config = mergeStructs(baseConfig, config2, structName)
% Adds struct structName from config2 to baseConfig, merging with the existing
% struct if needed.
    config = baseConfig;
    if isfield(baseConfig, structName) && isfield(config2, structName)
         fieldNames = fieldnames(config2.(structName));
         for iField = 1:length(fieldNames)
            config.(structName).(fieldNames{iField}) = ...
                config2.(structName).(fieldNames{iField});
         end
    elseif isfield(baseConfig, structName) && ~isfield(config2, structName)
        % Nothing to do. Struct already present.
    elseif ~isfield(baseConfig, structName) && isfield(config2, structName)
        config.(structName) = config2.(structName);
    else
        % Nothing to do. Struct doesn't exist.
        warning('Trying to merge non-existing field.');
    end
end

