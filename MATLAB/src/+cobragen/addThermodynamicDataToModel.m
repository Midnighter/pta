function model = addThermodynamicDataToModel(model, varargin)
% ADDTHERMODYNAMICDATATOMODEL Extends the input model with thermodynamics 
% information and stores the result in the specified model file.
%
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    %% Read options.
    parser = inputParser;
    parser.KeepUnmatched = true;
    addParameter(parser, 'metMapper', @(m) ['bigg.metabolite:', m(1:end-2)]);
    parse(parser, varargin{:});
    options = parser.Results;
    config = cobragen.getConfig();
    fprintf("Estimating DrG'° with eQuilibrator ...\n");
    
    %% Adjust metabolite names for eQuilibrator.
    eqModel = model;
    eqModel.mets = cellfun(options.metMapper, ...
        eqModel.mets, 'UniformOutput', false);
    
    %% Hard mappings for metabolites not recognized in MetaNetX.
    hardMappings = { ...
        'bigg.metabolite:2ddara', 'kegg:C03826', ...
        'bigg.metabolite:ppa', 'kegg:C00163', ...
        'bigg.metabolite:r2hglut', 'kegg:C01087', ...
        'bigg.metabolite:urdgci', 'kegg:C02091', ...
    };
    namesMap = containers.Map(hardMappings(1:2:end), hardMappings(2:2:end));
    for iMet = 1:numel(eqModel.mets)
        if isKey(namesMap, eqModel.mets{iMet})
            eqModel.mets{iMet} = namesMap(eqModel.mets{iMet});
        end
    end
    
    %% Prepare input file for python script.
    needsDfg0Estimate = any(eqModel.S(:, eqModel.isConstraintRxn) ~= 0, 2);
    inputFileName = fullfile(pwd, 'hf_input_tmp.mat');
    outputFileName = fullfile(pwd, 'hf_output_tmp.mat');
    metStrings = eqModel.mets(needsDfg0Estimate);
    rxnStrings = printRxnFormula(eqModel, ...
        'rxnAbbrList', eqModel.rxns(eqModel.isConstraintRxn), ...
        'printFlag', false);
    metpH = eqModel.metsPh(needsDfg0Estimate);
    metI = eqModel.metsI(needsDfg0Estimate);
    metT = eqModel.metsT(needsDfg0Estimate);
    metpMg = eqModel.metPmg(needsDfg0Estimate);
    save(inputFileName, 'metStrings', 'rxnStrings',  ...
        'metpH', 'metI', 'metT', 'metpMg');
    
    %% Call python scripts and read output.
    try
        scriptPath = which('free_energies_estimator.py');
        command = strcat(config.pythonPrefix, " ", config.pythonCommand, ...
            " ", scriptPath, " ", inputFileName, " ", outputFileName);
        [status, res] = system(command);
        
        disp(res);
        load(outputFileName, 'isBalanced', 'dfg0Mean', 'dfg0Cov');
    catch e
        disp(e);
        status = 1;
    end
    
    %% Delete temporary files.
    if isfile(inputFileName)
        delete(inputFileName);
    end
    if isfile(outputFileName)
        delete(outputFileName);
    end
    
    %% Process results.
    assert(status == 0, ['Unable to obtain free energy estimates from ' ...
        'eQuilibrator']);
    assert(isBalanced, ['One or more reactions are not balanced. ' ...
        'Thermodynamic analysis is not possible.']);
    model.dfg0Mean = zeros(size(model.mets));
    model.dfg0Cov = zeros(numel(model.mets));
    model.dfg0Mean(needsDfg0Estimate) = dfg0Mean;
    model.dfg0Cov(needsDfg0Estimate, needsDfg0Estimate) = dfg0Cov;
    model.drgCorr = createCorrectionForMembranePotential(model);
    fprintf('Estimation completed successfully!\n');
end

function drgCorrection = createCorrectionForMembranePotential(model)
% Adapted from estimateDrGt0.m in the COBRA toolbox.

    [nMetabolites, nReactions] = size(model.S);
    F = 96.485;
    R = 8.314e-3;
    
    % Find the number of hydrogen atoms in each metabolite.
    nMetHAtoms = zeros(nMetabolites, 1);
    for iMet = 1:nMetabolites
        try
            nMetHAtoms(iMet) = numAtomsOfElementInFormula( ...
                model.metFormulas{iMet}, 'H');
        catch
            warning('Could not determine the number of protons for %s.', ...
                model.mets{iMet});
            nMetHAtoms(iMet) = 0;
        end
    end
    
    % Make sure that each metabolite has a charge assigned.
    hasCharge = isfinite(model.metCharges);
    if any(~hasCharge)
        model.metCharges(~hasCharge) = 0;
        warning('Found %i metabolites without a charge assigned.', ...
            sum(~hasCharge));
    end

    % Find the compartment name of each metabolite.
    model.metCompartments = cellfun(@(m) m(end), model.mets);

    % Compute correction to the formation energy of each metabolite accounting
    % for its charge and number of hydrogen atoms.
    dfg0HCorrection = -R * model.metsT * log(10) .* model.metsPh;
    phiCorrection = F * model.metsPhi;
    
    drgCorrection = zeros(nReactions, 1);
    dphCorrection = model.S(:, model.isConstraintRxn)' * (nMetHAtoms .* dfg0HCorrection);
    dphiCorrection = model.S(:, model.isConstraintRxn)' * (model.metCharges .* phiCorrection);

    drgCorrection(model.isConstraintRxn) = dphCorrection + dphiCorrection;
end