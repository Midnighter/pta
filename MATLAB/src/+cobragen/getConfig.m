function config = getConfig()
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    yamlFileName = fullfile( ...
        getPtaRoot(), 'config', 'config.yaml');
    assert(exist(yamlFileName, 'file') == 2, ['config.yaml not found. ' ...
        'run initPta() to generate it again.']);
    config = yaml.ReadYaml(yamlFileName);
end

