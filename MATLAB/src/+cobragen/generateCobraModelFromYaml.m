function model = generateCobraModelFromYaml(yamlFileName, varargin)
% GENERATECOBRAMODELFROMYAML Generates a cobra model from a model definition
% file. Model definitions are encoded as a sequence of edits in YAML format.
%
%       model = generateCobraModelFromYaml('definitions.yaml', ...
%           'applyBoundsCallback', myCustomCallback);
%
%   Required:
%       yamlFileName:   Path to the .yaml file containing the model definitions.
%
%   Named parameters:
%       [step]Callback: Handle to a function defining custom behavior for the
%                       specified step. [step] can be any of the steps supported
%                       in the model definition file format.
%
%   Returns:
%       model:          The model generated from the given definition file.
%
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    %% Read options.
    options = parseOptions(varargin{:});
    
    %% Read and merge configurations and load base model.
    fprintf('Reading model configuration: %s\n', yamlFileName);
    assert(exist(yamlFileName, 'file') ~= 0, 'File not found.');
    config = yaml.ReadYaml(yamlFileName);
    while endsWith(config.baseModel, '.yaml')
        baseYamlFileName = which(config.baseModel);
        fprintf(' > Adding base model configuration: %s\n', baseYamlFileName);
        assert(exist(baseYamlFileName, 'file') ~= 0, 'File not found.');
        baseConfig = yaml.ReadYaml(baseYamlFileName);
        config = rmfield(config, 'baseModel');
        config = cobragen.mergeModelConfigs(baseConfig, config);
    end
    
    baseModelFile = which(config.baseModel);
    if ~isfile(baseModelFile)
        baseModelFile = config.baseModel;
        if ~isfile(baseModelFile)
            error('Cannot find base model: %s', config.baseModel);
        end
    end
    
    fprintf(' > Adding base model: %s\n', baseModelFile);
    model = readCbModel(baseModelFile);
    
    if isfield(config, 'maxFluxMagnitude')    
        model.lb = max(model.lb, -config.maxFluxMagnitude);
        model.ub = min(model.ub, config.maxFluxMagnitude);
    end
    
    %% Generate the model based on the given configuration.    
    model = renameReactions(model, config);
    model = executeCallbackIfAny(options, 'renameReactionsCallback', model);
    model = executeCallbackIfAny(options, 'renameMetabolitesCallback', model);
    model = addReactions(model, config);
    model = executeCallbackIfAny(options, 'addReactionsCallback', model);
    model = removeReactions(model, config);
    model = executeCallbackIfAny(options, 'removeReactionsCallback', model);
    model = removeMetabolitesCobragen(model, config);
    model = executeCallbackIfAny(options, 'removeMetabolitesCallback', model);
    model = applyBounds(model, config);
    model = executeCallbackIfAny(options, 'applyBoundsCallback', model);
    model = compressModelIfRequired(model, config);
    model = executeCallbackIfAny(options, 'compressModelCallback', model);
    
    % The following steps add data that are not COBRA standard and may not be
    % preserved by COBRA methods.
    model = addMetaboliteConcentrations(model, config, ...
        options.concentrationsPriorSource);
    model = executeCallbackIfAny(options, ...
        'addMetaboliteConcentrationsCallback', model);
    model = addCompartmentParameters(model, config);
    model = executeCallbackIfAny(options, ...
        'addCompartmentParamsetersCallback', model);
    model = setConstraintReactions(model, config);
    model = executeCallbackIfAny(options, ...
        'addConstraintReactionsCallback', model);
    model = importMetaboliteData(model, config);
    model = executeCallbackIfAny(options, ...
        'importMetaboliteDataCallback', model);
    
    fprintf('Model generated successfully.\n');
end

function model = executeCallbackIfAny(options, callbackName, model, varargin)
    if ~isempty(options.(callbackName))
        callback = options.(callbackName);
        fprintf('    > Executing custom step %s().\n', callbackName);
        if ~exist('varargin', 'var')
            varargin = {};
        end
        model = callback(model, varargin{:});
    end
end

function options = parseOptions(varargin)
    parser = inputParser;
    addParameter(parser, 'renameReactionsCallback', []);
    addParameter(parser, 'renameMetabolitesCallback', []);
    addParameter(parser, 'removeReactionsCallback', []);
    addParameter(parser, 'removeMetabolitesCallback', []);
    addParameter(parser, 'addReactionsCallback', []);
    addParameter(parser, 'applyBoundsCallback', []);
    addParameter(parser, 'compressModelCallback', []);
    addParameter(parser, 'addMetaboliteConcentrationsCallback', []);
    addParameter(parser, 'addCompartmentParamsetersCallback', []);
    addParameter(parser, 'addConstraintReactionsCallback', []);
    addParameter(parser, 'importMetaboliteDataCallback', []);
    addParameter(parser, 'concentrationsPriorSource', 'gerosa2015');
    parse(parser, varargin{:});
    options = parser.Results;
end

function isInList = isReactionInList(reactions, model)
    isInList = false(size(model.rxns));
    for iRxn = 1:numel(reactions)
        if strcmp(reactions(iRxn), '#internal')
            isInList = isInList | isInternalReaction(model.S);
        elseif strcmp(reactions(iRxn), '#exchange')
            isInList = isInList | ~isInternalReaction(model.S);
        elseif strcmp(reactions(iRxn), '#simple-export')
            isInList = isInList | isSimpleExportReaction(model);
        elseif startsWith(reactions(iRxn), '#subsystem')
            tokens = strsplit(reactions{iRxn}, ':');
            subsystem = tokens(2);
            isInList = isInList | strcmp([model.subSystems{:}]', subsystem);
        else
            isInList = isInList | strcmp(model.rxns, reactions(iRxn));
        end
    end
end

function model = applyBounds(model, config)
    if ~isfield(config, 'applyBounds')
        return;
    end
    
    fprintf(' > Applying reaction bounds.\n');
    for iGroup = 1:numel(config.applyBounds)
        isSelected = isReactionInList( ...
            config.applyBounds{iGroup}.reactions, model);
        model.lb(isSelected) = config.applyBounds{iGroup}.lb;
        model.ub(isSelected) = config.applyBounds{iGroup}.ub;
    end
end

function model = removeReactions(model, config)
    if ~isfield(config, 'removeReactions')
        return;
    end
    
    fprintf(' > Removing reactions.\n');
    isToRemove = isReactionInList(config.removeReactions, model);
    model = removeRxns(model, model.rxns(isToRemove));
end

function model = removeMetabolitesCobragen(model, config)
    if ~isfield(config, 'removeMetabolites')
        return;
    end
    
    fprintf(' > Removing metabolites.\n');
    model = removeRxns(model, ...
        findRxnsFromMets(model, config.removeMetabolites));
    model = removeMetabolites(model, config.removeMetabolites);
end

function model = renameReactions(model, config)
    if ~isfield(config, 'renameReactions')
        return;
    end
    
    fprintf(' > Renaming reactions.\n');
    for field = fieldnames(config.renameReactions)'
        model.rxns{strcmp(model.rxns, field{1})} = config.renameReactions.(field{1});
    end
end

function model = addReactions(model, config)
    if ~isfield(config, 'addReactions')
        return;
    end
    
    fprintf(' > Adding new reactions.\n');
    for iRxn = 1:numel(config.addReactions)
        reaction = config.addReactions{iRxn};
        model = addReaction(model, reaction.name, 'reactionFormula', ...
            reaction.formula, 'lowerBound', reaction.lb, 'upperBound', ...
            reaction.ub, 'printLevel', 0);
    end
end

function model = compressModelIfRequired(model, config)
    if ~isfield(config, 'compression') || ~config.compression.compressModel
        return;
    end
    
    fprintf(' > Compressing model.\n');
    protectedRxnsIds = find(isReactionInList( ...
        config.compression.protectReactions, model));
    unprotectedRxnsIds = find(isReactionInList( ...
        config.compression.unprotectReactions, model));
    protectedRxnsIds = setdiff(protectedRxnsIds, unprotectedRxnsIds);
    protectedRxns = model.rxns(protectedRxnsIds);
    
    % Find the indices of the metabolites that must be protected form 
    % compression.
    protectedMetsIds = nan(numel(config.compression.protectMetabolites), 1);
    for iMet = 1:numel(config.compression.protectMetabolites)
        protectedMetsIds(iMet) = find(strcmp( ...
            model.mets, config.compression.protectMetabolites(iMet)));
    end
    
    model = compressCobraModel(model, protectedRxnsIds, protectedMetsIds);
    model.protectedRxns = protectedRxns;
end

function model = addCompartmentParameters(model, config)
    fprintf(' > Adding compartement parameters.\n');
    
    %% Create empty fields for the physiological parameters if needed.
    if ~isfield(model, 'metsPh')
        model.metsPh = nan(size(model.mets));
    end
    if ~isfield(model, 'metsPhi')
        model.metsPhi = nan(size(model.mets));
    end
    if ~isfield(model, 'metsI')
        model.metsI = nan(size(model.mets));
    end
    if ~isfield(model, 'metsT')
        model.metsT = nan(size(model.mets));
    end
    if ~isfield(model, 'metPmg')
        model.metsT = nan(size(model.mets));
    end

    %% Add the parameters given in the config.
    if isfield(config, 'pH')
        model.metsPh = fillMetsParam(model.mets, model.metsPh, config.pH);
    end
    if isfield(config, 'potential')
        model.metsPhi = fillMetsParam(model.mets, model.metsPhi, config.potential);
    end
    if isfield(config, 'ionicStrength')
        model.metsI = fillMetsParam(model.mets, model.metsI, config.ionicStrength);
    end
    if isfield(config, 'temperature')
        model.metsT = fillMetsParam(model.mets, model.metsT, config.temperature);
    end
    if isfield(config, 'pMg')
        model.metPmg = fillMetsParam(model.mets, model.metPmg, config.pMg);
    end
end

function metsParam = fillMetsParam(mets, metsParam, paramConfig)
    for field = fieldnames(paramConfig)'
        selectedMets = endsWith(mets, field{1});
        metsParam(selectedMets) = double(paramConfig.(field{1}));
    end
end

function model = addMetaboliteConcentrations( ...
    model, config, concentrationsPriorSource)
   
    fprintf(' > Adding metabolite concentrations.\n');
    switch (concentrationsPriorSource)
        case 'bennet2009'
            [logcMean, logcStd] = getMetaboliteConcentrationPriorBennet2009();
        case 'gerosa2015'
            [logcMean, logcStd] = getMetaboliteConcentrationPriorGerosa2015();    
        case 'gerosa2015AcGlc'
            [logcMean, logcStd] = getMetaboliteConcentrationPriorGerosa2015AcGlc();    
    end
    nMetabolites = numel(model.mets);
    if ~isfield(model, 'logConcMean')
        model.logConcMean = ones(nMetabolites, 1) * logcMean;
    end
    if ~isfield(model, 'logConcCov')
        model.logConcCov = eye(nMetabolites, nMetabolites) * logcStd^2;
        possiblyAbsentExtracellularMetaboliteIds = ...
            find(endsWith(model.mets, '_e') | endsWith(model.mets, '_p'));
        for iMet = possiblyAbsentExtracellularMetaboliteIds'
            model.logConcCov(iMet, iMet) = ...
                model.logConcCov(iMet, iMet) * 5^2;
        end
    end
    
    % Add the concentrations specified in the model description.
    if isfield(config, 'concentrations')
        for iMet = 1:numel(config.concentrations)
            metData = config.concentrations{iMet};
            isMet = strcmp(model.mets, metData.name);
            model.logConcMean(isMet) = log(metData.mean);
            model.logConcCov(isMet, isMet) = metData.logStd^2;
        end
    end
    
    % Free energy estimates from eQuilibrator have [H] = 0 and [H2O] = 0 as
    % reference point.
    isProton = startsWith(model.mets, 'h_') | startsWith(model.mets, 'MNXM1_');
    isWater = startsWith(model.mets, 'h2o_') | startsWith(model.mets, 'MNXM2_');
    model.logConcMean(isProton) = 0;
    model.logConcCov(isProton, isProton) = 0;
    model.logConcMean(isWater) = 0;
    model.logConcCov(isWater, isWater) = 0;
end

function model = setConstraintReactions(model, config)
    
    fprintf(' > Identifying constraint reactions.\n');
    if ~isfield(model, 'isConstraintRxn')
        model.isConstraintRxn = true(numel(model.rxns), 1);
        
        % Reactions consisting of only hydrogen transport are not a constraint.
        for iRxn = 1:numel(model.rxns)
            isParticipatingMet = model.S(:, iRxn) ~= 0;
            if all(startsWith(model.mets(isParticipatingMet), 'h_'))
                model.isConstraintRxn(iRxn) = false;
            end
        end
    end
        
    if isfield(config, 'nonConstraintReactions')
        isNotConstraint = isReactionInList(config.nonConstraintReactions, model);
        model.isConstraintRxn = model.isConstraintRxn & ~isNotConstraint;
    elseif isfield(config, 'constraintReactions')
        isConstraint = isReactionInList(config.constraintReactions, model);
        model.isConstraintRxn = isConstraint;
    end
end

function model = importMetaboliteData(model, config)

    if isfield(config, 'importMetaboliteData')
        fprintf(' > Importing metabolite data.\n');
        model.metFormulas = cell(size(model.mets));
        model.metCharges = nan(size(model.mets));
        
        sourceModel = readCbModel(config.importMetaboliteData);
        for iMet = 1:numel(model.mets)
            sourceMetId = find(strcmp(sourceModel.mets, model.mets{iMet}));
            model.metFormulas{iMet} = sourceModel.metFormulas{sourceMetId};
            model.metCharges(iMet) = sourceModel.metCharges(sourceMetId);
        end
    end
end

function isSimpleExport = isSimpleExportReaction(model)

    model = tightenModelBounds(model);

    isSimpleExport = false(size(model.rxns));
    for iRxn = 1:numel(model.rxns)    
        metIds = find(model.S(:, iRxn));
        if numel(metIds) ~= 2 || (model.lb(iRxn) < 0 && model.ub(iRxn) > 0)
            continue;
        end
        
        metNames = model.mets(metIds);
        metCoeffs = model.S(metIds, iRxn);
        metName1 = metNames{1};
        metName2 = metNames{2};
        met1 = metName1(1:end-2);
        met2 = metName2(1:end-2);
        compartment1 = metName1(end);
        compartment2 = metName2(end);
        direction = sign(model.lb(iRxn) + model.ub(iRxn));
        
        if strcmp(met1, met2) && ~strcmp(met1, 'co2') && ( ...
        	(strcmp(compartment1, 'e') && metCoeffs(1) * direction > 0 && sum(model.S(metIds(1), :) ~= 0) == 2) || ...
            (strcmp(compartment2, 'e') && metCoeffs(2) * direction > 0 && sum(model.S(metIds(2), :) ~= 0) == 2))
            isSimpleExport(iRxn) = true;
        end
    end
end