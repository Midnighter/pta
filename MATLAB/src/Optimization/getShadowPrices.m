function result = getShadowPrices( ...
    directions, objectiveType, objective, cobraModel, freeEnergySpace, settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    %% Create constraints for fluxes, formation energies, and directions.
    % columnsScale = 1e3 ./ max(abs([cobraModel.lb, cobraModel.ub]'))'; 
    [vLhs, vSense, vRhs, vMin, vMax, vVarType, fluxScale] = ...
        createLPSteadyStateFluxConstraints(cobraModel, true);
    
    freeEnergyVarsToDrg = freeEnergySpace.standardMvnToDrgMvnTransform;
    
    %% Aggregate all the constraints in a single optimization problem.
    [~, nReactions] = size(cobraModel.S);
    nFreeEnergyVars = numel(freeEnergySpace.lb);
    nFluxConstraints = numel(vRhs);
    nConstraintRxns = size(freeEnergyVarsToDrg.T, 1);
    nVariables = nReactions + nFreeEnergyVars;
    isConstraintRxn = cobraModel.isConstraintRxn;
    
    %% Define gurobi model encoding the stFBA constraints.
    % The variables of the model are [v; freeEnergyVars; direction]
    gurobiModel = addObjective(struct(), ...
        objectiveType, objective, nReactions, nFreeEnergyVars, nVariables);
    gurobiModel.vtype = [vVarType, repmat('C', 1, nFreeEnergyVars)];
    
    gurobiModel.lb = [vMin; freeEnergySpace.lb];
    gurobiModel.ub = [vMax; freeEnergySpace.ub];
    
    % Prepare portions of the constraints matrix.
    ignoreV = zeros(nConstraintRxns, nReactions);
    ignoreFreeEnergyVars = zeros(nConstraintRxns, nFreeEnergyVars);
    fluxBigM = 1.01 * max( ...
        abs([vMin(isConstraintRxn), vMax(isConstraintRxn)]'))';
    fluxEpsilon = max(fluxBigM * 1e-8, settings.fluxEpsilon);
    
    vConstrainedDiagonal = eye(nReactions);
    vConstrainedDiagonal = vConstrainedDiagonal(isConstraintRxn, :);
    
    % Rescale the fluxes in order to improve numerics.
    gurobiModel.obj(1:nReactions) = gurobiModel.obj(1:nReactions) ./ fluxScale;
    
    % Create linear constraints.
    gurobiModel.A = sparse([
        vLhs, zeros(nFluxConstraints, nFreeEnergyVars);
        vConstrainedDiagonal,  ignoreFreeEnergyVars;
        ignoreV,                freeEnergyVarsToDrg.T
    ]);

    isForward = (directions > 0)';
    gurobiModel.sense = [
        vSense, ... 
        char(repmat('>', 1, nConstraintRxns) .* isForward + ...
             repmat('<', 1, nConstraintRxns) .* ~isForward), ...
        char(repmat('<', 1, nConstraintRxns) .* isForward + ...
             repmat('>', 1, nConstraintRxns) .* ~isForward), ...
    ];
    
    gurobiModel.rhs = [
        vRhs;
        fluxEpsilon .* isForward' - fluxEpsilon .* ~isForward'; ...
        (-settings.drgEpsilon * ones(nConstraintRxns, 1) - freeEnergyVarsToDrg.shift) .* isForward' + ...
            (settings.drgEpsilon * ones(nConstraintRxns, 1) - freeEnergyVarsToDrg.shift) .* ~isForward'
    ];

    
    %% Solve MILP.
    params = settings.gurobiSettings;
    gurobiResult = gurobi(gurobiModel, params);
    
    switch gurobiResult.status
        case {'OPTIMAL'}          
            result.dualV = gurobiResult.pi(nFluxConstraints + (1:nConstraintRxns));
            result.dualDrg = gurobiResult.pi(nFluxConstraints + nConstraintRxns + (1:nConstraintRxns));
        otherwise
            error('Optimization failed with status %s.', gurobiResult.status);
    end
end

function model = addObjective(model, objectiveType, objective, nReactions, ...
    nFreeEnergyVars, nVariables)

    tokens = split(objectiveType);
    if numel(tokens) == 1
    	model.modelsense = 'max';
    else
        model.modelsense = tokens{2};
    end

    % Decide in what position of the objective vector (or matrix) we must insert
    % the objective provided by the user.
    switch tokens{1}
        case 'all' 
            objectivePosition = 1:nReactions+nFreeEnergyVars;
        case 'flux'
            objectivePosition = 1:nReactions;
        case 'freeEnergyVars'
            objectivePosition = (nReactions+1):(nReactions+nFreeEnergyVars);
        otherwise
            error('Unsupported objective variable: %s', tokens{1});
    end 
    
    model.obj = zeros(nVariables, 1);
    if isvector(objective)
        % The objective is a linear function.
        model.obj(objectivePosition) = objective;
    else
        % The objective is a quadratic function.
        model.Q = sparse(nVariables, nVariables);
        model.Q(objectivePosition, objectivePosition) = objective;
    end
end