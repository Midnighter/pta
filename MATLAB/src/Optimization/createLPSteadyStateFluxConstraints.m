function [lhs, sense, rhs, vMin, vMax, varType, fluxScale] = ...
    createLPSteadyStateFluxConstraints(model, applyScaling)
% CREATELPSTEADYSTATEFLUXCONSTRAINTS Create a set of flux constraints enforcing
% the steady state condition.
%
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    if nargin == 1
        applyScaling = false;
    end
    [nMetabolites, nReactions] = size(model.S);

    %% Create the constraint equations, applying scaling if required.
    if applyScaling
        % Rescale fluxes such that the absolute maximums have the same value.
        referenceValue = 1e3;
        maxAbsoluteFlux = ...
            max(abs([model.lb, model.ub]'));
        columnScale = referenceValue ./ maxAbsoluteFlux';
        vMin = model.lb .* columnScale;
        vMax = model.ub .* columnScale;
        lhs = model.S * diag(1 ./ columnScale);

        % Normalize each row in order to reduce the range of coefficients.
        rowNorms = vecnorm(lhs, 2, 2);
        lhs = diag(referenceValue ./ rowNorms) * lhs;
        fluxScale = columnScale;
       
        % [columnScale, rowScale] = gmscale(lhs, 0, 0.99);
        % columnScale = sqrt(abs(columnScale)) .* sign(columnScale) / 100;
        % rowScale = sqrt(abs(rowScale)) .* sign(rowScale);
        % lhs = diag(1 ./ rowScale) * lhs * diag(1 ./ columnScale);
        % fluxScale = fluxScale .* columnScale;
        % vMin = vMin .* columnScale;
        % vMax = vMax .* columnScale;
    else
        lhs = model.S;
        vMin = model.lb;
        vMax = model.ub;
        fluxScale = ones(size(vMin));
    end
    
    %% Set remaining LP information.
    sense = repmat('=', 1, nMetabolites);
    varType = repmat('C', 1, nReactions);
    rhs = zeros(nMetabolites, 1);
end

