function [v, freeEnergyVars, drg, logConc, drg0] = PMO( ...
    objectiveType, objective, cobraModel, freeEnergySpace, settings)
% PMO Perform Probabilistic Metabolic Optimization (PMO) on the given model.
%
%   Required:
%       objectiveType:  String in the format 'variable direction' describing
%                       the type of objective (e.g. 'freeEnergyVars min').
%                       - 'variable' can be 'all', 'flux', 'freeEnergyVars'.
%                       - 'direction' can be 'min', 'max'.
%       objective:      Vector describing a linear objective of matrix
%                       describing a quadratic objective.
%       cobraModel:     Cobra model describing the network.
%       freeEnergySpace: GibbsFreeEnergiesSpace describing the thermodynamic
%                       space of the network.
%       settings:       Struct containing optimization and solver settings:
%           .fluxEpsilon:   Minimum flux magnitude through all reactions.
%           .fluxEpsilon:   Minimum DrG magnitude for all reactions.
%           .drgMax:        Maximum DrG value. Used in the Big-M forumlation.
%           .gurobiSettings: Struct of settings for the solver.
%           .diagnoseInfeasibilities: If true, compute the IIS of the problem
%                           and identify conflicting constraints.
%
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    %% Create constraints for fluxes, formation energies, and directions.
    % columnsScale = 1e3 ./ max(abs([cobraModel.lb, cobraModel.ub]'))'; 
    [vLhs, vSense, vRhs, vMin, vMax, vVarType, fluxScale] = ...
        createLPSteadyStateFluxConstraints(cobraModel, true);
    
    freeEnergyVarsToDrg = freeEnergySpace.standardMvnToDrgMvnTransform;
    
    %% Aggregate all the constraints in a single optimization problem.
    [~, nReactions] = size(cobraModel.S);
    nFreeEnergyVars = numel(freeEnergySpace.lb);
    nFluxConstraints = numel(vRhs);
    nConstraintRxns = size(freeEnergyVarsToDrg.T, 1);
    nVariables = nReactions + nFreeEnergyVars + nConstraintRxns;
    isConstraintRxn = cobraModel.isConstraintRxn;
    
    %% Define gurobi model encoding the stFBA constraints.
    % The variables of the model are [v; freeEnergyVars; direction]
    gurobiModel = addObjective(struct(), ...
        objectiveType, objective, nReactions, nFreeEnergyVars, nVariables);
    gurobiModel.vtype = [ ...
        vVarType, repmat('C', 1, nFreeEnergyVars), ...
        repmat('B', 1, nConstraintRxns)];
    
    gurobiModel.lb = [vMin; freeEnergySpace.lb; zeros(nConstraintRxns, 1)];
    gurobiModel.ub = [vMax; freeEnergySpace.ub; ones(nConstraintRxns, 1)];
    
    % Prepare portions of the constraints matrix.
    ignoreV = zeros(nConstraintRxns, nReactions);
    ignoreFreeEnergyVars = zeros(nConstraintRxns, nFreeEnergyVars);
    fluxBigM = 1.01 * max( ...
        abs([vMin(isConstraintRxn), vMax(isConstraintRxn)]'))';
    fluxEpsilon = max(fluxBigM * 1e-8, settings.fluxEpsilon);
    
    drgBigM = settings.drgMax;
    drgBigMDiagonal = drgBigM * eye(nConstraintRxns);
    vConstrainedDiagonal = eye(nReactions);
    vConstrainedDiagonal = vConstrainedDiagonal(isConstraintRxn, :);
    
    % Rescale the fluxes in order to improve numerics.
    fluxBigMDiagonal = diag(fluxBigM);
    gurobiModel.obj(1:nReactions) = gurobiModel.obj(1:nReactions) ./ fluxScale;
    
    % Create linear constraints.
    gurobiModel.A = sparse([
        vLhs, zeros(nFluxConstraints, nFreeEnergyVars + nConstraintRxns);
        -vConstrainedDiagonal,  ignoreFreeEnergyVars,   fluxBigMDiagonal;
        -vConstrainedDiagonal,  ignoreFreeEnergyVars,   fluxBigMDiagonal;
        ignoreV,                freeEnergyVarsToDrg.T,    drgBigMDiagonal;
        ignoreV,                freeEnergyVarsToDrg.T,    drgBigMDiagonal
    ]);

    gurobiModel.sense = [
        vSense, ... 
        repmat('<', 1, nConstraintRxns), ...
        repmat('>', 1, nConstraintRxns), ...
        repmat('<', 1, nConstraintRxns), ...
        repmat('>', 1, nConstraintRxns)
    ];
    
    gurobiModel.rhs = [
        vRhs;
        fluxBigM - fluxEpsilon;
        fluxEpsilon;
        (drgBigM  - settings.drgEpsilon) * ones(nConstraintRxns, 1) - ...
            freeEnergyVarsToDrg.shift;
        settings.drgEpsilon * ones(nConstraintRxns, 1) - ...
            freeEnergyVarsToDrg.shift
    ];
    gurobiModel = addQuadraticConstraints( ...
        gurobiModel, freeEnergySpace, nVariables, nReactions);
    
    %% Solve MILP.
    params = settings.gurobiSettings;    
    result = gurobi(gurobiModel, params);
    
    switch result.status
        case {'OPTIMAL', 'TIME_LIMIT'}
            % In principle we accept sub-optimal solutions when hitting a time
            % limit. Only complain if no feasible solution has been found yet.
            if ~isfield(result, 'x')
                error(['Time limit reached, but no feasible solution has ' ...
                    'been found yet.']);
            end
            
            % Extract fluxes and thermodynamic variables from the result.
            v = result.x(1:nReactions) ./ fluxScale;
            freeEnergyVars = result.x(nReactions+(1:nFreeEnergyVars));
            drg = freeEnergyVarsToDrg.transform(freeEnergyVars);
            logConc = freeEnergySpace.standardMvnToLogConcMvnTransform ...
                .transform(freeEnergyVars);
            drg0 = freeEnergySpace.standardMvnToDrg0MvnTransform ...
                .transform(freeEnergyVars);
            
            % Sanity check ensuring that we didn't run into precision problems.
            assert(all(sign(v(isConstraintRxn)) ~= sign(drg)), ['The free ' ...
                'energies and the flux directions estimated by the ' ...
                'optimizer are inconsistent. This is likely a numerical ' ...
                'precision issue. Possible solutions include increasing ' ...
                'the precision of the solver or simplifying the model.']);
            assert(all(v(isConstraintRxn) ~= 0), ['At least one flux is ' ...
                'zero, meaning that the reaction doesn''t have a ' ...
                'well-defined direction.']);
        case 'INF_OR_UNBD'
            if settings.diagnoseInfeasibilities
                params.NumericFocus = 3;
                diagnoseInfeasibilities( ...
                    gurobiModel, params, cobraModel, nFreeEnergyVars, fluxScale);
            end
            error(['Optimization terminated with status INF_OR_UNBD. This ' ...
                'is generally an indicator of numerical issues. Does the ' ...
                'model contain thermodynamic inconsistencies?']);
        case 'INFEASIBLE'
            if settings.diagnoseInfeasibilities
                params.NumericFocus = 3;
                diagnoseInfeasibilities( ...
                    gurobiModel, params, cobraModel, nFreeEnergyVars, fluxScale);
            end
            error(['Optimization failed with status INFEASIBLE. This ' ...
                'usually means that the model is thermodynamically ' ...
                'inconsistent (no flux distribution is thermodynamically ' ...
                'feasible).']);
        otherwise
            error('Optimization failed with status %s.', result.status);
    end
end

function model = addObjective(model, objectiveType, objective, nReactions, ...
    nFreeEnergyVars, nVariables)

    tokens = split(objectiveType);
    if numel(tokens) == 1
    	model.modelsense = 'max';
    else
        model.modelsense = tokens{2};
    end

    % Decide in what position of the objective vector (or matrix) we must insert
    % the objective provided by the user.
    switch tokens{1}
        case 'all' 
            objectivePosition = 1:nReactions+nFreeEnergyVars;
        case 'flux'
            objectivePosition = 1:nReactions;
        case 'freeEnergyVars'
            objectivePosition = (nReactions+1):(nReactions+nFreeEnergyVars);
        otherwise
            error('Unsupported objective variable: %s', tokens{1});
    end 
    
    model.obj = zeros(nVariables, 1);
    if isvector(objective)
        % The objective is a linear function.
        model.obj(objectivePosition) = objective;
    else
        % The objective is a quadratic function.
        model.Q = sparse(nVariables, nVariables);
        model.Q(objectivePosition, objectivePosition) = objective;
    end
end

function model = addQuadraticConstraints( ...
    model, freeEnergySpace, nVariables, nReactions)
    
    nFreeEnergyVars = size(freeEnergySpace.standardMvnToDrgMvnTransform.T, 2);
    model.quadcon(1).Qc = sparse(nVariables, nVariables);
    model.quadcon(1).Qc(nReactions+(1:nFreeEnergyVars), ...
                        nReactions+(1:nFreeEnergyVars)) = eye(nFreeEnergyVars);
    model.quadcon(1).q = zeros(nVariables, 1);
    model.quadcon(1).rhs = freeEnergySpace.radiusMultiplier ^ 2;
    model.quadcon(1).name = 'free_energy_vars_ellipsoid';
end

function diagnoseInfeasibilities(model, params, cobraModel, nFreeEnergyVars, ...
    fluxScale)

    fprintf('Model infeasible. Searching for conflicting constraints ...\n');            
    iis = gurobi_iis(model, params);
    fprintf('IIS found. The constraints are:\n');
    [nMetabolites, nReactions] = size(cobraModel.S);
    nConstraintRxns = sum(cobraModel.isConstraintRxn);
    
    fprintf(' - Lower bounds:\n');
    fprintf('   > Fluxes: ');
    lbFluxes = cobraModel.rxns(iis.lb(1:nReactions))';
    fprintf('%s, ', lbFluxes{:}); fprintf('\n');
    fprintf('             ');
    lbs = model.lb(1:nReactions) ./ fluxScale;
    fprintf('%f, ', lbs(iis.lb(1:nReactions))); fprintf('\n');
    fprintf('   > DrG basis: ');
    lbVars = find(iis.lb(nReactions+1:nReactions+nFreeEnergyVars))';
    fprintf('%i, ', lbVars); fprintf('\n');
            
    fprintf(' - Upper bounds:\n');
    fprintf('   > Fluxes: ');
    ubFluxes = cobraModel.rxns(iis.ub(1:nReactions));
    fprintf('%s, ', ubFluxes{:}); fprintf('\n');
    fprintf('             ');
    ubs = model.ub(1:nReactions) ./ fluxScale;
    fprintf('%f, ', ubs(iis.ub(1:nReactions))); fprintf('\n');
    fprintf('   > DrG basis: ');
    ubVars = find(iis.ub(nReactions+1:nReactions+nFreeEnergyVars))';
    fprintf('%i, ', ubVars); fprintf('\n');
            
    fprintf(' - Linear constraints:\n');
    constrainedRxnNames = cobraModel.rxns(cobraModel.isConstraintRxn);
    fprintf('   > Steady state constraints: ');
    steadyStatesMets = cobraModel.mets(iis.Arows(1:nMetabolites))';
    fprintf('%s, ', steadyStatesMets{:}); fprintf('\n');
    fprintf('   > Flux direction constraints (forward): ');
    fluxDirsFwd = constrainedRxnNames(iis.Arows( ...
        nMetabolites+1:nMetabolites+nConstraintRxns))';
    fprintf('%s, ', fluxDirsFwd{:}); fprintf('\n');
    fprintf('   > Flux direction constraints (backward): ');
    fluxDirsBwd = constrainedRxnNames(iis.Arows( ...
        nMetabolites+nConstraintRxns+1:nMetabolites+2*nConstraintRxns))';
    fprintf('%s, ', fluxDirsBwd{:}); fprintf('\n');
    fprintf('   > DrG sign constraints (negative): ');
    drgSignNeg = constrainedRxnNames(iis.Arows( ...
        nMetabolites+2*nConstraintRxns+1:nMetabolites+3*nConstraintRxns))';
    fprintf('%s, ', drgSignNeg{:}); fprintf('\n');
    fprintf('   > DrG sign constraints (positive): ');
    drgSignPos = constrainedRxnNames(iis.Arows( ...
        nMetabolites+3*nConstraintRxns+1:nMetabolites+4*nConstraintRxns))';
    fprintf('%s, ', drgSignPos{:}); fprintf('\n');
            
    fprintf(' - Quadratic constraint (covariance): %i\n', iis.quadcon);
end