# Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
# Computational Systems Biology group, D-BSSE
#
# This software is freely available under the GNU General Public License v3.
# See the LICENSE file or http://www.gnu.org/licenses/ for further information.

import argparse
from equilibrator_api import ComponentContribution, \
    Reaction, Q_
import logging
import numpy as np
import scipy.io as sio
from typing import List, Tuple

def _try_parse_reaction(rxn_string, eq_api=None):
    """Parse a string into a Reaction object.

    Returns none if eQuilibrator cannot provide an estimate for the free energy
    of the reaction.
    """
    try:
        logging.getLogger().setLevel(logging.ERROR)
        reaction = eq_api.parse_reaction_formula(rxn_string)
        if eq_api is not None:
            eq_api.standard_dg_prime(reaction)
        logging.getLogger().setLevel(logging.WARNING)
        return reaction
    except:
        return None

def _try_get_drg0_prime_single(
    rxn : Reaction,
    pH : float,
    I : float,
    T : float,
    pMg : float, 
    eq_api
) -> float:
    """Estimate the transformed standard free energy of a reaction.
    """
    eq_api.p_mg = pMg
    eq_api.p_h = pH
    eq_api.ionic_strength = I
    eq_api.temperature = T

    return eq_api.standard_dg_prime(rxn).value.magnitude

def get_dfg0_prime(
    met_keys : List[str],
    met_pH : np.array,
    met_I : np.array,
    met_T : np.array,
    met_pMg : np.array,
    eq_api 
) -> Tuple[np.array, np.array]:
    """Compute estimates for the formation energies of a list of metabolites.

    The method uses eQuilibrator to return mean as well as covariance of the
    estimates.
    
    Parameters
    ----------
    met_keys : List[str]
        List of n keys identifying the metabolites. These can be in any format
        or namespace supported by eQuilibrator.
    met_pH : np.array
        Vector containing the pH of the compartment of each metabolite.
    met_I : np.array
        Vector containing the ionic strength of the compartment of each 
        metabolite.
    met_T : np.array
        Vector containing the temperature of the compartment of each 
        metabolite.
    met_pMg : np.array
        Vector containing the pMg of the compartment of each metabolite.

    Returns
    -------
    Tuple[np.array, np.array]
        Tuple with a n-dimensional vector containing the estimates of the n
        metabolites and a n-by-n matrix containing the covariance matrix of the
        estimation uncertainty.
    """
    
    # Use half-reactions as a workaropund for eQuilibrator not exposing
    # formation energies explicitely. 
    half_rxn_strings = ['= ' + m for m in met_keys]
    rxns = [_try_parse_reaction(r, eq_api) for r in half_rxn_strings]
    
    # Only work with metabolites for which eQuilibrator can provide an estimate.
    # The remaining metabolites will be handled further down.
    covered_rxn_ids, covered_rxns = map(list, zip(
        *[(i, rxn) for i, rxn in enumerate(rxns) if rxn is not None]))
    missing_rxn_ids = [i for i, rxn in enumerate(rxns) if rxn is None]
    met_pH = [met_pH[i] for i in covered_rxn_ids]
    met_I = [met_I[i] for i in covered_rxn_ids]
    met_T = [met_T[i] for i in covered_rxn_ids]
    met_pMg = [met_pMg[i] for i in covered_rxn_ids]
    
    logging.warning('\n    '.join(
        ['The following metabolites could not be found in eQuilibrator:'] + 
        [met_keys[idx] for idx in missing_rxn_ids]))

    met_count = len(met_keys)
    dfg0_mean = np.zeros((met_count, 1))
    dfg0_cov = np.eye(met_count)

    dfg0_mean[covered_rxn_ids, 0] = np.array([
        _try_get_drg0_prime_single(r, Q_(pH), Q_(I, "M"), Q_(T, "K"), Q_(pMg), eq_api) for \
            r, pH, I, T, pMg in zip(covered_rxns, met_pH, met_I, met_T, met_pMg)])
    dfg0_cov[np.ix_(covered_rxn_ids, covered_rxn_ids)] = \
        eq_api.standard_dg_multi(covered_rxns)[1]

    # Fill estimates for non-covered metabolites with conservative values.
    default_dfg0_mean = np.mean(dfg0_mean[np.ix_(covered_rxn_ids, [0])])
    default_dfg0_cov = np.std(dfg0_mean[np.ix_(covered_rxn_ids, [0])])**2
    dfg0_mean[np.ix_(missing_rxn_ids, [0])] = default_dfg0_mean

    dfg0_cov[missing_rxn_ids, :] = 0
    dfg0_cov[:, missing_rxn_ids] = 0
    dfg0_cov[missing_rxn_ids, missing_rxn_ids] = default_dfg0_cov

    # Enforce correlation between multiple occurrences of the same metabolite in
    # different compartments when eQuilibrator doesn't recognize the metabolite.
    for rxn_idx in missing_rxn_ids:
        occurrences_ids = [i for i, rxn in \
            enumerate(half_rxn_strings) if rxn == half_rxn_strings[rxn_idx]]
        for i in occurrences_ids:
            for j in occurrences_ids:
                dfg0_cov[i, j] = default_dfg0_cov

    return dfg0_mean, dfg0_cov

def check_reactions_balance(rxn_strings : List[str]) -> bool:
    """Verify that all the reactions in the list are balanced.

    This method can be used an additional check to verify the correctness of
    eQuilibrator's parsing functions.
    
    Parameters
    ----------
    rxn_strings : List[str]
        List of reaction strings. These can be in any format or namespace
        supported by eQuilibrator.
    
    Returns
    -------
    bool
        True if all recognized reactions are balanced, false otherwise.
    """
    # First, parse the reactions and warn about the ones that are not recognized
    # by eQuilibrator.
    rxns = [_try_parse_reaction(r) for r in rxn_strings]
    logging.getLogger().setLevel(logging.WARNING)
    missing_rxn_ids = [i for i, r in enumerate(rxns) if r is None]
    logging.warning('\n    '.join(
        ['The following reactions were not recognized by eQuilibrator:'] +  
        [rxn_strings[idx] for idx in missing_rxn_ids]))
    
    # Then use eQuilibrator to check the belancing of the recognized reactions.
    logging.getLogger().setLevel(logging.ERROR)
    unbalanced_rxn_ids = [idx for idx, rxn in enumerate(rxns) if \
        rxn is not None and not rxn.is_balanced()]
    logging.getLogger().setLevel(logging.WARNING)
    logging.warning('\n    '.join(
        ['The following reactions are unbalanced, probably because of ' \
            'incorrect metabolite mappings:'] +
        [rxn_strings[idx] for idx in unbalanced_rxn_ids]))

    return len(unbalanced_rxn_ids) == 0

if __name__ == '__main__':
    # Parse arguments.
    parser = argparse.ArgumentParser(
        description = 'Get estimates of the Gibbs formation energies.')
    parser.add_argument('input_file_name', help='Path to the input file.')
    parser.add_argument('output_file_name', help='Path to the input file.')
    logging.getLogger().setLevel(logging.WARNING)
    args = parser.parse_args()

    #initialize Component contribution Mg+
    eq_api = ComponentContribution()

    eq_api.rmse_inf=Q_("3000 kJ/mol")

    # Read inputs.
    inputMat = sio.loadmat(args.input_file_name, squeeze_me=True)
    met_strings = inputMat['metStrings'].tolist()
    rxn_strings = inputMat['rxnStrings'].tolist()
    
    # Verify reaction balance and estimate formation energies.
    is_balanced = True
    # is_balanced = check_reactions_balance(rxn_strings)
    dfg0_mean, dfg0_cov = get_dfg0_prime(
        met_strings, inputMat['metpH'], inputMat['metI'], inputMat['metT'],
        inputMat['metpMg'], eq_api)

    # Store result in the output file.
    sio.savemat(args.output_file_name, {
        'isBalanced' : is_balanced,
        'dfg0Mean' : dfg0_mean,
        'dfg0Cov' : dfg0_cov,
    })