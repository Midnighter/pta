function [compressedModel, rxnsMap, metsMap] = compressCobraModel( ...
    model, protectedRxns, protectedMets)
% COMPRESSCOBRAMODEL Reduce the given COBRA model using the CNAcompressMFNetwork
% method provided by CellNetAnalyzer.
%
%       [compressedModel, rxnsMap, metsMap] = compressCobraModel( ...
%           compressedModel, [], 3:7);
%
%   Required:
%       model:          The COBRA model that must be compressed.
%
%   Optional:
%       protectedRxns:  Indices of protected reactions that should not be
%                       compressed.
%       protectedMets:  Indices of protected metabolites that should not be
%                       compressed.
%
%   Returns:
%       reducedModel:   COBRA model representing the compressed network.
%       reactionsMap:   Map from the indices of the reactions in the reduced
%                       model to indices of reactions in the original model.
%       metabolitesMap: Map from the indices of the metabolites in the reduced
%                       model to indices of metabolites in the original model.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    if nargin < 2
        protectedRxns = [];
    end
    if nargin < 3
        protectedMets = [];
    end

    %% Make sure that rational arithmetic routines are available.
    startcna(1);
    try
        ch.javasoft.metabolic.compress.CompressionMethod.STANDARD;
    catch
        error(['Unable to find metabolic-efm-all.jar (efmtool). It is ' ...
            'strongly recommended that CNA can use it in order to  avoid ' ...
            'numerical issues.']);
    end
    
    %% Compress model using NetwokReducer.
    evalc('cnaModel = CNAcobra2cna(model)'); % Use evalc to suppress output.
    [~, ~, rxnsMap, metsMap, reducedCnaModel] = CNAcompressMFNetwork( ...
        cnaModel, protectedRxns, protectedMets, 0, 0, 1, [], true);
    compressedModel = CNAcna2cobra(reducedCnaModel);
    fprintf(['Model compressed successfully: %i metabolites, %i ' ...
        'reactions.\n'], numel(compressedModel.mets), ...
        numel(compressedModel.rxns));
    
    %% Rescale model to the original biomass.
    isOriginalBiomassRxn = contains(model.rxns, 'BIOMASS');
    biomassRxnIdx = find(contains(compressedModel.rxns, 'BIOMASS'));
    biomassScaling = rxnsMap(isOriginalBiomassRxn, biomassRxnIdx);
    
    rxnsMap(:, biomassRxnIdx) = rxnsMap(:, biomassRxnIdx) / biomassScaling;
    compressedModel.S(:, biomassRxnIdx) = ...
        compressedModel.S(:, biomassRxnIdx) / biomassScaling;
    compressedModel.lb(biomassRxnIdx) = ...
        compressedModel.lb(biomassRxnIdx) * biomassScaling;
    compressedModel.ub(biomassRxnIdx) = ...
        compressedModel.ub(biomassRxnIdx) * biomassScaling;
    compressedModel.rxns{biomassRxnIdx} = 'BIOMASS';
    
    %% Copy/map non standard fields to the compressed model.
    if isfield(model, 'metCharges')
        compressedModel.metCharges = model.metCharges(metsMap);
    end
    if isfield(model, 'metFormulas')
        compressedModel.metFormulas = model.metFormulas(metsMap);
    end
    
    if isfield(model, 'vMean') && isfield(model, 'vCov')
        % Probabilistic flux data are more complicated. First check that no two
        % reactions with different measurements are lumped together.
        oldRxnIds = find(isfinite(model.vMean));
        newRxnIds = zeros(size(oldRxnIds));
        assert(all(sum(rxnsMap(oldRxnIds, :) ~= 0, 2) == 1), ['Reactions ' ...
            'with probabilistic flux estimates must map to exactly one ' ...
            'reaction in the compressed model.']);
        % Now we can map the data.
        nCompressedRxns = numel(compressedModel.rxns);
        compressedModel.vMean = nan(nCompressedRxns, 1);
        compressedModel.vCov = nan(nCompressedRxns, nCompressedRxns);
        for iRxn = 1:numel(oldRxnIds)
            iOldRxn = oldRxnIds(iRxn);
            newRxnIds(iRxn) = find(rxnsMap(iOldRxn, :));
        end
        compressedModel.vMean(newRxnIds) = model.vMean(oldRxnIds);
        compressedModel.vCov(newRxnIds, newRxnIds) = ...
            model.vCov(oldRxnIds, oldRxnIds);
    end
    
    %% Restore original objective function.
    if isfield(compressedModel, 'osense')
        compressedModel = rmfield(compressedModel, 'osense');
    end
    compressedModel.osenseStr = 'max';
    compressedModel.c( ...
        contains(compressedModel.rxns, model.rxns(model.c ~= 0))) = 1;
end