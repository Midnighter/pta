function polytope = cobraModelToPolytope(model)
% COBRAMODELTOPOLYTOPE Create a polytope describing the flux space of a COBRA
% model.
%
%       polytope = cobraModelToPolytope(model);
%
%   Required:
%       model:      Model description in the format used by the COBRA toolbox.
%
%   Returns:
%       polytope:   Polytope representation of the flux space of specified
%                   model.
%
%   See also: POLYTOPE

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    nMetabolites = size(model.S, 1);
    [lb, ub] = fluxVariability(model, 0);
    
    implicationThreshold = 1e-4;
    isImpliedLb = abs((lb - model.lb) ./ model.lb) > implicationThreshold;
    isImpliedUb = abs((ub - model.ub) ./ model.ub) > implicationThreshold;
    % lb(isImpliedLb) = nan;
    % ub(isImpliedUb) = nan;
    
    [A, b, G, h] = samply.createPolytopeConstraintsFromBounds(lb, ub);
    polytope = samply.Polytope([model.S; A], [zeros(nMetabolites, 1); b], G, h);
end