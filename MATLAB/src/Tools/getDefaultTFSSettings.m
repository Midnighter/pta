function [drgSamplingSettings, fluxSamplingSettings] = getDefaultTFSSettings(outputFolder)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    drgSamplingSettings.cache.file = '';
    drgSamplingSettings.cache.useInitialPoints = false;
    drgSamplingSettings.cache.useDirectionsDistribution = false;

    drgSamplingSettings.adaptiveDirectionsDistribution.enabled = false;
	drgSamplingSettings.adaptiveDirectionsDistribution.nIterations = 10;
	drgSamplingSettings.adaptiveDirectionsDistribution.nStepsPerIteration = 20000;
    drgSamplingSettings.adaptiveDirectionsDistribution.stepsMultiplier = 1.3;
    drgSamplingSettings.adaptiveDirectionsDistribution.outputFolder = fullfile(outputFolder, "adaptive_directions_distribution");
    drgSamplingSettings.adaptiveDirectionsDistribution.guessScaleMultiplier = 1;
    drgSamplingSettings.adaptiveDirectionsDistribution.updateScaleMultiplier = 0.1;

    drgSamplingSettings.samplerSettings.nSamples = 2000;
	drgSamplingSettings.samplerSettings.nDirectionSamples = 100000;
	drgSamplingSettings.samplerSettings.nChains = 32;
	drgSamplingSettings.samplerSettings.nSteps = 1000000;
	drgSamplingSettings.samplerSettings.nWarmupSteps = nan;
	drgSamplingSettings.samplerSettings.nRecordedSteps = 400;
	drgSamplingSettings.samplerSettings.maxWorkers = inf;
	drgSamplingSettings.samplerSettings.logsDir = fullfile(outputFolder, "logs/tfs");
	drgSamplingSettings.samplerSettings.consoleLoggingIntervalMs = 1000;
    drgSamplingSettings.samplerSettings.feasibilityCacheSize = 10000;

    drgSamplingSettings.samplerSettings.maxTestedRxnsForInitialPoints = inf;
	drgSamplingSettings.samplerSettings.fluxEpsilon = 1e-4;
    drgSamplingSettings.samplerSettings.drgEpsilon = 1e-1;
    drgSamplingSettings.samplerSettings.drgMax = 500;
	drgSamplingSettings.samplerSettings.minRelRegionCoverage = 1e-8;
	drgSamplingSettings.samplerSettings.diagnoseInfeasibilities = false;

	drgSamplingSettings.samplerSettings.gurobiSettings.outputflag = 0;
	drgSamplingSettings.samplerSettings.gurobiSettings.IntFeasTol = 1e-9;
	drgSamplingSettings.samplerSettings.gurobiSettings.FeasibilityTol = 1e-9;
    drgSamplingSettings.samplerSettings.gurobiSettings.TimeLimit = 240;
    drgSamplingSettings.samplerSettings.gurobiSettings.Threads = 1;

    drgSamplingSettings.convergenceDiagnostic.enabled = true;
    drgSamplingSettings.convergenceDiagnostic.outputFolder = fullfile(outputFolder, "convergence/tfs");
    drgSamplingSettings.convergenceDiagnostic.tests = ["splitR", "ESS"];
    
    fluxSamplingSettings.enabled = true;
    fluxSamplingSettings.method = "TwoStages";
    fluxSamplingSettings.nOrthants = 100;
    fluxSamplingSettings.nApproxSamples = 10000;
        
    fluxSamplingSettings.samplerSettings.nSamples = nan;
    fluxSamplingSettings.samplerSettings.nChains = 1;             
    fluxSamplingSettings.samplerSettings.nSteps = nan;
    fluxSamplingSettings.samplerSettings.nWarmupSteps = nan;
    fluxSamplingSettings.samplerSettings.nRecordedSteps = nan;
    fluxSamplingSettings.samplerSettings.maxWorkers = inf;
    fluxSamplingSettings.samplerSettings.logsDir = "${root}/${condition}/logs/chrr_orthants";
    fluxSamplingSettings.samplerSettings.consoleLoggingIntervalMs = 1000;

    fluxSamplingSettings.convergenceDiagnostic.enabled = false;
end