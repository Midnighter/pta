function isInternal = isInternalReaction(S)
% ISINTERNALREACTIONS finds the internal reactions of the network defined by
% the stoichiometric matrix S. Internal reactions are reactions that involve at
% least two metabolites.
%
%   isInternal = findInternalReactions(S);
%
%   Required:
%       S:          The m-by-r stoichiometric matrix of the network.
%
%   Returns:
%       isInternal: 1-by-r logical array determining whether the i-th reaction
%                   is internal or not.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    isInternal = (sum(S ~= 0) > 1)';
end