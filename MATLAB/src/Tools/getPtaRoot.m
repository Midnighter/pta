function root = getPtaRoot()
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    thisScriptPath = fileparts(which('getPtaRoot.m'));
    root = fullfile(thisScriptPath, '..', '..', '..');
end

