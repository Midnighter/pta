function runThermodynamicModelAssessment(model, outputFolder, ...
    samplingSettings, FESsettings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    nVarsToPrint = 10000;
    [~, ~] = mkdir(outputFolder);
    rxnNames = model.rxns(model.isConstraintRxn);
    nReactions = numel(rxnNames);
    nMetabolites = numel(model.mets);
    
    %% Construct free energy space and optimize for the probability.
    FESsettings.sampleConcentrations = true;
    freeEnergySpace = GibbsFreeEnergiesSpace(model, FESsettings);
    nVariables = numel(freeEnergySpace.lb); 
    samplingSettings.diagnoseInfeasibilities = true;
    [~, vars] = PMO('freeEnergyVars min', eye(nVariables), model, ...
        freeEnergySpace, samplingSettings);

    %% Rotate the variables to the thermodynamic parameters.
    rotatedValues = freeEnergySpace.varsRotation * vars;
    drg = freeEnergySpace.standardMvnToDrgMvnTransform.transform(vars);
    drg0 = freeEnergySpace.standardMvnToDrg0MvnTransform.transform(vars);
    concentrations = exp( ...
        freeEnergySpace.standardMvnToLogConcMvnTransform.transform(vars));
   
    %% Print reaction energies ranked by deviation from the prior.
    fileID = fopen(fullfile(outputFolder, 'drg.txt'), 'w');
    [~, highestDrgDeviationsIds] = maxk( ...
        abs(rotatedValues(1:nReactions)), nVarsToPrint);
    for iRxn = highestDrgDeviationsIds'
        fprintf(fileID, '% 8.3f  % 8.3f  ', rotatedValues(iRxn), drg(iRxn));
        printRxnFormula(model, 'rxnAbbrList', rxnNames(iRxn), 'fid', fileID);
    end
    fclose(fileID);
    
    if ~freeEnergySpace.hasExplicitConcentrationAndDrg0
        return;
    end
    
    %% Print standard reaction energies ranked by deviation from the prior.
    fileID = fopen(fullfile(outputFolder, 'drg0.txt'), 'w');
    [~, highestDrg0DeviationsIds] = maxk( ...
        abs(rotatedValues(nReactions+nMetabolites+1:end)), nVarsToPrint);
    for iRxn = highestDrg0DeviationsIds'
        iValue = nReactions + nMetabolites + iRxn;
        fprintf(fileID, '% 8.3f  % 8.3f  ', rotatedValues(iValue), drg0(iRxn));
        printRxnFormula(model, 'rxnAbbrList', rxnNames(iRxn), 'fid', fileID);
    end
    fclose(fileID);
    
    %% Print concentrations ranked by deviation from the prior.
    fileID = fopen(fullfile(outputFolder, 'concentrations.txt'), 'w');
    [~, highestConcDeviationsIds] = maxk( ...
        abs(rotatedValues(nReactions+1:nReactions+nMetabolites)), nVarsToPrint);
    for iMet = highestConcDeviationsIds'
        iValue = nReactions + iMet;
        fprintf(fileID, '% 8.3f  % 8.3e     %s\n', rotatedValues(iValue), ...
            concentrations(iMet), model.mets{iMet});
    end
    fclose(fileID);
end