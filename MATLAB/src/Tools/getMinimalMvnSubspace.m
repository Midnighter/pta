function [standardMvnToMvnTransform, varsScaling, varsRotation] = ...
    getMinimalMvnSubspace(covariance, mean, dimensionTruncationThreshold, ...
    maxEigenvalue)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
 
    if ~exist('maxEigenvalue', 'var')
       maxEigenvalue = 1e7; 
    end
    
    %% Reduce the variables to a minimal subspace.
    [V, D] = eig(covariance);
    [eigenvalues, indices] = sort(diag(D));
    V = V(:, indices);
    
    assert(all(abs(imag(eigenvalues)) < dimensionTruncationThreshold) && ...
        all(all(abs(imag(V * real(eigenvalues))) < dimensionTruncationThreshold)), [ ...
        'The covariance matrix of the MVN has imaginary eigenvalues or ' ...
        'eigenvectors, meaning that it is not SPD and thus not a valid ' ...
        'covariance matrix.']);
    assert(eigenvalues(1) > -dimensionTruncationThreshold, ['The ', ...
        'covariance matrix of the MVN has megative entries, meaning that ' ...
        'it is not SPD and thus not a valid covariance matrix.']);
    V = real(V);
    eigenvalues = real(eigenvalues);
    eigenvalues(eigenvalues > maxEigenvalue) = maxEigenvalue;
    
    % Compute transformation from the unit hypersphere to the full-ranksubspace.
    isNotTruncatedDimension = eigenvalues >= dimensionTruncationThreshold;
    truncatedV = V(:, isNotTruncatedDimension);
    truncatedD = diag(eigenvalues(isNotTruncatedDimension));
        
    %% Map concentrations and standard formation energies to reaction energies.
    standardMvnToMvnTransform = samply.AffineTransform( ...
        truncatedV * sqrt(truncatedD), mean);
    standardMvnToMvnTransform.T(abs(standardMvnToMvnTransform.T) < 1e-6) = 0;
    
    varsScaling = sqrt(truncatedD);
    varsRotation = truncatedV;
end

