function h5WriteMatrixDataset(fileId, name, data)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    typeId = H5T.copy('H5T_IEEE_F64LE');
    
    dimensions = fliplr(size(data));
    spaceId = H5S.create_simple(2, dimensions, dimensions);
    
    if H5L.exists(fileId, name, 'H5P_DEFAULT')
        H5L.delete(fileId, name,'H5P_DEFAULT');
    end
    
    datasetId = H5D.create(fileId, name, typeId, spaceId, 'H5P_DEFAULT');
    H5D.write(datasetId, typeId, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data);
    
    H5S.close(spaceId);
    H5T.close(typeId);
    H5D.close(datasetId);
end

