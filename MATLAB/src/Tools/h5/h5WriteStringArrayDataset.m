function h5WriteStringArrayDataset(fileId, name, data)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    typeId = H5T.copy('H5T_C_S1');
    H5T.set_size(typeId, 'H5T_VARIABLE');
    
    spaceId = H5S.create_simple(1, numel(data), numel(data));
    
    propertyList = H5P.create('H5P_DATASET_CREATE');
    H5P.set_chunk(propertyList, 1);
    
    if H5L.exists(fileId, name, 'H5P_DEFAULT')
        H5L.delete(fileId, name,'H5P_DEFAULT');
    end
    
    datasetId = H5D.create(fileId, name, typeId, spaceId, 'H5P_DEFAULT');
    H5D.write(datasetId, typeId, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data);
    
    H5P.close(propertyList);
    H5S.close(spaceId);
    H5T.close(typeId);
    H5D.close(datasetId);
end

