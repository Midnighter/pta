function printCoefficientsRange(A)
% PRINTCOEFFICIENTSRANGE Print the range of coefficients (absolute values) of
% the given matrix.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    absCoefficients = full(abs(A(A ~= 0)));
    fprintf('Matrix coefficient range: [%.1e, %.1e]\n', ...
        min(absCoefficients), max(absCoefficients));
end

