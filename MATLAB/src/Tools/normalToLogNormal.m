function [logScaleMean, logScaleStd] = normalToLogNormal(normalMean, normalStd)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
  
    samples = max(normrnd(normalMean, normalStd, 10000, 1), 1e-10);
    logSamples = log(samples);
    logScaleMean = mean(logSamples);
    logScaleStd = std(logSamples);
end