function couplings = findEnzymeSubsets(model)
% FINDENZYMESUBSETS Finds the enzyme subsets of the reaction network described
% by the given stoichiometric matrix.
%
%   Required:
%       S:          m-by-n stoichiometric matrix of the reactions network.
%
%   Returns:
%       subsetIdx:  1-by-n vector containing the index of the enzyme subset of
%                   each reaction.
%

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    toSwitch = model.ub <= 0;
    tmp = model.lb(toSwitch);
    model.lb(toSwitch) = -model.ub(toSwitch);
    model.ub(toSwitch) = -tmp;
    model.S(:, toSwitch) = -model.S(:, toSwitch);
    
    network.stoichiometricMatrix = full(model.S);
    network.reversibilityVector = model.lb < 0;
    network.Reactions = model.rxns;
    network.Metabolites = model.mets;
    [nonBlockedCouplings, blocked] = F2C2('glpk', network);
    couplings = zeros(numel(model.rxns));
    couplings(~blocked, ~blocked) = nonBlockedCouplings;
    
    couplings(couplings > 1) = 0;
    
    %% Find the relative directions within each coupled set of reactions.
    for iRxn = 1:numel(model.rxns)
        if abs(model.ub(iRxn)) > abs(model.lb(iRxn))
            direction = 'max';
        else
            direction = 'min';
        end
        model.c = zeros(size(model.c));
        model.c(iRxn) = 1;
        result = optimizeCbModel(model, direction);
        couplings(:, iRxn) = couplings(:, iRxn) .* sign(result.x);
    end
end