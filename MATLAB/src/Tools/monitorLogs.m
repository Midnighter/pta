function monitorLogs(folder)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    folder = strrep(folder, '\', filesep);
    folder = strrep(folder, '/', filesep);
    relativePath = relativepath(char(folder), pwd);
    command = ['bash -c ''watch -n 1 tail -q -n 1 ', relativePath, '/*.log'' &'];
    command = strrep(command, '\', '/');
    system(command);
end