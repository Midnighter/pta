function isSteadyState = isSteadyStateFluxDirection(directions, S, vMin, vMax, isConstraint)
% ISSTEADYSTATEFLUXDIRECTION Verifies whether the given flux directions are
% feasible at steady state in a reaction network.
%
%   Required:
%       directions: n-by-s matrix  of element in {-1, 1} containing s direction
%                   distributions inside the network.
%       S:          m-by-n stoichiometric matrix of the system.
%       vMin:       n-by-1 vector of lower bounds for the reaction fluxes.
%       vMax:       n-by-1 vector of upper bounds for the reaction fluxes.
%
%   Returns:
%       isSteadyState: 1-by-s vector of logicals determining which directions
%                   are feasible at steady state.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    nSamples = size(directions, 2);
    [nMetabolites, nReactions] = size(S);
    isSteadyState = false(1, nSamples);
    tmpDir = zeros(nReactions, nSamples);
    tmpDir(isConstraint, :) = directions;
    directions = tmpDir;
    
    parfor iSample = 1:nSamples
        % Setup COBRA model for the given reaction network and constain it with
        % the current flux directions distribution.
        model = struct();
        model.lb = vMin;
        model.lb(directions(:, iSample) > 0 & model.lb < 0) = 0;
        model.ub = vMax;
        model.ub(directions(:, iSample) < 0 & model.ub > 0) = 0;
        model.obj = zeros(nReactions, 1);
        model.A = sparse(S);
        model.rhs = zeros(nMetabolites, 1);
        model.sense = repmat('=', 1, nMetabolites);

        params = struct();
        params.outputflag = 0;
        params.MIPGap = 1e-12;
        params.IntFeasTol = 1e-9;
        params.FeasibilityTol = 1e-9;
        params.OptimalityTol = 1e-9;

        % Verify whether at least one solution exists in the constrained space.
        result = gurobi(model, params);
        if strcmp(result.status, 'OPTIMAL')
            isSteadyState(iSample) = true;
        end
    end
end