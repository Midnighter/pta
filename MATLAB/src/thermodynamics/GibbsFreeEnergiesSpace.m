classdef GibbsFreeEnergiesSpace < samply.Ellipsoid
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

properties (SetAccess = private)
    standardMvnToObservablesMvnTransform;
    
    observablesToDrgTransform;
    observablesToDrg0Transform;
    observablesToLogConcTransform;
    
    standardMvnToDrgMvnTransform;  
    standardMvnToDrg0MvnTransform;  
    standardMvnToLogConcMvnTransform;  
    
    observablesCov;
    observablesMean;
    
    varsScaling;
    varsRotation;
    
    confidence;
    lb;
    ub;
    nMetabolites;
    radiusMultiplier;
    RT;
    
    isConstraintReaction;
    hasExplicitConcentrationAndDrg0;
end

methods
    function self = GibbsFreeEnergiesSpace(model, settings)
        
        import samply.*
        
        assert(all(model.metsT == model.metsT(1)), ...
            'The entire system must be modeled at the same temperature.');
        RT = model.metsT(1) * 8.314e-3;
        isConstraintRxn = model.isConstraintRxn == 1;
        drgCorrection = model.drgCorr;
        S = model.S;
        
        % model = GibbsFreeEnergiesSpace.adjustLargePriors(model, 5.02e+05);
        
        %% Create free vairables and their mappings to observables.        
        if settings.sampleConcentrations
            % Observables layout:
            %   - DrG
            %   - logConc
            %   - DrG0
            nMetabolites = numel(model.logConcMean);
            nDrg = sum(isConstraintRxn);
            
            %% Standard minimal subspace for DrG0.
            dfg0ToDrg0Transform = AffineTransform( ...
                S(:, isConstraintRxn)', drgCorrection(isConstraintRxn));
            lnConcToDrgCorrTransform = AffineTransform( ...
                RT * S(:, isConstraintRxn)', zeros(nDrg, 1));
            
            [stdMvnToDrg0MvnTransform, drg0varsScaling, drg0VarsRotation] = ...
            GibbsFreeEnergiesSpace.createMinimalSubspaceForObservables( ...
                model.dfg0Cov, model.dfg0Mean, dfg0ToDrg0Transform, ...
                settings.minDimensionEigenvalue);
            nDrg0Vars = size(drg0VarsRotation, 2);
            
            %% Standard minimal subspace for logConc.
            assert(isdiag(model.logConcCov), ...
                'This implementation assumes uncorrelated concentrations.');
            logConcVarsScaling = sqrt(model.logConcCov);
            logConcVarsRotation = eye(nMetabolites);
            
            % Remove variables corresponding to metabolites with no uncertainty.
            isUnnecessaryLogConcVariable = diag(logConcVarsScaling) == 0;
            logConcVarsScaling = logConcVarsScaling( ...
                ~isUnnecessaryLogConcVariable, ~isUnnecessaryLogConcVariable);
            logConcVarsRotation = logConcVarsRotation(:, ~isUnnecessaryLogConcVariable);
            nMetaboliteVars = sum(~isUnnecessaryLogConcVariable);
            
            stdMvnToLogConcTransform = AffineTransform( ...
                logConcVarsRotation * logConcVarsScaling, model.logConcMean);
            stdMvnToDrg0CorrTransform = ...
                lnConcToDrgCorrTransform.applyTransform(stdMvnToLogConcTransform);
            
            %% Combine variables in a single MVN.
            % The variables vector is [Base of Drg0, base of logConc].
            % The observable variables vector is [DrG, logConc, DrG0].
            stdMvnToObservablesMvnTransform = AffineTransform( ...
                [stdMvnToDrg0MvnTransform.T, stdMvnToDrg0CorrTransform.T;
                 zeros(nMetabolites, nDrg0Vars), stdMvnToLogConcTransform.T;
                 stdMvnToDrg0MvnTransform.T, zeros(nDrg, nMetaboliteVars)], ...
                [stdMvnToDrg0MvnTransform.shift + stdMvnToDrg0CorrTransform.shift;
                 stdMvnToLogConcTransform.shift;
                 stdMvnToDrg0MvnTransform.shift]);
             
             varsScaling = [ ...
                 drg0varsScaling, zeros(size(drg0varsScaling, 1), size(logConcVarsScaling, 2));
                 zeros(size(logConcVarsScaling, 1), size(drg0varsScaling, 2)), logConcVarsScaling];
             varsRotation = [ ...
                 drg0VarsRotation, RT * S(:, isConstraintRxn)' * logConcVarsRotation;
                 zeros(nMetabolites, nDrg0Vars), logConcVarsRotation;
                 drg0VarsRotation, zeros(nDrg, nMetaboliteVars)];
             % columnNorms = vecnorm(varsRotation);
             % varsScaling = varsScaling * diag(columnNorms);
             % varsRotation = varsRotation * diag(1 ./ columnNorms);
        else
            [priorCov, priorMean] = GibbsFreeEnergiesSpace.createPrior( ...
                model.dfg0Mean, model.dfg0Cov, model.logConcMean, model.logConcCov);
            nMetabolites = numel(model.logConcMean);
            nDrg = sum(isConstraintRxn);
            priorToDrg = GibbsFreeEnergiesSpace.createPriorsToDrgTransform( ...
                RT, S, isConstraintRxn, drgCorrection);
            priorToObservables = ...
                AffineTransform(priorToDrg.T, priorToDrg.shift);
            
            [stdMvnToObservablesMvnTransform, varsScaling, varsRotation] = ...
            GibbsFreeEnergiesSpace.createMinimalSubspaceForObservables( ...
                priorCov, priorMean, priorToObservables, ...
                settings.minDimensionEigenvalue);
        end
       
        nVariables = size(stdMvnToObservablesMvnTransform.T, 2);
        self@samply.Ellipsoid(eye(nVariables), zeros(nVariables, 1));
        
        self.hasExplicitConcentrationAndDrg0 = settings.sampleConcentrations;
        self.radiusMultiplier = ...
            sqrt(chi2inv(settings.freeEnergyVarsConfidence, nVariables));
        self.lb = -Inf(nVariables, 1);
        self.ub = Inf(nVariables, 1);
        
        self.confidence = settings.freeEnergyVarsConfidence;
        self.nMetabolites = numel(model.dfg0Mean);
        self.isConstraintReaction = isConstraintRxn;
        self.standardMvnToObservablesMvnTransform = ...
            stdMvnToObservablesMvnTransform;
        self.RT = RT;
      
        if settings.sampleConcentrations
            self.observablesToDrgTransform = AffineTransform( ...
                [eye(nDrg), zeros(nDrg, nMetabolites), zeros(nDrg)], zeros(nDrg, 1));
            self.observablesToDrg0Transform = AffineTransform( ...
                [zeros(nDrg), zeros(nDrg, nMetabolites), eye(nDrg)], zeros(nDrg, 1));
            self.observablesToLogConcTransform = AffineTransform( ...
                [zeros(nMetabolites, nDrg), eye(nMetabolites), zeros(nMetabolites, nDrg)], ...
                zeros(nMetabolites, 1));
        else
            self.observablesToDrgTransform = ...
                AffineTransform(eye(nDrg), zeros(nDrg, 1));
            self.observablesToDrg0Transform = ...
                AffineTransform(eye(nDrg), zeros(nDrg, 1));
            self.observablesToLogConcTransform = AffineTransform( ...
                zeros(nMetabolites, nDrg), zeros(nMetabolites, 1));
        end
       
        self.standardMvnToDrgMvnTransform = self.observablesToDrgTransform ...
            .applyTransform(self.standardMvnToObservablesMvnTransform);
        self.standardMvnToDrg0MvnTransform = self.observablesToDrg0Transform ...
            .applyTransform(self.standardMvnToObservablesMvnTransform);
        self.standardMvnToLogConcMvnTransform = self.observablesToLogConcTransform ...
            .applyTransform(self.standardMvnToObservablesMvnTransform);
        
        self.varsScaling = varsScaling;
        self.varsRotation = varsRotation;
        [self.observablesCov, self.observablesMean] = ...
            self.getObservablesMeanAndCov(model, self.RT);
    end
    
    function concentrations = getConcentrations(self, point)
        pointToConcentrations = ...
            self.observablesToLogConcTransform.applyTransform( ...
                self.standardMvnToObservablesMvnTransform);
        concentrations = exp(pointToConcentrations.transform(point));
    end
    
    function [lb, ub] = getThermodynamicBounds( ...
            self, confidenceForIrreversibility)
        drgCov = self.standardMvnToDrgMvnTransform.T * ...
            self.standardMvnToDrgMvnTransform.T';
        drgStd = sqrt(diag(drgCov));
        drgMean = self.standardMvnToDrgMvnTransform.shift;
        confidenceMultiplier = ...
            sqrt(chi2inv(confidenceForIrreversibility, 1));
        
        drgLb = -inf(size(self.isConstraintReaction));
        drgUb = inf(size(self.isConstraintReaction));
        drgLb(self.isConstraintReaction) = ...
            drgMean - drgStd * confidenceMultiplier;
        drgUb(self.isConstraintReaction) = ...
            drgMean + drgStd * confidenceMultiplier;
        
        lb = -inf(size(self.isConstraintReaction));
        ub = inf(size(self.isConstraintReaction));
        lb(drgUb < 0) = 0;
        ub(drgLb > 0) = 0;
    end
end

methods (Static, Access = private)
    
    function [observablesCov, observablesMean] = getObservablesMeanAndCov(model, RT)
        
        import samply.*
        
        isConstraintRxn = model.isConstraintRxn == 1;
        drgCorrection = model.drgCorr;
        S = model.S;
        nMetabolites = numel(model.logConcMean);
        nDrg = sum(isConstraintRxn);
            
        %% Covariance and mean for DfG0 and logConc.
        dfg0AndLogConcCov = [
            model.dfg0Cov, zeros(nMetabolites, nMetabolites);
            zeros(nMetabolites, nMetabolites), model.logConcCov];
        dfg0AndLogConcMean = [model.dfg0Mean; model.logConcMean];
            
        %% Mapping to observables
        toDrgTransform = AffineTransform( ...
            [S(:, isConstraintRxn)', RT * S(:, isConstraintRxn)'], ...
            drgCorrection(isConstraintRxn));
        toLogConcTransform = AffineTransform( ...
            [zeros(nMetabolites), eye(nMetabolites)], ...
            zeros(nMetabolites, 1));
        toDrg0Transform = AffineTransform( ...
            [S(:, isConstraintRxn)', zeros(nDrg, nMetabolites)], ...
            drgCorrection(isConstraintRxn));
            
        toObservablesTransform = AffineTransform( ...
            [toDrgTransform.T; toLogConcTransform.T; toDrg0Transform.T], ...
            [toDrgTransform.shift; toLogConcTransform.shift; ...
                toDrg0Transform.shift]);
                
        %% Find observables MVN and minimal subspace.
        observablesCov = toObservablesTransform.T * dfg0AndLogConcCov * ...
            toObservablesTransform.T';
        observablesMean = ...
            toObservablesTransform.transform(dfg0AndLogConcMean);
    end
    
    function model = adjustLargePriors(model, maxCovariance)
        Sint = model.S(:, model.isConstraintRxn);
        drg0Cov = Sint' * model.dfg0Cov * Sint;
        isLargeDrg0 = diag(drg0Cov) > maxCovariance;
        isLargeDfg0 = diag(model.dfg0Cov) > maxCovariance;
        
        isToAdjust = isLargeDfg0 & any(Sint(:, isLargeDrg0), 2);
        model.dfg0Cov(isToAdjust, :) = 0;
        model.dfg0Cov(:, isToAdjust) = 0;
        model.dfg0Cov(isToAdjust, isToAdjust) = eye(sum(isToAdjust)) * maxCovariance;
    end
        
    function [priorCov, priorMean] = createPrior( ...
            dfg0Mean, dfg0Cov, lnActivityMean, lnConcentrationCov)
        nMetabolites = numel(dfg0Mean);
        priorCov = [
            dfg0Cov, zeros(nMetabolites);
            zeros(nMetabolites), lnConcentrationCov];
        priorMean = [dfg0Mean; lnActivityMean];    
    end
    
    function priorToDfg0 = createPriorsToDfg0Transform(nMetabolites)
        import samply.*
        priorToDfg0 = AffineTransform( ...
            [eye(nMetabolites), zeros(nMetabolites)], ...
            zeros(nMetabolites, 1));
    end
    
    function priorToDfg = createPriorsToDfgTransform(RT, nMetabolites)
        import samply.*
        priorToDfg = AffineTransform( ...
            [eye(nMetabolites), RT * eye(nMetabolites)], ...
            zeros(nMetabolites, 1));
    end
    
    function priorToDrg0 = createPriorsToDrg0Transform( ...
            S, isConstraintRxn, drgCorrection)
        import samply.*
        priorToDfg0 = GibbsFreeEnergiesSpace.createPriorsToDfg0Transform( ...
            size(S, 1));
        dfg0ToDrg0 = AffineTransform(S(:, isConstraintRxn)', ...
            drgCorrection(isConstraintRxn));
        priorToDrg0 = dfg0ToDrg0.applyTransform(priorToDfg0);
    end
    
    function priorToDrg = createPriorsToDrgTransform( ...
            RT, S, isConstraintRxn, drgCorrection)
        import samply.*
        priorToDfg = GibbsFreeEnergiesSpace.createPriorsToDfgTransform( ...
            RT, size(S, 1));
        dfgToDrg = AffineTransform(S(:, isConstraintRxn)', ...
            drgCorrection(isConstraintRxn));
        priorToDrg = dfgToDrg.applyTransform(priorToDfg);
    end
    
    function priorToLogConc = createPriorsToLogConcTransform(nMetabolites)
        import samply.*
        priorToLogConc = AffineTransform( ...
            [zeros(nMetabolites), eye(nMetabolites)], ...
            zeros(nMetabolites, 1));
    end
    
    function [standardMvnToObservablesMvn, varsScaling, varsRotation] = ...
            createMinimalSubspaceForObservables( ...
                priorCov, priorMean, priorsToObservablesTransform, ...
                svdTruncationThreshold)
    
        observablesCov = priorsToObservablesTransform.T * priorCov * ...
            priorsToObservablesTransform.T';
        observablesMean = priorsToObservablesTransform.T * priorMean + ...
            priorsToObservablesTransform.shift;
        
        [standardMvnToObservablesMvn, varsScaling, varsRotation] = ...
            getMinimalMvnSubspace( ...
                observablesCov, observablesMean, svdTruncationThreshold);
    end
end

end