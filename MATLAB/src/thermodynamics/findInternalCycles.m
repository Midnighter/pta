function cyclesEFMs = findInternalCycles(model)
% FINDINTERNALCYCLES Find the internal cycles of the network. These cycles are
% equivalent to EFMs of the network when all exchange fluxes are set to zero.
% This function requires the efmtool package:
%   http://www.csb.ethz.ch/tools/software/efmtool.html
%
%       cycles = findInternalCycles(cobraModel);
%
%   Required:
%       model:      Model in COBRA format representing the network for which
%                   internal cycles must be found.
% 
%   Returns:
%       cyclesEFMs: EFMs of the internal cycles of the network.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    curDir = pwd;
    efmtoolDir = fileparts(which('CalculateFluxModes.m'));
    cd(efmtoolDir);
    
    % efmtool needs a temporary directory.
    [~, ~] = mkdir('tmp');

    % Force the boundary reaction to zero and find all the possible EFMs. By
    % definition, these EFMs will describe all the possible internal cycles.
    % Additionally remove the ATP mainteinance reaction (if present) and all
    % blocked reactions.
    internalModel = removeRxns( ...
        model, [model.rxns(~isInternalReaction(model.S)); 'ATPM'; 'BIOMASS']);
    internalModel = removeRxns(internalModel, findBlockedReaction(internalModel));
    internalModel = tightenModelBounds(internalModel);
    
    % Reverse reactions that are feasible only in the negative direction.
    toReverse = internalModel.ub <= 0;
    internalModel.S(:, toReverse) = -internalModel.S(:, toReverse);
    [internalModel.lb(toReverse), internalModel.ub(toReverse)] = ...
        deal(-internalModel.ub(toReverse), -internalModel.lb(toReverse));
    isReversible = internalModel.lb < 0;
    
    % Find all EFMs in the reduced internal network.
    options = CreateFluxModeOpts('memory', 'sort-out-core');
    result = CalculateFluxModes(full(internalModel.S), isReversible, internalModel.mets, ...
                                internalModel.rxns, options);
                            
    % Remove the temporary directory when done.
    [~, ~] = rmdir('tmp', 's');
    
    % Read result and map EFMs to the original model.
	internalCyclesEFMs = result.efms;
    internalCyclesEFMs(toReverse, :) = -internalCyclesEFMs(toReverse, :);
    [~, cycleRxnIds, internalCycleRxnIds] = intersect(model.rxns, internalModel.rxns);
    cyclesEFMs = zeros(numel(model.rxns), size(internalCyclesEFMs, 2));
    cyclesEFMs(cycleRxnIds, :) = internalCyclesEFMs(internalCycleRxnIds, :);
    
    cd(curDir);
end