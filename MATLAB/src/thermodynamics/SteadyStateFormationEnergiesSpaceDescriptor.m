classdef SteadyStateFormationEnergiesSpaceDescriptor
% STEADYSTATEFORMATIONENERGIESSPACEDESCRIPTOR Descriptors for the space of Gibbs
% formation energies that allow steady state flux distributions.
    
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

properties(SetAccess = private) 
    toOriginal;
    model;

    drgEpsilon;
    fluxEpsilon;
    dfgConfidence;
    maxTestedRxnsForInitialPoints;

    freeEnergySpace;
    
    varsToDrg;
    ellipsoid;
    polytope;
    maxDrg;
    isConstraintReaction;
    
    settings;
    
    maxWorkers;
end

methods
    function self = SteadyStateFormationEnergiesSpaceDescriptor( ...
            model, freeEnergiesSpace, settings, varargin)
        
        % Read options.
        parser = inputParser;
        parser.KeepUnmatched = true;
        addParameter(parser, 'initializationStruct', []);
        parse(parser, varargin{:});
        options = parser.Results;
        
        % Copy optional arguments.
        self.maxDrg = settings.drgMax;
        self.drgEpsilon = settings.drgEpsilon;
        self.fluxEpsilon = settings.fluxEpsilon;
        self.dfgConfidence = freeEnergiesSpace.confidence;
        self.isConstraintReaction = model.isConstraintRxn;
        self.maxTestedRxnsForInitialPoints = ...
            settings.maxTestedRxnsForInitialPoints;
        self.settings = settings;
        self.maxWorkers = settings.maxWorkers;
        self.freeEnergySpace = freeEnergiesSpace;
        
        % Create polytope encoding the given constraints.
        varsToDrg = freeEnergiesSpace.standardMvnToDrgMvnTransform;
        isConstraint = model.isConstraintRxn;
        
        % Create polytope and ellipsoid if they have not been specified.
        if isempty(options.initializationStruct)
            options.initializationStruct = struct();
            
            % Create (open) polytope containing the directionality constraints.
            forwardConstraintIdx = find(model.lb(isConstraint) >= 0);
            backwardConstraintIdx = find(model.ub(isConstraint) <= 0);
            G = [
                varsToDrg.T(forwardConstraintIdx, :);
                -varsToDrg.T(backwardConstraintIdx, :)
            ];
            h = [
                -settings.drgEpsilon * ones(numel(forwardConstraintIdx), 1) - ...
                    varsToDrg.shift(forwardConstraintIdx);
                -settings.drgEpsilon * ones(numel(backwardConstraintIdx), 1) + ...
                    varsToDrg.shift(backwardConstraintIdx);
            ];
            options.initializationStruct.polytope = samply.Polytope([], [], G, h);
            
            % Create ellipsoid corresponding to the
            % (self.dfgConfidence * 100)% confidence interval of the
            % formation energies covariance matrix.
            nDimensions = size(varsToDrg.T, 2);
            options.initializationStruct.ellipsoid = samply.Ellipsoid( ...
                eye(nDimensions), zeros(nDimensions, 1));
        end
        
        % Copy initialization fields to the object.
        self.polytope = options.initializationStruct.polytope;
        self.ellipsoid = options.initializationStruct.ellipsoid;
        
        self.toOriginal = varsToDrg;
        self.model = model;
        self.varsToDrg = varsToDrg;        
    end
    
    function samples = getInitialSamples(self, nSamples)
    % GETINITIALSAMPLES Returns nSamples points that are guaranteed to lie
    % inside the described space. Points are generated by first finding the
    % reaction directions that allow minimal and maximal flux on each reaction.
    % Then, for each direction found, an assignment of formation free energies
    % with maximal distance from the constraints is found.
    %
    %   Requires:
    %       nSamples:   Number of samples to generate.
    %       pdfSampler: Sampler describing the pdf over the space.
    % 
    %   Returns:
    %       samples:    d-by-nSamples vector containing the initial samples.
            
        %% Create flux polytope.
        [~, nReactions] = size(self.model.S);
        
        fprintf("Generating initial points ...\n");
        
        %% Maximize the flux along each reaction.
        % This aims at reaching as many different modes as possible.
        nTestedRxns = min(self.maxTestedRxnsForInitialPoints, nReactions);
        initialDirections = nan(nReactions, nTestedRxns*2);
        isValidDirection = true(1, nTestedRxns*2);
        
        % Prioritize reversible reactions.
        isReversible = self.model.lb < 0 & self.model.ub > 0;
        nReversibleRxns = sum(isReversible);
        if nSamples < 2*nReversibleRxns
            warning(['In order to obtain reliable convergence estimates ' ...
                'it is recommended to run twice as many chains as ' ...
                'the number of reversible reations in the model.']);
        end
        objectives = ...
            [-eye(nReactions, nReactions), eye(nReactions, nReactions)];
        isReversibleObj = [isReversible; isReversible];
        objectives = [ ...
            objectives(:, isReversibleObj), objectives(:, ~isReversibleObj)];
       
        parfor (iRxn = 1:nTestedRxns*2, self.maxWorkers)
            try
                initialDirections(:, iRxn) = PMO('flux', -objectives(:, iRxn), ...
                    self.model, self.freeEnergySpace, self.settings); %#ok<PFBNS>
            catch exception
                isValidDirection(iRxn) = false;
                if iRxn <= nReactions
                    warning("Search failed for reaction %i-.", iRxn);
                else
                    warning("Search failed for reaction %i+.", iRxn-nReactions);
                end
                disp(getReport(exception));
            end
        end
        
        initialDirections = initialDirections(:, isValidDirection);
        initialDirections = sign(initialDirections(self.isConstraintReaction, :));
        assert(all(all(initialDirections ~= 0)), ['Failed to generate ' ...
               'initial points because at least one reaction does not have ' ...
               'a direction']);
        assert(size(initialDirections, 2) ~= 0, ['Failed to generate ' ...
               'initial points because stFBA failed in all tested reaction ' ...
               'directions.']);
        
        % Only take unique directions.
        initialDirections = unique(initialDirections', 'rows')';
        initialDirections = initialDirections(:, 1:min(nSamples, end));
        nInitialPoints = size(initialDirections, 2);
        
        %% For unique modes, find an assignment of free energies of formation.
        % We choose points with maximal distance from the boundaries, as they
        % have lower probability of spending too many iterations in narrow
        % corners.
        initialPoints = nan(size(self.toOriginal.T, 2), nInitialPoints);
        isValidInitialPoint = true(1, nInitialPoints);
        parfor (iInitialPoint = 1:size(initialDirections, 2), self.maxWorkers)
            try
                initialPoints(:, iInitialPoint) = self.findCentralFreeEnergyVars( ...
                    initialDirections(:, iInitialPoint));
            catch exception
                isValidInitialPoint(iInitialPoint) = false;
                warning("Initial point search failed for point %i.", ...
                    iInitialPoint);
                disp(getReport(exception));
            end
        end
        initialPoints = initialPoints(:, isValidInitialPoint);
        nInitialPoints = size(initialPoints, 2);
        assert(nInitialPoints ~= 0, ['Failed to generate initial points ' ...
            'because central formation energy values could not be found.']);
        fprintf(['Initial points generated for %i different direction ' ...
                 'sets.\n'], size(initialPoints, 2));
             
        samples = [ ...
            repmat(initialPoints, 1, floor(nSamples / nInitialPoints)), ...
            initialPoints(:, 1:mod(nSamples, nInitialPoints))];
    end
end

methods (Access = protected)
    function freeEnergyVars = findCentralFreeEnergyVars(self, v)
    % FINDCENTRALDFG Find the vector of free energies of formation (in the
    % optimal parametrization) that have maximal distance from the constraints
    % satisfying the given reaction fluxes.
        
        %% Create constraints on the free energies of reaction based on the
        % flux directions.
        drGmin = -self.maxDrg * ones(size(v));
        drGmax =  self.maxDrg * ones(size(v));
        drGmin(v < 0) =  self.drgEpsilon;
        drGmax(v > 0) = -self.drgEpsilon;
  
        %% Ellipsoid constraint corresponding to the formation energies prior.    
        nVariables = numel(self.freeEnergySpace.lb);
        
        gurobiModel.quadcon(1).Qc = sparse(nVariables + 1, nVariables + 1);
        gurobiModel.quadcon(1).Qc(1:nVariables, 1:nVariables) = eye(nVariables);
        gurobiModel.quadcon(1).q  = zeros(nVariables + 1, 1);
        gurobiModel.quadcon(1).q(end) = 2;
        gurobiModel.quadcon(1).rhs = self.freeEnergySpace.radiusMultiplier ^ 2;
        gurobiModel.quadcon(1).name = 'free_energy_vars_ellipsoid';
        
        gurobiModel.obj = [zeros(nVariables, 1); 1];
        gurobiModel.modelsense = 'max';
        gurobiModel.lb = [self.freeEnergySpace.lb; 0];
        gurobiModel.ub = [self.freeEnergySpace.ub; Inf];
        gurobiModel.vtype = repmat('C', 1, nVariables + 1);
        
        %% Create formation energy constraints based on the flux directions.
        % Free energies are expressed in the optimized parametrization. The
        % additional variable describes the distance form the constraints.
        varsToDrgConstraints = self.varsToDrg.T;
        drgShift = self.varsToDrg.shift;
        
        G = [-varsToDrgConstraints; varsToDrgConstraints];
        rowNorms = vecnorm(G, 2, 2);
        
        gurobiModel.A = [sparse(G) rowNorms];
        gurobiModel.rhs = [
            -drGmin + drgShift;
            drGmax - drgShift];
        gurobiModel.sense = repmat('<', 1, size(G, 1));
        
        %% Solve linear program maximizing the distance from the constraints.
        params.outputflag = 0;
        params.FeasibilityTol = 1e-9;

        result = gurobi(gurobiModel, params);
        if strcmp(result.status, 'OPTIMAL')
            freeEnergyVars = result.x(1:nVariables);
            if result.objval < 1e-6
                warning(['At least one initial point is very close to the ' ...
                    'constraints. This is unusual and generally indicates' ...
                    'problems in the constraints. Expect possible failures ' ...
                    'in the sampling process.']);
            end
            
            % Sanity check ensuring that we don't encountered precision problems.
            drg = varsToDrgConstraints * result.x(1:end-1) + drgShift;
            assert(all(sign(v) ~= sign(drg)), ['The free energies ' ...
            'and estimated by the optimizer are inconsistent with the flux ' ...
            'directions. This is likely a numerical precision issue. ' ...
            'Possible solutions include increasing the precision of the ' ...
            'solver or reducing the gap between M and drgEpsilon.']);
        else
            error('Unable to find initial point. Gurobi result: %s.', ...
                result.status);
        end
    end
end

end