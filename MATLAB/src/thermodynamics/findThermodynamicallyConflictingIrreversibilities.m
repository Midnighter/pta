function findThermodynamicallyConflictingIrreversibilities(model, settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    [~, ~] = mkdir(settings.outputFolder);
    logFileName = fullfile(settings.outputFolder, 'structural_analysis.txt');
    fileID = fopen(logFileName, 'w');
    minFlux = 1e-7;
    
    % Get all the internal EFMs of the network.
    internalEFMs = findInternalCycles(model);
    nInternalEFMs = size(internalEFMs, 2);
    if nInternalEFMs == 0
        fprintf('No reaction with conflicting irreversibilities found.\n');
        fprintf(fileID, ...
            'No reaction with conflicting irreversibilities found.\n');
        fclose(fileID);
        return;
    else
        fprintf('Found %i internal EFMs.\n', nInternalEFMs);
        fprintf(fileID, 'Found %i internal EFMs.\n', nInternalEFMs);
    end
    
    % Check if any of the internal EFMs is forced to happen in the model.
    for e = 1:size(internalEFMs, 2)
        EFM = internalEFMs(:, e);
        rxnIds = find(EFM ~= 0);
        isRequired = true;
        
        for i=1:numel(rxnIds)
            for j=i:numel(rxnIds)
                if i ~= j
                    tmpModel = model;
                    iRxn = rxnIds(i);
                    jRxn = rxnIds(j);
                    
                    % Set the direction of the EFM for the first reaction.
                    if EFM(iRxn) > 0
                        tmpModel.lb(iRxn) = max(tmpModel.lb(iRxn), minFlux);
                    else
                        tmpModel.ub(iRxn) = min(tmpModel.ub(iRxn), -minFlux);
                    end
                    
                    % Set the opposite direction than the EFM for the second
                    % reaction.
                    if EFM(jRxn) < 0
                        tmpModel.lb(jRxn) = max(tmpModel.lb(jRxn), minFlux);
                    else
                        tmpModel.ub(jRxn) = min(tmpModel.ub(jRxn), -minFlux);
                    end
                    
                    % Check whether the model is feasible. If yes, there are
                    % thermodynamically feasible solutions for the set of
                    % reactions involved in the EFM.
                    if any(tmpModel.lb > tmpModel.ub)
                        continue
                    end
                    result = optimizeCbModel(tmpModel);
                    if result.stat == 1
                        isRequired = false;
                        break;
                    end
                end
            end
            
            if ~isRequired
                break;
            end
        end
        
        if isRequired
            fprintf(['Found set of reactions preventing thermodynamically ', ...
                'feasible non-zero solutions: %s. Please review the ' ...
                'reactions and their reversibility.\n'], ...
                strjoin(model.rxns(rxnIds), ', '));
            fprintf(fileID, ['Found set of reactions preventing thermodynamically ', ...
                'feasible non-zero solutions: %s. Please review the ' ...
                'reactions and their reversibility.\n'], ...
                strjoin(model.rxns(rxnIds), ', '));
        end
    end
    
    fclose(fileID);
end