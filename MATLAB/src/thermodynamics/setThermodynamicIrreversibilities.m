function [model, isNewIrreversibility] = setThermodynamicIrreversibilities( ...
    model, freeEnergySpaceSettings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    freeEnergySpace = GibbsFreeEnergiesSpace(model, freeEnergySpaceSettings);
    [lb, ub] = freeEnergySpace.getThermodynamicBounds();
    
    isNewIrreversibility = ...
        (model.lb < 0 & model.ub > 0) & (lb == 0 | ub == 0);
    if any(isNewIrreversibility)
        fprintf('New thermodynamic irreversibilities found:');
        fprintf(' %s', model.rxns{isNewIrreversibility});
        fprintf('\n');
        model.lb(isNewIrreversibility) = max(model.lb(isNewIrreversibility), lb(isNewIrreversibility));
        model.ub(isNewIrreversibility) = min(model.ub(isNewIrreversibility), ub(isNewIrreversibility));
    else
        fprintf('No additional thermodynamic irreversibility found.\n');
    end
end