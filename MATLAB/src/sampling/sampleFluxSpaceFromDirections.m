function result = sampleFluxSpaceFromDirections( ...
    model, directions, isConstrainedRxn, settings, varargin)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    parser = inputParser;
    parser.KeepUnmatched = true;
    addParameter(parser, 'minSamples', 1);
    addParameter(parser, 'resultFunction', []);
    addParameter(parser, 'postprocessFunction', @(x, varargin) x);
    parse(parser, varargin{:});
    options = parser.Results;

    %% Find unique directions sets and mapping to the original samples.
    [uniqueDirs, uniquesToSamples, samplesToUniques] = ...
        unique(directions', 'rows', 'stable');
    uniqueDirs = uniqueDirs';
    nOrthants = numel(uniquesToSamples);
   
    %% Create a flux polytope for each direction set and draw samples from it.
    workerSamples = cell(1, nOrthants);
    workerSamplesIds = cell(1, nOrthants);
    nFluxSamples = size(directions, 2);
    isSamplingSuccessful = false(1, nOrthants);
    
    fprintf('Sampling %i polytopes.\n', nOrthants);
    parfor (iUniqueDirection = 1:nOrthants, ...
            settings.samplerSettings.maxWorkers)
        try            
            workerSamplesIds{iUniqueDirection} = find(samplesToUniques == ...
                samplesToUniques(uniquesToSamples(iUniqueDirection)));
            
            localSettings = settings;
            localSettings.samplerSettings.maxWorkers = 0;
            localSettings.samplerSettings.nSamples = ...
                max(numel(workerSamplesIds{iUniqueDirection}), options.minSamples);
            localSettings.samplerSettings.nRecordedSteps = ceil( ...
                localSettings.samplerSettings.nSamples / ...
                localSettings.samplerSettings.nChains);
            sampleModel = model;

            directionSample = uniqueDirs(:, iUniqueDirection);
            constrainedLb = sampleModel.lb(isConstrainedRxn);
            constrainedUb = sampleModel.ub(isConstrainedRxn);
            constrainedLb(directionSample > 0) = ...
                max(constrainedLb(directionSample > 0), 0);
            constrainedUb(directionSample < 0) = ...
                min(constrainedUb(directionSample < 0), 0);
            sampleModel.lb(isConstrainedRxn) = constrainedLb;
            sampleModel.ub(isConstrainedRxn) = constrainedUb;
            
            localSettings.samplerSettings.workerId = iUniqueDirection;
            [~, workerResult] = ...
                sampleFluxSpaceUniformWithOutputCapture( ...
                sampleModel, localSettings);
            
            workerSamples{iUniqueDirection} = options.postprocessFunction(...
                workerResult.fluxes.samples, workerSamplesIds{iUniqueDirection}, model);
            isSamplingSuccessful(iUniqueDirection) = true;
            fprintf('Sampling of polytope %i completed.\n', iUniqueDirection);
        catch exception
            warning('Failed to sample polytope %i\n', iUniqueDirection);
            disp(getReport(exception));
        end
    end
    
    if ~all(isSamplingSuccessful)
        warning('Sampling failed for %i out of %i polytopes.', ...
            sum(isSamplingSuccessful ~= true), nOrthants);
    else
        fprintf('Sampling successful for all directions.\n');
    end
    
    if isempty(options.resultFunction)
        nDimensions = numel(model.lb);
        nSteps = size(workerSamples{1}, 3);
        result = nan(nDimensions, nFluxSamples, nSteps);
        for iUniqueDirection = 1:nOrthants
            if isSamplingSuccessful(iUniqueDirection)
                result(:, workerSamplesIds{iUniqueDirection}, :) = ...
                    workerSamples{iUniqueDirection};
            end
        end
    else
        result = options.resultFunction(model, workerSamples, workerSamplesIds, isSamplingSuccessful, nFluxSamples);
    end
end

function [consoleOutput, result] = ...
    sampleFluxSpaceUniformWithOutputCapture(model, settings)
    [consoleOutput, result] = evalc( ...
                'sampleFluxSpaceUniform(model, settings);');
end