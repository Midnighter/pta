function result = sampleFreeEnergies(model, freeEnergySpace, settings)
% SAMPLEFREEENERGIES Sample the space of the Gibbs free energies of a
% reaction netowrk.
%
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    %% Initialize settings and load cache.
    settings.samplerSettings.truncationMultiplier = freeEnergySpace.radiusMultiplier;
    samplerSettings = settings.samplerSettings;
    convergenceSettings = settings.convergenceDiagnostic;
    
    cache = loadOrGenerateCache(model, freeEnergySpace, samplerSettings, ...
        settings.cache, settings.adaptiveDirectionsDistribution);
    
    % Execute the line below at this point to generate a MAT file with the data
    % required for the benchmark executable.
    % saveBenchmarkParamsToMat(freeEnergySpace, model, cache.initialPoints, samplerSettings, cache);
    
    %% Iteratively improve the distributions of Hit-and-Run directions.
    improveDirectionsDistribution(cache, freeEnergySpace, model, settings);
    
    %% Run the actual sampling procedure.
    sampler = makeSamplingFunction( ...
        freeEnergySpace, model, cache, samplerSettings);
    samplerResult = sampler();

    result = makeResultObject(samplerResult, freeEnergySpace, model);
    chainGroups = makeChainGroups( ...
        samplerResult, result, freeEnergySpace, model);
    samply.analyzeChainsConvergence(chainGroups, convergenceSettings);
end 

function cache = loadOrGenerateCache(model, freeEnergySpace, ...
    samplerSettings, cacheSettings, directionsDistributionSettings)

    %% Check whether the specified cachefile exists.
    if exist(cacheSettings.file, 'file')
        hasCache = true;
        load(cacheSettings.file, 'cache');
        fprintf('Cache loaded.\n');
    else
        hasCache = false;
    end
    
    %% Generate the initial point if necessary.
    if ~hasCache || ~cacheSettings.useInitialPoints
        descriptor = SteadyStateFormationEnergiesSpaceDescriptor( ...
            model, freeEnergySpace, samplerSettings);
        initialPoints = freeEnergySpace.standardMvnToObservablesMvnTransform ...
            .transform(descriptor.getInitialSamples(samplerSettings.nChains));
    else
        initialPoints = cache.initialPoints;
    end
    
    %% Create the directions distribution if necessary.
    if ~hasCache || ~cacheSettings.useDirectionsDistribution
        directionsDistribution = AdaptiveDirectionSelector( ...
            model, freeEnergySpace, directionsDistributionSettings);
    else
        directionsDistribution = cache.directionsDistribution;
    end
    
    %% If there is any new data, store it.
    if ~hasCache || ~cacheSettings.useInitialPoints || ...
            ~cacheSettings.useDirectionsDistribution
        cache = SampleFreeEnergiesCache(initialPoints, directionsDistribution);
        if strlength(cacheSettings.file) > 0
            save(cacheSettings.file, 'cache');
        end
    end
end

function improveDirectionsDistribution(cache, freeEnergySpace, model, settings)
    
    directionsDistributionSettings = settings.adaptiveDirectionsDistribution;
    if ~directionsDistributionSettings.enabled
        return;
    end
    
    nIterationsSteps = directionsDistributionSettings.nStepsPerIteration * ...
        directionsDistributionSettings.stepsMultiplier .^ ...
        (0:directionsDistributionSettings.nIterations-1);
    
    for iIteration = 1:directionsDistributionSettings.nIterations
        % 1) Run the simulation for a short period.
        iterationSettings = settings.samplerSettings;
        iterationSettings.nSteps = nIterationsSteps(iIteration);
        iterationSettings.nDirectionSamples = 2; % TODO fix the mex file such
        % 0 is accepted.
      
        sampler = makeSamplingFunction( ...
            freeEnergySpace, model, cache, iterationSettings);
        samplerResult = sampler();
        
        % 2) Use the resulting chains to improve the distribution.
        varsChains = freeEnergySpace.standardMvnToObservablesMvnTransform ...
            .inverseTransformChains(samplerResult.chains);
        drgChains = freeEnergySpace ...
            .observablesToDrgTransform.transformChains(samplerResult.chains);
        cache.directionsDistribution.update(varsChains, drgChains);
    end
    
    if strlength(settings.cache.file) > 0
        save(settings.cache.file, 'cache');
    end
end

function chainGroups = makeChainGroups( ...
    samplerResult, result, freeEnergySpace, model)
    minimalChains = freeEnergySpace.standardMvnToObservablesMvnTransform ...
        .inverseTransformChains(samplerResult.chains);
    minimalParameterNames = samply.getAffectedVariables( ...
        freeEnergySpace.varsRotation, ...
        [strcat(model.rxns(model.isConstraintRxn), '_drg'); ...
        model.mets; strcat(model.rxns(model.isConstraintRxn), '_drg0')], 20);
    
    chainGroups = { ...
        struct( ...
            'name', 'Minimal subspace variables', ...
            'parameterNames', { minimalParameterNames }, ...
            'chains', minimalChains), ...
        struct( ...
            'name', 'Reaction energies', ...
            'parameterNames', { model.rxns(model.isConstraintRxn) }, ...
            'chains', result.drg.chains), ...
    };
end

function samplingFunction = makeSamplingFunction( ...
    freeEnergySpace, model, cache, samplerSettings)

    if ~isfinite(samplerSettings.nWarmupSteps)
        nWarmupSteps = ceil(samplerSettings.nSteps / 2);
    else
        nWarmupSteps = samplerSettings.nWarmupSteps;
    end
    samplerSettings.stepsThinningDirections = min(samplerSettings.nSteps, max(1, ...
        floor((samplerSettings.nSteps - nWarmupSteps) / ...
        samplerSettings.nDirectionSamples * samplerSettings.nChains)));

    mexHost  = mexhost;
    isConstraint = freeEnergySpace.isConstraintReaction;
    sampler = @(initialPoints, settings) feval(mexHost, ...
        'sample_td_space_mex', ...
        freeEnergySpace.standardMvnToObservablesMvnTransform.T, ...
        freeEnergySpace.standardMvnToObservablesMvnTransform.shift, ...
        full(model.S), model.lb, model.ub, int64(find(isConstraint)-1), ...
        initialPoints, settings, ...
        freeEnergySpace.observablesToDrgTransform.T, ...
        freeEnergySpace.observablesToDrgTransform.shift, ...
        cache.directionsDistribution.directionsTransform);
    samplingFunction = @() runSampler( ...
        sampler, cache.initialPoints, samplerSettings, ...
        @(partitionSamples, settings) resultFunction( ...
            partitionSamples, settings, model));
end

function result = resultFunction(partitionSamples, settings, tdModel)

    % Reject partitions that ended in an invalid state.
    isValidPartition = false(1, numel(partitionSamples));
    for iPartition = 1:numel(partitionSamples)
        partition = partitionSamples{iPartition};
        directions = -sign(partition.finalStates);
        if all(isSteadyStateFluxDirection(directions, tdModel.S, tdModel.lb, ...
                tdModel.ub, tdModel.isConstraintRxn))
            isValidPartition(iPartition) = true;
        else
            warning('Samples from partition %i are invalid.', iPartition);
        end
    end

    allSamples = [partitionSamples{isValidPartition}];
    result.initialStates = cat(2, allSamples.initialStates);
    result.chains = cat(2, allSamples.chains);
    result.samples = sampleFromChains(result.chains, settings.nSamples);
    
    maps = cellfun(@(p) containers.Map( ...
        num2cell(char(p.directions), 1), ...
        num2cell(p.counts)), partitionSamples, 'UniformOutput', false);
    result.directionsCounts = mergeMaps(maps);
end

function map = mergeMaps(maps)
    nMaps = numel(maps);
    if nMaps == 1
        map = maps{1};
    else
        nMaps1 = ceil(nMaps / 2);
        map1 = mergeMaps(maps(1:nMaps1));
        map2 = mergeMaps(maps((nMaps1+1):end));
        
        directions2 = keys(map2);
        values2 = values(map2);
        isInMap1 = isKey(map1, directions2);
        for iDir = find(isInMap1)
            map1(directions2{iDir}) = ...
                map1(directions2{iDir}) + values2{iDir};
        end
        
        map = containers.Map( ...
            [keys(map1), directions2(~isInMap1)], ...
            [values(map1), values2(~isInMap1)]);
    end
end

function saveBenchmarkParamsToMat( ...
    freeEnergySpace, model, initialPoints, settings, cache)
    
    file = '../data/benchmark/td_benchmark_data.mat';
    [~, ~] = mkdir('../data/benchmark');
    settings.workerId = 1;
    
    mvnToObsT = freeEnergySpace.standardMvnToObservablesMvnTransform.T;
    mvnToObsShift = freeEnergySpace.standardMvnToObservablesMvnTransform.shift;
    S = full(model.S);
    lb = model.lb;
    ub = model.ub;
    constrainedRxnsIds = int64(find(freeEnergySpace.isConstraintReaction)-1);
    obsToDrgT = freeEnergySpace.observablesToDrgTransform.T;
    obsToDrgShift = freeEnergySpace.observablesToDrgTransform.shift;
    dirT = cache.directionsDistribution.directionsTransform;
    
    save(file, 'mvnToObsT', 'mvnToObsShift', 'S', 'lb', 'ub', ...
        'constrainedRxnsIds', 'initialPoints', 'settings', 'obsToDrgT', ...
        'obsToDrgShift', 'dirT');
end

function result = makeResultObject(samplerResult, freeEnergySpace, model)
    result.drg.initialPoints = freeEnergySpace ...
        .observablesToDrgTransform.transform(samplerResult.initialStates);
    result.drg.chains = single(freeEnergySpace ...
        .observablesToDrgTransform.transformChains(single(samplerResult.chains)));
    result.drg.samples = freeEnergySpace ...
        .observablesToDrgTransform.transformChains(samplerResult.samples);
    
    numSigns = sum(model.lb(model.isConstraintRxn) < 0 & ...
        model.ub(model.isConstraintRxn) > 0);
    result.directions.signs = bin2signs( ...
        keys(samplerResult.directionsCounts), numSigns);
    result.directions.signs = result.directions.signs(1:numSigns, :);
    result.directions.counts = cell2mat(values(samplerResult.directionsCounts));
end

function signs = bin2signs(binarySigns, numSigns)
    signs = zeros(numSigns, numel(binarySigns), 'int8');
    for i = 1:numel(binarySigns)
        s = int8(str2num(reshape(flip( ...
            dec2bin(binarySigns{i}, 8), 2)', 1, [])')) * 2 - 1; %#ok<ST2NM>
        signs(:, i) = s(1:numSigns);
    end
end