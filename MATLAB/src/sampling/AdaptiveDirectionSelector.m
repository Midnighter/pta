classdef AdaptiveDirectionSelector < handle
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

properties(SetAccess = private)
    drgToVars;
    freeEnergySpace;
    model;
    settings;
    directionsTransform;
    fluxCouplings;
    
    varWeights;
    drgWeights;
    
    nDrg;
    nVariables;
    nUpdates;
end
    
methods
    function setVar(self, idx, val)
        self.varWeights(idx) = val;
    end
    
    function self = AdaptiveDirectionSelector(model, freeEnergySpace, settings)
    % ADAPTIVEDIRECTIONSELECTOR Construct a new instance of the
    % AdaptiveDirectionSelector class and computes an initial guess for the 
    % direcitons distribution.
    
        drgToVarsTransform =  ...
            freeEnergySpace.standardMvnToDrgMvnTransform.getInverse();
        drgToVarsTransform.shift = zeros(size(drgToVarsTransform.shift));
        self.drgToVars = @(v) drgToVarsTransform.transform(v);
        
        self.freeEnergySpace = freeEnergySpace;
        self.model = model;
        self.settings = settings;
        self.nVariables = numel(self.freeEnergySpace.lb);
        self.nDrg = sum(model.isConstraintRxn);
        self.nUpdates = 0;
        self.fluxCouplings = self.createFluxCouplingsTable();
        self.settings.pngResolutionCm = [15, 10];
        
        self.varWeights = ones(self.nVariables, 1);
        self.drgWeights = ones(self.nDrg, 1);
        
        if settings.enabled
            self.guessTransform();
        else
            self.directionsTransform = eye(self.nVariables);
        end
    end
    
    function manualUpdate(self, varargin)
        crxns = self.model.rxns(self.model.isConstraintRxn);
        updates = ones(size(crxns));
        doAxisUpdate = false(size(crxns));
        doDiagUpdate = false(size(crxns));
        iInput = 1;
        
        % Parse the inputs and create scale factors for each free energy.
        while (iInput < nargin)
            tokens = split(varargin{iInput});
            rxnName = tokens{1};
            rxnId = find(strcmp(crxns, rxnName));
            if numel(rxnId) ~= 1
                error('Reaction %s not found.', varargin{iInput});
            end
            updates(rxnId) = varargin{iInput+  1};
            if numel(tokens) == 1
                doAxisUpdate(rxnId) = true;
            elseif numel(tokens) == 2 && strcmp(tokens{2}, 'j')
                doDiagUpdate(rxnId) = true;
                doAxisUpdate(rxnId) = false;
            elseif numel(tokens) == 2 && strcmp(tokens{2}, 'ja')
                doDiagUpdate(rxnId) = true;
                doAxisUpdate(rxnId) = true;
            end
            iInput = iInput + 2;
        end
        
        % Create control points from the given scale factors.
        for iRxn = 1:self.nDrg
            scaleFactor = updates(iRxn);
            if doAxisUpdate(iRxn)
                rxnIds = find(self.fluxCouplings(:, iRxn) ~= 0);
                for iSubsetRxn = 1:numel(rxnIds)
                    self.drgWeights(rxnIds(iSubsetRxn)) = scaleFactor;
                end
            end
        end
        
        self.updateTransform();
    end
    
    function update(self, varsChains, drgChains)
    % UPDATE Update the directions distribution according to how well each
    % parameter was explored in a simulation.      
        if ~self.settings.enabled
            return;
        end
        self.nUpdates = self.nUpdates + 1;
                
        %% Compute splitR for all the parameter chains.
        varsRhat = nan(self.nVariables, 1);
        drgRhat = nan(self.nDrg, 1);
        varsChains = samply.splitChains(varsChains);
        drgChains = samply.splitChains(drgChains);
        
        for iVar = 1:self.nVariables
            varsRhat(iVar) = samply.computeRhat( ...
                squeeze(varsChains(iVar, :, :)));
        end
        for iRxn = 1:self.nDrg
            drgRhat(iRxn) = samply.computeRhat(squeeze(drgChains(iRxn, :, :)));
        end
        if ~isempty(self.settings.outputFolder)
            self.plotRhatHistogram(varsRhat, 'Minimal variables');
            self.plotRhatHistogram(drgRhat, 'Reaction energies');
            self.printDrgStats(drgChains);
        end
        
        %% Compute weight scaling factors.
        [varsDeviations, drgDeviations] = self.computeNormalizedDeviations( ...
            varsRhat, drgRhat);
        
        self.updateVarWeights( ...
            varsDeviations, self.settings.updateScaleMultiplier);
        self.updateDrgWeights(...
            drgDeviations, self.settings.updateScaleMultiplier);
        
        self.updateTransform();
    end
    
    function printDrgStats(self, drgChains)
        
        nameSuffix = num2str(self.nUpdates);
        
        %% Split-R of the free energy chains.
        drgSplitRFile = fopen(fullfile(self.settings.outputFolder, ...
            ['drg_split_R_', nameSuffix, '.txt']), 'w');
        names = self.model.rxns(self.model.isConstraintRxn);
        [nParameters, nChains, ~] = size(drgChains);
        
        splitR = nan(1, nParameters);
        for iParameter = 1:nParameters
            parameterChains = squeeze(drgChains(iParameter, :, :));
            splitR(iParameter) = samply.computeRhat(parameterChains);
        end

        [sortedValues, sortedIds] = sort(splitR, 'descend');
        for iElement = 1:numel(sortedIds)
            fprintf('    %10f | %s\n', sortedValues(iElement), ...
                names{sortedIds(iElement)});
            fprintf(drgSplitRFile, '    %10f | %s\n', sortedValues(iElement), ...
                names{sortedIds(iElement)});
        end
        fclose(drgSplitRFile);
        
        %% Direction mixing score
        mixingScoreFile = fopen(fullfile(self.settings.outputFolder, ...
            ['drg_mixing_', nameSuffix, '.txt']), 'w');
        isReversible = self.model.lb < 0 & self.model.ub > 0;
        drgChains = drgChains(isReversible(self.model.isConstraintRxn), :, :);
        nReversible = sum(isReversible & self.model.isConstraintRxn);
        
        numSignChanges = nan(nReversible, 1);
        symmetryScore = nan(nReversible, 1);
        for iRxn = 1:nReversible
            rxnChains = squeeze(drgChains(iRxn, :, :));
            numSignChanges(iRxn) = sum(sum( ...
                sign(rxnChains(:, 1:end-1)) ~= sign(rxnChains(:, 2:end))));
            symmetryScore(iRxn) = 2 / numel(rxnChains) * min( ...
                sum(sum(rxnChains > 0)), sum(sum(rxnChains < 0)));
            
        end
        numSignChanges = numSignChanges / nChains;
        mixingScore = numSignChanges ./ symmetryScore;
        crxns = self.model.rxns(self.model.isConstraintRxn & isReversible);
        
        [~, sortedIds] = sort(mixingScore);
        for iEntry = 1:min(100, nReversible)
            iRxn = sortedIds(iEntry);
            fprintf('  %10f (%7f / %10f) | %s\n', mixingScore(iRxn), ...
                numSignChanges(iRxn), symmetryScore(iRxn), crxns{iRxn});
            fprintf(mixingScoreFile, '  %10f (%7f / %10f) | %s\n', ...
                mixingScore(iRxn), numSignChanges(iRxn), ...
                symmetryScore(iRxn), crxns{iRxn});
        end
        fclose(mixingScoreFile);
    end
    
        
    function printCouplings(self)
        [~, uniqueIds] = unique(abs(self.fluxCouplings), 'rows');
        uniqueSets = self.fluxCouplings(uniqueIds, :);
        crxns = self.model.rxns(self.model.isConstraintRxn);
        crev = self.model.lb(self.model.isConstraintRxn) < 0 & ...
            self.model.ub(self.model.isConstraintRxn) > 0;
        
        for iSet = 1:size(uniqueSets, 1)
            rxnIds = find(uniqueSets(iSet, :));
            if numel(rxnIds) <= 1
               continue; 
            end
            if crev(rxnIds(1))
                fprintf('Reversible: ');
            end
            for rxnId = rxnIds
                fprintf('%s ', crxns{rxnId});
            end
            fprintf('\n');
        end
    end
end

methods(Access = private)
    function plotRhatHistogram(self, Rhat, figureTitle)
        [~, ~] = mkdir(self.settings.outputFolder);
        RhatFigure = figure();
        clf;
        set(RhatFigure, 'Name', [figureTitle, ' - R-hat']);
        set(RhatFigure, 'NumberTitle',' off');
        histogram(Rhat, 50);
        xlabel('R-hat');
        ylabel('Parameters count');
        title('Distribution of Rhat values over parameters');

        nameSuffix = num2str(self.nUpdates);
        fileNamePrefix = fullfile(self.settings.outputFolder, ...
            [strrep(figureTitle, ' ', '_'), '_Rhat_', nameSuffix]);
        set(RhatFigure,'PaperUnits', 'centimeters', 'PaperPosition', ...
            [0 0 self.settings.pngResolutionCm]);
        saveas(RhatFigure, strcat(fileNamePrefix, '.png'));
        close(RhatFigure);
    end
    
    function controlPoints = getAllVarsPairsControlPoints(self)
        allPairsIds = nchoosek(1:self.nVariables, 2);
        nControlPoints = size(allPairsIds, 1);
        controlPoints = zeros(nControlPoints, self.nVariables);
        
        value = 1 / norm([1 1]);
        linearIds = sub2ind(size(controlPoints), ...
            [1:nControlPoints, 1:nControlPoints], ...
            [allPairsIds(:, 1); allPairsIds(:, 2)]');
        controlPoints(linearIds) = value;
    end
    
    function [varsDeviations, drgDeviations] = computeNormalizedDeviations( ...
            ~, varsRhat, drgRhat)
        
        % Assume Rhat values should (ideally) be normally distributed.
        varsMean = mean(varsRhat);
        varsStd = std(varsRhat);
        drgMean = mean(drgRhat);
        drgStd = std(drgRhat);
        
        % Compute normalized deviations from the mean over all the parameters.
        varsDeviations = (varsRhat - varsMean) / varsStd;
        drgDeviations = (drgRhat - drgMean) / drgStd;
        maxDeviation = max([varsDeviations; drgDeviations]);
        varsDeviations = varsDeviations / maxDeviation;
        drgDeviations = drgDeviations / maxDeviation;
    end
    
    function updateVarWeights(self, varsDeviations, updateScaleMultiplier)
    
        % Prevent penalization of variables with negative deviation and rescale
        % deviations to the desired multiplier.
        maxDeviation = max(varsDeviations);
        varsDeviations(varsDeviations < 0) = 0;
        varsDeviations = varsDeviations .* ...
            (varsDeviations / maxDeviation).^2 * updateScaleMultiplier + 1;
    
        self.varWeights = self.varWeights .* varsDeviations;
    end
    
    function updateDrgWeights(self, drgDeviations, updateScaleMultiplier)
    % GETDRGSCALECONTRIBUTION Get the contribution of the reaction energies to the
    % scaling factor of the exploration weights.
    
        % Prevent penalization of variables with negative deviation and rescale
        % deviations to the desired multiplier.
        maxDeviation = max(drgDeviations);
        drgDeviations(drgDeviations < 0) = 0;
        drgDeviations = drgDeviations .* ...
            (drgDeviations / maxDeviation).^2 * updateScaleMultiplier + 1;
        
        self.drgWeights = self.drgWeights .* drgDeviations;
    end
    
    function guessTransform(self)
    % GUESSTRANSFORM Get an initial exploration transform estimated with
    % heuristics on the model structure.
    
        %% Find all the enzyme subsets (for constrained reactions only).
        isConstraintRxn = self.model.isConstraintRxn;
        constrainedRxnLb = self.model.lb(isConstraintRxn);
        constrainedRxnUb = self.model.ub(isConstraintRxn);
        isReversible = constrainedRxnLb < 0 & constrainedRxnUb > 0;
        
        %% Compute the exploration weight for each reaction.
        % The weight depends on reversibility and participation to enzyme
        % subsets. Reaction energy weights are then trasnformed to weights
        % in the minimal variables.
        for iRxn = 1:self.nDrg
            nRxnsInEnzymeSubset = sum(abs(self.fluxCouplings(:, iRxn)));
            
            if nRxnsInEnzymeSubset > 1 && isReversible(iRxn)
                scaleOffset = 1;
            elseif nRxnsInEnzymeSubset > 1 && ~isReversible(iRxn)
                scaleOffset = 0.5;
            elseif isReversible(iRxn)
                scaleOffset = 0.5;
            else
                scaleOffset = 0;
            end
            
            self.drgWeights(iRxn) = 1 + ...
                scaleOffset * self.settings.guessScaleMultiplier;
        end
        self.updateTransform();
    end
    
    function couplings = createFluxCouplingsTable(self)
        isConstraintRxn = self.model.isConstraintRxn;
        couplings = findEnzymeSubsets(self.model);
        couplings = couplings(isConstraintRxn, isConstraintRxn);
    end
   
    function updateTransform(self)
        % Convert free energy weights to control points.
        drgControlPoints = nan(self.nDrg, self.nVariables);
        for iRxn = 1:self.nDrg
            drgScaleAxis = self.fluxCouplings(:, iRxn);
            varsScaleAxis = self.drgToVars(drgScaleAxis);
            varsScaleAxis = varsScaleAxis / norm(varsScaleAxis);
            drgControlPoints(iRxn, :) = varsScaleAxis * self.drgWeights(iRxn);
        end
        
        % Convert variable weights to control points.
        varControlPoints = nan(self.nVariables, self.nVariables);
        for iVar = 1:self.nVariables
            varsScaleAxis = zeros(1, self.nVariables);
            varsScaleAxis(iVar) = self.varWeights(iVar);
            varControlPoints(iVar, :) = varsScaleAxis;
        end
        
        % Assemble control points and compute estimate transform.
        controlPoints = [drgControlPoints; varControlPoints];
        self.directionsTransform = self.getTransformFromControlPoints( ...
            controlPoints);
    end
    
    function transform = getTransformFromControlPoints(self, points)   
        % Mirror each control point to guarantee symmetry. Add control points
        % for each pair of axis as a regularization trick.
        points = [
            points;
            self.getAllVarsPairsControlPoints();
            -points;
            -self.getAllVarsPairsControlPoints();
        ];
    
        % The transform is obtained by normalizing the eiganvalues of the
        % coavariance matrix of the control points.
        covariance = cov(points);
        [V, D] = eig(covariance);
        d = D / min(diag(D));
        transform = V * d * V';
        
        fprintf('Transform found. Min axis: %.3f. Max axis: %.3f\n', ...
            min(diag(d)), max(diag(d)));
    end
end

end

