function result = runSampler( ...
    singleThreadSampler, initialPoints, settings, mergeFunction)
% RUNSAMPLERMULTITHREAD Helper function for running a sampling method over
% multiple workers.

% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.
    
    %% Fill missing settings.
    settings = fillAndValidateSettings(settings);
    [~, ~] = mkdir(settings.logsDir);
    
    % Determine the number of processe to use.
    if settings.maxWorkers > 0
        parpool = gcp();
        nProcesses = max(min(parpool.NumWorkers, settings.maxWorkers), 1);
    else
        nProcesses = 1;
    end
    
    %% Partition the chains for the workers.
    partitionSize = ceil(settings.nChains / nProcesses);
    partitionSamples = cell(nProcesses, 1);
    for iWorker = 1:nProcesses
        partition = [1 + (iWorker - 1) * partitionSize, ...
                     min(settings.nChains, iWorker * partitionSize)];
        partitionSamples{iWorker} = initialPoints(:, partition(1):partition(2));
    end

    %% Run the sampling function in parallel on the available workers.
    isSuccessful = false(1, nProcesses);
    parfor (iWorker = 1:nProcesses, settings.maxWorkers)
        partitionSettings = settings;
        partitionSettings.nChains = size(partitionSamples{iWorker}, 2);
        if ~(isfield(partitionSettings, 'workerId') ...
                && partitionSettings.workerId > 0)
            partitionSettings.workerId = iWorker;
        end
        
        try
            partitionSamples{iWorker} = singleThreadSampler( ...
                partitionSamples{iWorker}, partitionSettings); %#ok<PFBNS>
            isSuccessful(iWorker) = true;
        catch exception
            warning('Sampling failed for worker %i.', iWorker);
            disp(getReport(exception));
        end
    end
    
    %% Collect data in a single result object.
    if (nargin <= 3)
        allSamples = [partitionSamples{isSuccessful}];
        result.initialStates = cat(2, allSamples.initialStates);
        result.chains = cat(2, allSamples.chains);
        result.samples = sampleFromChains(result.chains, settings.nSamples);
    else
        result = mergeFunction(partitionSamples(isSuccessful), settings);
    end
end

function settings = fillAndValidateSettings(settings)
    % By default, skip the first half of the chains.
    if ~isfinite(settings.nWarmupSteps)
        settings.nWarmupSteps = ceil(settings.nSteps / 2);
    end
    
    % By default, return all the recorded steps.
    if ~isfinite(settings.nSamples)
        settings.nSamples = settings.nChains * settings.nRecordedSteps;
    end
    
    % Make the chains longer if too many samples have been requested.
    if settings.nWarmupSteps + settings.nRecordedSteps > settings.nSteps
        settings.nSteps = settings.nWarmupSteps + settings.nRecordedSteps;
    end
    
    settings.stepsThinning = min(settings.nSteps, max(1, ...
        floor((settings.nSteps - settings.nWarmupSteps) / ...
        settings.nRecordedSteps)));
    
    assert(settings.nWarmupSteps + settings.nRecordedSteps <= settings.nSteps);
    assert(settings.nSamples <= settings.nChains * settings.nRecordedSteps);
end

