function [logConc, drg0] = sampleLogConcAndDrg0FromDrg(drg, model, settings)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    [nDrg, nSamples] = size(drg);
    nMetabolites = size(model.S, 1);
   
    settings.sampleConcentrations = true;
    freeEnergySpace = GibbsFreeEnergiesSpace(model, settings);
   
    allVarsMean = freeEnergySpace.standardMvnToObservablesMvnTransform.shift;
    allVarsCov = freeEnergySpace.standardMvnToObservablesMvnTransform.T * ...
        freeEnergySpace.standardMvnToObservablesMvnTransform.T';
   
    % Prepare indices of the different variable types.
    drgRange = 1:nDrg;
    logConcRange = nDrg+1:nDrg+nMetabolites;
    drg0Range = nDrg+nMetabolites+1:nMetabolites+nDrg*2;
    unknownsRange = [logConcRange, drg0Range];
       
    % Compute the covariance of the unknown variables knowing that reaciton
    % energies are fixed.
    % Adapted from: https://en.wikipedia.org/wiki/Schur_complement
    B = allVarsCov(drgRange, unknownsRange);
    A = allVarsCov(drgRange, drgRange);
    C = allVarsCov(unknownsRange, unknownsRange);
    unknownsCov = C - B' * pinv(A) * B;
    stdMvnToUnknownsTransform = getMinimalMvnSubspace( ...
        unknownsCov, zeros(size(unknownsCov, 1), 1), ...
        settings.minDimensionEigenvalue);
    nStdDimensions = size(stdMvnToUnknownsTransform.T, 2);
    
    % Compute the mean of the unknown variables conditioned on the
    % given reaction free energies.
    % Adapted from: https://en.wikipedia.org/wiki/Schur_complement
    unknownsMeans = allVarsMean(unknownsRange) + ...
        B' * pinv(A) * (drg - allVarsMean(drgRange));
    
    % Sample concentration and DrG0 from minimal subspace of the resulting
    % MVN. This is necessary because unknownsCov may contain small imaginary
    % or negative eigenvalues because of numerical issues ocmputing the
    % Schur's complement of a singular covariance matrix.
    sample = unknownsMeans + stdMvnToUnknownsTransform.transform( ...
        mvnrnd(zeros(1, nStdDimensions), eye(nStdDimensions), nSamples)');
    logConc = sample(1:nMetabolites, :);
    drg0 = sample(nMetabolites+1:end, :);
end

