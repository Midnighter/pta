function [samples, linearSampleIds] = sampleFromChains(chains, nSamples)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    nAvailableSamples = numel(chains(1, :, :));
    assert(nAvailableSamples >= nSamples, ...
        'Requested more samples than contained in the chains.');
    
    linearSampleIds = datasample(1:nAvailableSamples, nSamples, ...
        'Replace', false);
    samples = chains(:, linearSampleIds);
end