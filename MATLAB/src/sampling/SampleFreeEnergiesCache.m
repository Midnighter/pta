classdef SampleFreeEnergiesCache < handle
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information..
    
properties
    initialPoints;
    directionsDistribution;
end
    
methods
    function self = SampleFreeEnergiesCache(initialPoints, directionsDistribution)
    % SAMPLEFREEENERGIESCACHE Construct a new instance of the
    % SampleFreeEnergiesCache class.
        self.initialPoints = initialPoints;
        self.directionsDistribution = directionsDistribution;
    end
end

end

