function [samples, orthants, weights] = sampleFluxSpaceWithDirectionsPrior( ...
    model, directionsPrior, rxnsWithPrior, settings, varargin)
% Copyright (c) 2020 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the GNU General Public License v3.
% See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    if nargin <= 4
        varargin = {};
    end

    [~, isConstrainedRxn] = intersect(model.rxns, rxnsWithPrior, 'stable');
    nOrthants = numel(directionsPrior.counts);
    nSampledOrthants = min(nOrthants, settings.nOrthants);
    
    % Sample directions according to the specified sampling strategy.
    switch settings.method
        case 'Direct'
            nSamplesMultiplier = settings.nApproxSamples / nSampledOrthants;
            directionIds = datasample(1:nOrthants, nSampledOrthants, 2, ...
                'Weights', directionsPrior.counts);
            orthants = directionsPrior.signs(:, directionIds);
            weights = directionsPrior.signs(:, directionIds);
            directionIds = repelem(directionIds, nSamplesMultiplier);
            directions = directionsPrior.signs(:, directionIds);
        case 'TwoStages'
            orthantWeights = double(directionsPrior.counts);
            originalWeights = orthantWeights;
            expectedNSamples = getExpectedSamples(orthantWeights, ...
                nSampledOrthants);
            orthantWeights = orthantWeights * ...
                (settings.nApproxSamples / expectedNSamples);      
            orthantWeights = floor(orthantWeights) + ...
                (rand(size(orthantWeights)) < ...
                (orthantWeights - floor(orthantWeights)));
            
            toRemove = orthantWeights == 0;
            orthantSigns = directionsPrior.signs(:, ~toRemove);
            orthantWeights = orthantWeights(~toRemove);
            originalWeights = originalWeights(~toRemove);
            nOrthants = numel(orthantWeights);
            nSampledOrthants = min(nOrthants, settings.nOrthants);
            directionIds = datasample(1:nOrthants, nSampledOrthants, 2, ...
                'Weights', orthantWeights, 'Replace', false);
            
            orthants = orthantSigns(:, directionIds);
            weights = originalWeights(:, directionIds);
            
            directionIds = repelem(directionIds, orthantWeights(directionIds));
            directions = orthantSigns(:, directionIds);
        % case 'TopX'
        %     [~, sortedDirectionIds] = sort(directionsPrior.counts, 'descend');
        %     directionIds = sortedDirectionIds(1:nSampledOrthants);
        %     directionIds = repelem(directionIds, ...
        %         directionsPrior.counts(directionIds) * ...
        %         settings.nSamplesMultiplier);
        %     directions = directionsPrior.signs(:, directionIds);
        otherwise
            error("Unsupported sampling method '%s'.\n", settings.method);
    end
    
    samples = sampleFluxSpaceFromDirections( ...
    	model, directions, isConstrainedRxn, settings, varargin{:});
end

function expectedSamples = getExpectedSamples(weights, nSamples)
    nRuns = 20;
    sumOfSamples = zeros(1, nRuns);
    for iRun = 1:nRuns
        sumOfSamples(iRun) = sum(weights(datasample(1:numel(weights), ...
            nSamples, 2, 'Weights', weights, 'Replace', false)));
    end
    expectedSamples = mean(sumOfSamples);
end