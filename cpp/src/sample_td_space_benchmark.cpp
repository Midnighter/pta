// Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
// Computational Systems Biology group, D-BSSE
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <chord_samplers/mvn_pdf_sampler.h>
#include <descriptors/ellipsoid.h>
#include <hit_and_run_sampler.h>
#include <loggers/eigen_state_logger.h>
#include <loggers/file_progress_logger.h>
#include <loggers/multi_logger.h>

#include <Eigen/Dense>
#include <limits>
#include <memory>

#include "loggers/console_progress_logger.h"
#include "matlab_helper.h"
#include "settings/free_energy_sampling_settings.h"
#include "steady_state_free_energies_descriptor.h"

using namespace hyperflux;
using namespace hyperflux::matlab_helper;
using namespace matlab::engine;
using namespace samply;
using namespace std;

using DoubleArray = matlab::data::TypedArray<double>;
using IndexArray = matlab::data::TypedArray<int64_t>;

typedef double Scalar;

using Sampler = HitAndRunSampler<Scalar,
                                 SteadyStateFreeEnergiesDescriptor,
                                 MvnPdfSampler,
                                 ConsoleProgressLogger>;

template <typename T>
T get_var(std::shared_ptr<MATLABEngine> engine, const std::u16string& var_name)
{
    auto result = engine->getVariable(var_name);
    return T(result);
}

template <typename T>
auto get_as_matrix(std::shared_ptr<MATLABEngine> engine, const std::u16string& var_name)
{
    auto tmp = get_var<T>(engine, var_name);
    return matlab_helper::matlab_matrix_to_eigen(T(tmp));
}

int main(int argc, char* argv[])
{
    // Create MATLAB engine and load MAT file with the benchmark inputs.
    std::shared_ptr<MATLABEngine> engine = startMATLAB();
    engine->eval(u"load('../data/benchmark/td_benchmark_data.mat');");

    auto E = get_as_matrix<DoubleArray>(engine, u"mvnToObsT");
    auto f = get_as_matrix<DoubleArray>(engine, u"mvnToObsShift");
    auto S = get_as_matrix<DoubleArray>(engine, u"S");
    auto lb = get_as_matrix<DoubleArray>(engine, u"lb");
    auto ub = get_as_matrix<DoubleArray>(engine, u"ub");
    Eigen::Matrix<Eigen::Index, -1, 1> constrained_rxns_ids =
        get_as_matrix<IndexArray>(engine, u"constrainedRxnsIds").cast<Eigen::Index>();
    auto initial_state =
        get_as_matrix<DoubleArray>(engine, u"initialPoints").cast<Scalar>();
    auto matlab_settings = get_var<matlab::data::StructArray>(engine, u"settings");
    FreeEnergySamplingSettings settings =
        create_free_energy_sampling_settings(matlab_settings);
    AffineTransform<double> vars_to_drg(
        get_as_matrix<DoubleArray>(engine, u"obsToDrgT"),
        get_as_matrix<DoubleArray>(engine, u"obsToDrgShift"));
    auto direction_transform_T = get_as_matrix<DoubleArray>(engine, u"dirT");
    AffineTransform<double> directions_transform(
        direction_transform_T, Vector<double>::Zero(direction_transform_T.rows()));

    Ellipsoid<Scalar> vars_constraints(E * settings.truncation_multiplier, f);
    FluxConstraints flux_constraints{S, lb, ub};
    ThermodynamicConstraints thermo_constraints{constrained_rxns_ids, vars_to_drg};

    SteadyStateFreeEnergiesDescriptor<Scalar> descriptor(
        vars_constraints, flux_constraints, thermo_constraints, directions_transform,
        settings);
    MvnPdfSampler<Scalar> chord_sampler(AffineTransform<double>(E, f));

    const size_t preallocated_states =
        (settings.num_steps - settings.num_skipped_steps) / settings.steps_thinning;
    Sampler::LoggerType logger(1, 1'000ms);

    Sampler sampler(descriptor, chord_sampler, logger);
    sampler.set_state(initial_state);
    sampler.simulate(settings.num_steps);

    return 0;
}
