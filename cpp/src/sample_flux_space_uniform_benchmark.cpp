// Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
// Computational Systems Biology group, D-BSSE
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <chord_samplers/uniform_pdf_sampler.h>
#include <coordinate_hit_and_run_sampler.h>
#include <descriptors/polytope.h>

#include <Eigen/Dense>
#include <limits>
#include <memory>

#include "loggers/console_progress_logger.h"
#include "loggers/file_progress_logger.h"
#include "loggers/multi_logger.h"
#include "settings/uniform_flux_sampling_settings.h"

using namespace hyperflux;
using namespace samply;
using namespace std;

typedef double Scalar;

template <typename ChainState, typename DurationFormat>
using Loggers = MultiLogger<ChainState, DurationFormat, ConsoleProgressLogger>;
using Sampler =
    CoordinateHitAndRunSampler<Scalar, Polytope, UniformPdfSampler, Loggers>;

int main(int argc, char* argv[])
{
    Eigen::Index n_dimensions = 500;
    Eigen::Index n_constraints = n_dimensions * 2;
    Eigen::Index n_samples = 24;

    Matrix<double> G(n_constraints, n_dimensions);
    G << Matrix<double>::Identity(n_dimensions, n_dimensions),
        -Matrix<double>::Identity(n_dimensions, n_dimensions);
    auto h = Vector<double>::Ones(n_constraints);
    auto initial_state = Matrix<Scalar>::Zero(n_dimensions, n_samples);

    UniformFluxSamplingSettings settings(
        {0, 300'000, 1, 3000, 150'000, 1'000ms, std::string("")});

    Polytope<Scalar> descriptor(G, h);
    UniformPdfSampler<Scalar> chord_sampler(G.cols());

    Sampler::LoggerType::Logger<0> cl1(settings.worker_id,
                                       settings.console_logging_interval_ms);
    Sampler::LoggerType logger(cl1);

    Sampler sampler(descriptor, chord_sampler, logger);
    sampler.set_state(initial_state);
    sampler.simulate(settings.num_steps);

    return 0;
}