// Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
// Computational Systems Biology group, D-BSSE
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <chord_samplers/uniform_pdf_sampler.h>
#include <coordinate_hit_and_run_sampler.h>
#include <descriptors/polytope.h>
#include <loggers/eigen_state_logger.h>
#include <loggers/file_progress_logger.h>
#include <loggers/multi_logger.h>

#include <Eigen/Dense>
#include <limits>
#include <memory>

#include "matlab_helper.h"
#include "settings/uniform_flux_sampling_settings.h"

using namespace hyperflux;
using namespace hyperflux::matlab_helper;
using namespace samply;
using namespace std;

using DoubleArray = matlab::data::TypedArray<double>;

typedef double Scalar;

template <typename ChainState, typename DurationFormat>
using Loggers =
    MultiLogger<ChainState, DurationFormat, FileProgressLogger, EigenStateLogger>;
using Sampler =
    CoordinateHitAndRunSampler<Scalar, Polytope, UniformPdfSampler, Loggers>;

class MexFunction : public matlab::mex::Function {
public:
    /**
     * @brief Executes the MEX function. Forwards the call to
     * ThermodynamicFeasibilityVerifier::is_thermodynamically_feasible()
     *
     * @param outputs Output arguments of the MATLAB function.
     * @param inputs Input arguments of the MATLAB function.
     */
    void operator()(matlab::mex::ArgumentList outputs, matlab::mex::ArgumentList inputs)
    {
        // Verify that we got the expected number and type of arguments.
        check_arguments(outputs, inputs);

        auto G = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[0]));
        auto h = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[1]));
        AffineTransform<double> from_Fd(
            matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[2])),
            Vector<double>::Zero(G.cols()));
        Matrix<Scalar> initial_state =
            matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[3]))
                .cast<Scalar>();
        auto matlab_settings = matlab::data::StructArray(inputs[4]);
        UniformFluxSamplingSettings settings =
            create_uniform_flux_sampling_settings(matlab_settings);

        Polytope<Scalar> descriptor(G, h, from_Fd);
        UniformPdfSampler<Scalar> chord_sampler(G.cols());

        const size_t preallocated_states =
            (settings.num_steps - settings.num_skipped_steps) / settings.steps_thinning;
        Sampler::LoggerType loggers(
            Sampler::LoggerType::Logger<0>(settings.log_directory, settings.worker_id,
                                           settings.console_logging_interval_ms),
            Sampler::LoggerType::Logger<1>(settings.worker_id, settings.steps_thinning,
                                           settings.num_skipped_steps,
                                           preallocated_states));

        Sampler sampler(descriptor, chord_sampler, loggers);
        sampler.set_state(initial_state);
        sampler.simulate(settings.num_steps);

        // Copy back resulting samples.
        outputs[0] =
            create_sampling_result_object(initial_state, sampler.get_state(),
                                          sampler.get_logger().get<1>().get_states());
    }

    /**
     * @brief Verifies that the argument passed to the function are consistent.
     *
     * @param outputs The inputs passed to the function.
     * @param inputs The outputs passed to the function.
     * @return True if the function has been called in initialization mode.
     */
    void check_arguments(matlab::mex::ArgumentList outputs,
                         matlab::mex::ArgumentList inputs)
    {
        using matlab_helper::throw_error;

        // Validate output types:
        //		0) The samples.
        if (outputs.size() != 1u)
            throw_error("One output required.", getEngine());

        // Validate input types:
        if (inputs.size() != 5)
            throw_error("Five inputs required.", getEngine());
        if (inputs[0].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[0].getDimensions().size() != 2)
            throw_error("G must be a 2-dimensional double matrix.", getEngine());
        if (inputs[1].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[1].getDimensions().size() != 2 || inputs[1].getDimensions()[1] != 1)
            throw_error("h must be a 1-dimensional double vector.", getEngine());
        if (inputs[2].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[2].getDimensions().size() != 2)
            throw_error("from_Fd must be a 2-dimensional double matrix.", getEngine());
        if (inputs[3].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[3].getDimensions().size() != 2)
            throw_error("initialPoints must be a 2-dimensional double matrix.",
                        getEngine());
        if (inputs[4].getType() != matlab::data::ArrayType::STRUCT)
            throw_error("settings must be a structure.", getEngine());
    }
};
