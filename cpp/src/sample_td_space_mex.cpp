// Copyright (c) 2019 ETH Zurich, Mattia Gollub (mattia.gollub@bsse.ethz.ch)
// Computational Systems Biology group, D-BSSE
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <chord_samplers/mvn_pdf_sampler.h>
#include <descriptors/ellipsoid.h>
#include <hit_and_run_sampler.h>
#include <loggers/directions_logger.h>
#include <loggers/eigen_state_logger.h>
#include <loggers/file_progress_logger.h>
#include <loggers/multi_logger.h>

#include <Eigen/Dense>
#include <limits>
#include <memory>

#include "matlab_helper.h"
#include "settings/free_energy_sampling_settings.h"
#include "steady_state_free_energies_descriptor.h"

using namespace hyperflux;
using namespace hyperflux::matlab_helper;
using namespace samply;
using namespace std;

using DoubleArray = matlab::data::TypedArray<double>;
using IndexArray = matlab::data::TypedArray<int64_t>;

using Scalar = double;

template <typename ChainState, typename DurationFormat>
using Loggers =
    MultiLogger<ChainState, DurationFormat, FileProgressLogger, EigenStateLogger>;
template <typename ChainState, typename DurationFormat>
using LoggersWithDirs = MultiLogger<ChainState,
                                    DurationFormat,
                                    FileProgressLogger,
                                    EigenStateLogger,
                                    DirectionsLogger>;

using Sampler =
    HitAndRunSampler<Scalar, SteadyStateFreeEnergiesDescriptor, MvnPdfSampler, Loggers>;
using SamplerWithDirectionsLogger = HitAndRunSampler<Scalar,
                                                     SteadyStateFreeEnergiesDescriptor,
                                                     MvnPdfSampler,
                                                     LoggersWithDirs>;

class MexFunction : public matlab::mex::Function {
public:
    /**
     * @brief Executes the MEX function. Forwards the call to
     * ThermodynamicFeasibilityVerifier::is_thermodynamically_feasible()
     *
     * @param outputs Output arguments of the MATLAB function.
     * @param inputs Input arguments of the MATLAB function.
     */
    void operator()(matlab::mex::ArgumentList outputs, matlab::mex::ArgumentList inputs)
    {
        // Verify that we got the expected number and type of arguments.
        check_arguments(outputs, inputs);

        auto E = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[0]));
        auto f = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[1]));
        auto S = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[2]));
        auto lb = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[3]));
        auto ub = matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[4]));
        Eigen::Matrix<Eigen::Index, -1, 1> constrained_rxns_ids =
            matlab_helper::matlab_matrix_to_eigen(IndexArray(inputs[5]))
                .cast<Eigen::Index>();
        auto initial_state =
            matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[6]))
                .cast<Scalar>();
        auto matlab_settings = matlab::data::StructArray(inputs[7]);
        FreeEnergySamplingSettings settings =
            create_free_energy_sampling_settings(matlab_settings);
        AffineTransform<double> vars_to_drg(
            matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[8])),
            matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[9])));
        auto direction_transform_T =
            matlab_helper::matlab_matrix_to_eigen(DoubleArray(inputs[10]));
        AffineTransform<double> directions_transform(
            direction_transform_T, Vector<double>::Zero(direction_transform_T.rows()));

        Ellipsoid<Scalar> vars_constraints(E * settings.truncation_multiplier, f);
        FluxConstraints flux_constraints{S, lb, ub};
        ThermodynamicConstraints thermo_constraints{constrained_rxns_ids, vars_to_drg};

        SteadyStateFreeEnergiesDescriptor<Scalar> descriptor(
            vars_constraints, flux_constraints, thermo_constraints,
            directions_transform, settings);
        MvnPdfSampler<Scalar> chord_sampler(AffineTransform<double>(E, f));

        const size_t preallocated_states =
            (settings.num_steps - settings.num_skipped_steps) / settings.steps_thinning;

        if (false) {
            Sampler::LoggerType loggers(
                Sampler::LoggerType::Logger<0>(settings.log_directory,
                                               settings.worker_id,
                                               settings.console_logging_interval_ms),
                Sampler::LoggerType::Logger<1>(
                    settings.worker_id, settings.steps_thinning,
                    settings.num_skipped_steps, preallocated_states));

            Sampler sampler(descriptor, chord_sampler, loggers);
            sampler.set_state(initial_state);
            sampler.simulate(settings.num_steps);

            // Copy back resulting samples.
            outputs[0] = create_sampling_result_object(
                initial_state, sampler.get_state(),
                sampler.get_logger().get<1>().get_states());
        } else {
            SamplerWithDirectionsLogger::LoggerType loggers(
                SamplerWithDirectionsLogger::LoggerType::Logger<0>(
                    settings.log_directory, settings.worker_id,
                    settings.console_logging_interval_ms),
                SamplerWithDirectionsLogger::LoggerType::Logger<1>(
                    settings.worker_id, settings.steps_thinning,
                    settings.num_skipped_steps, preallocated_states),
                SamplerWithDirectionsLogger::LoggerType::Logger<2>(
                    settings.worker_id, settings.steps_thinning_directions,
                    settings.num_skipped_steps));

            SamplerWithDirectionsLogger sampler(descriptor, chord_sampler, loggers);
            sampler.get_logger().get<2>().set_descriptor(
                sampler.get_space_descriptor());
            sampler.get_logger().get<2>().set_sampler(sampler.get_chord_sampler());
            sampler.set_state(initial_state);
            sampler.simulate(settings.num_steps);

            // Copy back resulting samples.
            outputs[0] = create_sampling_result_object(
                initial_state, sampler.get_state(),
                sampler.get_logger().get<1>().get_states(),
                sampler.get_logger().get<2>().get_directions_counts());
        }
    }

    /**
     * @brief Verifies that the argument passed to the function are consistent.
     *
     * @param outputs The inputs passed to the function.
     * @param inputs The outputs passed to the function.
     * @return True if the function has been called in initialization mode.
     */
    void check_arguments(matlab::mex::ArgumentList outputs,
                         matlab::mex::ArgumentList inputs)
    {
        using matlab_helper::throw_error;

        // Validate output types:
        //		0) The samples.
        if (outputs.size() != 1u)
            throw_error("One output required.", getEngine());

        // Validate input types:
        //		0) Transform from hypershpere to DrG
        //		1) Shift from hypershpere to DrG
        //		2) S
        //		3) lb
        //		4) ub
        //		5) constrainedRxnIds
        //		6) initialPoints
        //		7) settings
        //		8 + 9) Transform from hypersphere to observables
        //		10) Weights for direciton choice
        if (inputs.size() != 11)
            throw_error("Eleven inputs required.", getEngine());
        if (inputs[0].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[0].getDimensions().size() != 2)
            throw_error("T must be a 2-dimensional double matrix.", getEngine());
        if (inputs[1].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[1].getDimensions().size() != 2 || inputs[1].getDimensions()[1] != 1)
            throw_error("f must be a 1-dimensional double vector.", getEngine());
        if (inputs[2].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[2].getDimensions().size() != 2)
            throw_error("S must be a 2-dimensional double matrix.", getEngine());
        if (inputs[3].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[3].getDimensions().size() != 2 || inputs[3].getDimensions()[1] != 1)
            throw_error("lb must be a 1-dimensional double vector.", getEngine());
        if (inputs[4].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[4].getDimensions().size() != 2 || inputs[4].getDimensions()[1] != 1)
            throw_error("ub must be a 1-dimensional double vector.", getEngine());
        if (inputs[5].getType() != matlab::data::ArrayType::INT64 ||
            inputs[5].getDimensions().size() != 2 || inputs[5].getDimensions()[1] != 1)
            throw_error("constrainedRxnIds must be a 1-dimensional integer vector.",
                        getEngine());
        if (inputs[6].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[6].getDimensions().size() != 2)
            throw_error("initialPoints 2-dimensional double matrix.", getEngine());
        if (inputs[7].getType() != matlab::data::ArrayType::STRUCT)
            throw_error("settings must be a structure.", getEngine());
        if (inputs[8].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[8].getDimensions().size() != 2)
            throw_error("Tdrg must be a 2-dimensional double matrix.", getEngine());
        if (inputs[9].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[9].getDimensions().size() != 2 || inputs[9].getDimensions()[1] != 1)
            throw_error("fdrg must be a 1-dimensional double vector.", getEngine());
    }
};
